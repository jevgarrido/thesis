

Direct and Indirect Method Paths and the Covector Mapping Theorem
	Mathematical Background
		Gaussian Quadrature
		Lagrange Polynomial Interpolation
		Legendre-Radau Polynomial and Runge Phenomenon
		Optimization and Lagrange Multipliers
	The Optimal Control Problem
		Continuous Hamilton Boundary-Value Problem (HBVP)
		Discrete Hamilton Boundary-Value Problem (DHBVP)
			+ The structure of the differentiation matrix will change according to the differentiation method employed.
			+ For instance, using central differences in a uniform mesh (common in indirect methods) yields a tridiagonal matrix, while using Lagrange interpolation at the Radau nodes yields a dense matrix.
			+ 
			Chart
		Nonlinear Programming Problem 
		KKT Conditions
			Chart
	Covector Mapping Theorem
		

Method Implementation
	Multiphase Optimal Control and NLP Problems
	Legendre-Radau Root finding
	Initial guess Generation
	Vector Formatting
	Jacobian Matrix associated with the NLP


Numerical Examples
	