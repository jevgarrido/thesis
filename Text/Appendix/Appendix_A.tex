
\chapter{Detailed Jacobian Matrix Structure}
\label{append:Jacobian}

In this Appendix is presented the generic structure of the Jacobian matrix in accordance with
\begin{stdlist}
	\item the flipped Radau pseudospectral method.
	\item the ordering of the decision variables described in Section \ref{sec:formatting}.
	\item the choice of covector scalars $C_{\lambda,k}\pp$ and $C_{\mu,k}\pp$ presented in Table \ref{tab:choice_covector_scalars}.
\end{stdlist}
The generic form of the Jacobian matrix from \eqref{eq:JacobianGeneric} is here reiterated,
\begin{equation} \label{App_eq:JacobianGeneric}
    \textbf{Jac} =
    \begin{bmatrix}
    \nabla \mathcal{J}^N \\
    \nabla \bm{F}^{(1:P)} \\
    \nabla \bm{H}^{(1:P)} \\
    \nabla \bm{\phi}^{(1:P)} \\
    \nabla \fLink^{(1:P-1)}
    \end{bmatrix} \,.
\end{equation}
In this appendix, a Section is dedicated to each group of gradients of the Jacobian matrix in the same order as they appear in \eqref{App_eq:JacobianGeneric}, such that Section \ref{App:Jacobian_Cost} is dedicated to the gradient of the cost functional, $\nabla \mathcal{J}^N$, Section \ref{App:Jacobian_Dynamics} is dedicated to the gradient of the dynamic defects, $\nabla \bm{F}^{(1:P)}$, and so on up to Section \ref{App:Jacobian_Link} which is dedicated to the gradient of the linkage conditions, $\nabla \fLink^{(1:P-1)}$.

\newcommand{\sz}[1]{ S^{(#1)} }
\newcommand{\szNLP}{S_{\text{NLP}} }

In order to simplify the syntax, the quantity, $\sz{p}$ is introduced as
\begin{equation}
	\sz{p} = n_x + N^{(p)}(n_x+n_u) \,.
\end{equation}
This quantity represents the size of the array containing the decision variables of state and control in a given phase $^{(p)}$. \par 

It is relevant to note that the computation of partial derivatives is taken for granted in this chapter. These derivatives are not trivial, but they can be computed by means of several different methods. By omitting the computation of the derivatives, the length of this chapter is shortened and also no particular differentiation method is highlighted. \par 


\section[Cost Function Gradient]{Cost Function Gradient, $\nabla \mathcal{J}^N$}
\label{App:Jacobian_Cost}

The phase-wise gradient of the cost function, $\nabla_{\bm{XU}^{(p)}} \mathcal{J}^N$ is
\begin{equation}
\begin{split}
	\nabla_{\bm{XU}^{(p)}} \mathcal{J}^N & = \frac{t_f^{(p)}-t_0^{(p)}}{2} \biggl[
		\begin{matrix}
			\displaystyle \frac{2}{t_f^{(p)}-t_0^{(p)}} \bm{a}^{(p)} &
			\displaystyle w_1^{(p)} \pdv{\Psi_1}{\bm{x}_1^{(p)}} &
			\displaystyle w_1^{(p)} \pdv{\Psi_1}{\bm{u}_1^{(p)}} &
			\displaystyle w_2^{(p)} \pdv{\Psi_2}{\bm{x}_2^{(p)}} &
			\displaystyle w_2^{(p)} \pdv{\Psi_2}{\bm{u}_2^{(p)}} &
			\dots
		\end{matrix} \\[0.5em]
		& \phantom{= \frac{t_f^{(p)}}{2}} \begin{matrix}
			\displaystyle w_{N^{(p)}-1}^{(p)} \pdv{\Psi_{N^{(p)}-1}}{\bm{x}_{N^{(p)}-1}^{(p)}} &
			\displaystyle w_{N^{(p)}-1}^{(p)} \pdv{\Psi_{N^{(p)}-1}}{\bm{u}_{N^{(p)}-1}^{(p)}} &
			\displaystyle \frac{2}{t_f^{(p)}-t_0^{(p)}} \bm{b}^{(p)} + w_{N^{(p)}}^{(p)} \pdv{\Psi_{N^{(p)}}}{\bm{x}_{N^{(p)}}^{(p)}} &
			\displaystyle w_{N^{(p)}}^{(p)} \pdv{\Psi_{N^{(p)}}}{\bm{u}_{N^{(p)}}^{(p)}}
		\end{matrix}
		\biggr] \\
		& = \nabla \mathcal{J}_{\bm{XU}}^{(p)} \,,
\end{split}
\end{equation}
where $\nabla \mathcal{J}_{\bm{XU}}^{(p)}$ is of size $1 \times \sz{p}$, and
\begin{equation}
	\bm{a}^{(p)} =
		\begin{cases}
			\displaystyle \pdv{\Phi}{\bm{x}_0^{(p)}} \,, & \text{if } p = 1 \\
			\bm{0}_{1 \times n_{\bm{x}} } \,, & \text{if } p \neq 1
		\end{cases} \,, \quad \quad \quad \:
	\bm{b}^{(p)} =
		\begin{cases}
		 	\bm{0}_{1 \times n_{\bm{x}} } \,, & \text{if } p \neq P \\
		 	\displaystyle \pdv{\Phi}{\bm{x}_{N^{(p)}}^{(p)}} \,, & \text{if } p = P
		\end{cases} \,.
\end{equation}
And
\begin{align}
	\pdv{\mathcal{J}^N}{t_f\pp} & = \begin{cases}
		{\displaystyle \frac{1}{2}\sum_{k}^{N^{(p)}} w_k^{(p)} \Psi_k^{(p)} - \frac{1}{2}\sum_{k}^{N^{(p+1)}} w_k^{(p+1)} \Psi_k^{(p+1)} } \,, \quad & \text{if } p = 1,2,\dots, P-1\\
		{\displaystyle \frac{1}{2}\sum_{k}^{N^{(p)}} w_k^{(p)} \Psi_k^{(p)} +  \pdv{\Phi}{t_f^{(p)}} } \,, \quad & \text{if } p = P
	\end{cases} \\
	& = \nabla \mathcal{J}_{t_f}\pp \,.
\end{align}
Ultimately, the vector of concatenated gradients of the cost is
\begin{equation} \label{eq:CostGradient}
\begin{split}
	\nabla \mathcal{J}^N =
		\begin{bmatrix}
			\nabla \mathcal{J}_{\bm{XU}}^{(1)} &
			\nabla \mathcal{J}_{\bm{XU}}^{(2)} &
			\dots &
			\nabla \mathcal{J}_{\bm{XU}}^{(P)} &
			\nabla \mathcal{J}_{t_f}^{(1)} &
			\nabla \mathcal{J}_{t_f}^{(2)} &
			\dots &
			\nabla \mathcal{J}_{t_f}^{(P)}
		\end{bmatrix} \,.
\end{split}
\end{equation}
Equation \eqref{eq:CostGradient} corresponds to the first row of the matrix illustrated in Figure \ref{fig:JacobianSparisty}.


\section[Gradient of Dynamic Defects]{Gradient of Dynamic Defects, $\nabla \bm{F}^{(1:P)}$}
\label{App:Jacobian_Dynamics}

The dependency of the dynamic defects with respect to the states and controls can be stated as
\begin{equation}
	\nabla_{\bm{XU}^{(q)}} \bm{F}\pp = \begin{cases}
		\nabla \bm{F}_{\bm{XU}}\pp \,, \quad & \text{if } q = p  \\
		\bm{0}_{n_{\bm{x}}N\pp \times \sz{q}} \,, \quad & \text{if } q \neq p
		\end{cases} \,,
\end{equation}
which is saying that states and controls of a given phase do not influence the dynamics of a different phase. This is favourable because it implies that the Jacobian will be sparse.
The dynamic defects for a given phase can be expressed as
\begin{equation}
	\nabla \bm{F}_{\bm{XU}}\pp = \left[ \begin{smallmatrix}
		-\bm{D}_{10}\pp \bm{I}_{n_{\bm{x}}} & \frac{t_f^{(p)}-t_0^{(p)}}{2} {\textstyle \pdv{\bm{f}_1\pp}{\bm{x}_1\pp}} -\bm{D}_{11}\pp \bm{I}_{n_{\bm{x}}} & \frac{t_f^{(p)}-t_0^{(p)}}{2} {\textstyle \pdv{\bm{f}_1\pp}{\bm{u}_1\pp} } & {\textstyle \cdots } & -\bm{D}_{1N\pp}\pp \bm{I}_{n_{\bm{x}}} & \bm{0}_{n_{\bm{x}} \times n_{\bm{u}}} \\
		-\bm{D}_{20}\pp \bm{I}_{n_{\bm{x}}} & -\bm{D}_{21}\pp \bm{I}_{n_{\bm{x}}} & \bm{0}_{n_{\bm{x}} \times n_{\bm{u}}}  & {\textstyle \cdots } & -\bm{D}_{2N\pp}\pp \bm{I}_{n_{\bm{x}}} & \bm{0}_{n_{\bm{x}} \times n_{\bm{u}}} \\
		\vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
		-\bm{D}_{N\pp0}\pp \bm{I}_{n_{\bm{x}}} & - \bm{D}_{N\pp1}\pp \bm{I}_{n_{\bm{x}}} & \bm{0}_{n_{\bm{x}} \times n_{\bm{u}}} & {\textstyle \cdots } & \frac{t_f^{(p)}-t_0^{(p)}}{2} {\textstyle \pdv{\bm{f}_{N\pp}\pp}{\bm{x}_{N\pp}\pp} } - \bm{D}_{{N\pp}{N\pp}}\pp \bm{I}_{n_{\bm{x}}} & \frac{t_f^{(p)}-t_0^{(p)}}{2} {\textstyle \pdv{\bm{f}_{N\pp}\pp}{\bm{u}_{N\pp}\pp} }
	\end{smallmatrix} \right] \,.
\end{equation}
Further, the dependency of the dynamic defects with respect to the initial and final times is
\begin{equation}
	\nabla_{t_f^{(q)}}\bm{F}\pp = \begin{bmatrix}
		\displaystyle \pdv{\Ddyn_1\pp}{t_f^{(q)}}^\intercal &
		\displaystyle \pdv{\Ddyn_2\pp}{t_f^{(q)}}^\intercal &
		\displaystyle \cdots &
		\displaystyle \pdv{\Ddyn_{N\pp}\pp}{t_f^{(q)}}^\intercal
	\end{bmatrix}^\intercal \,,
\end{equation}
with
\begin{equation}
	\pdv{\Ddyn_k\pp}{t_f^{(q)}} = \begin{cases}
	\frac{1}{2} \bm{f}_k \,,		& \text{if } q = p \\
	-\frac{1}{2} \bm{f}_k \,,		& \text{if } q = p-1\\
	\bm{0}_{n_{\bm{x}}\times 1} \,, & \text{if } q \neq p, p-1
	\end{cases} \,.
\end{equation}
Letting the initial and final time gradients of a given phase be
\begin{align}
	\nabla \bm{F}_{t_0}\pp &= \nabla_{t_f^{(p-1)}}\bm{F}\pp \,, \\
	\nabla \bm{F}_{t_f}\pp &= \nabla_{t_f\pp}\bm{F}\pp \,.
\end{align}
Accounting for all phases, the gradient of the dynamic defects can be written as
\begin{equation}
	\nabla \bm{F}^{(1:P)} = \left[ \begin{smallmatrix}
		\nabla \bm{F}_{\bm{XU}}^{(1)} & \bm{0}_{n_{\bm{x}}N^{(1)} \times \sz{2}} 	& \cdots & \bm{0}_{n_{\bm{x}}N^{(1)} \times \sz{P}} & \nabla \bm{F}_{t_f}^{(1)} & \bm{0}_{n_{\bm{x}}N^{(1)} \times 1} & \cdots  & \bm{0}_{n_{\bm{x}}N^{(1)} \times 1}  & \bm{0}_{n_{\bm{x}}N^{(1)} \times 1}  \\
		\bm{0}_{n_{\bm{x}}N^{(2)} \times \sz{1}}  & \nabla \bm{F}_{\bm{XU}}^{(2)}	& \cdots & \bm{0}_{n_{\bm{x}}N^{(2)} \times \sz{P}} & \nabla \bm{F}_{t_0}^{(2)} & \nabla \bm{F}_{t_f}^{(2)} & \cdots  & \bm{0}_{n_{\bm{x}}N^{(2)} \times 1}  & \bm{0}_{n_{\bm{x}}N^{(2)} \times 1}  \\
		\vdots & \vdots	& \ddots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots  \\
		\bm{0}_{n_{\bm{x}}N^{(P)} \times \sz{1}}  & \bm{0}_{n_{\bm{x}}N^{(P)} \times \sz{2}} 							& \cdots & \nabla \bm{F}_{\bm{XU}}^{(P)} & \bm{0}_{n_{\bm{x}}N^{(P)} \times 1} & \bm{0}_{n_{\bm{x}}N^{(P)} \times 1} & \cdots & \nabla \bm{F}_{t_0}^{(P)} & \nabla \bm{F}_{t_f}^{(P)}
	\end{smallmatrix} \right] \,.
\end{equation}
In this matrix the sparsity pattern of the dynamic defects is evident. This pattern is represented by blocks of red dots and blue diagonal dots in Fig.~\ref{fig:JacobianSparisty}. \par 


\section[Gradient of Path Constraints]{Gradient of Path Constraints, $\nabla \bm{H}^{(1:P)}$}
\label{App:Jacobian_Path}

Similarly to the case of dynamic defects, the states and controls of a given phase do not influence the path constraints of a different phase. The phase-wise path constraint gradient is thus:
\begin{equation}
	\nabla \bm{H}_{\bm{XU}}\pp = \left[ \begin{smallmatrix}
		\bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}} & {\textstyle \pdv{\bm{h}_1\pp}{\bm{x}_1\pp}} & {\textstyle \pdv{\bm{h}_1\pp}{\bm{u}_1\pp} } & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}}  & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{u}}}  & {\textstyle \cdots } & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}} & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{u}}} \\
		\bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}} & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}}  & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{u}}} & {\textstyle \pdv{\bm{h}_2\pp}{\bm{x}_2\pp}} & {\textstyle \pdv{\bm{h}_2\pp}{\bm{u}_2\pp} }  & {\textstyle \cdots } & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}}  & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{u}}}  \\
		\vdots & \vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
		\bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}} & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}}  & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{u}}} & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{x}}}  & \bm{0}_{ n_{\bm{h}}\pp \times n_{\bm{u}}}  & {\textstyle \cdots } & {\textstyle \pdv{\bm{h}_{N\pp}\pp}{\bm{x}_{N\pp}\pp} } & {\textstyle \pdv{\bm{h}_{N\pp}\pp}{\bm{u}_{N\pp}\pp} }
	\end{smallmatrix} \right] \,,
\end{equation}
and the concatenated matrix for all phases is
\begin{equation}
	\nabla \bm{H}^{(1:P)} = \begin{bmatrix}
		\nabla \bm{H}_{\bm{XU}}^{(1)} & \bm{0}_{n_{\bm{h}}^{(1)}N^{(1)} \times \sz{2}} 	& \cdots & \bm{0}_{n_{\bm{h}}^{(1)}N^{(1)} \times \sz{P}} & \bm{0}_{ n_{\bm{h}}^{(1)} N^{(1)} \times P } \\
		\bm{0}_{n_{\bm{h}}^{(2)}N^{(2)} \times \sz{1}}  & \nabla \bm{H}_{\bm{XU}}^{(2)}	& \cdots & \bm{0}_{n_{\bm{h}}^{(2)}N^{(2)} \times \sz{P}} & \bm{0}_{ n_{\bm{h}}^{(2)} N^{(2)} \times P } \\
		\vdots & \vdots	& \ddots & \vdots & \vdots \\
		\bm{0}_{n_{\bm{h}}^{(P)}N^{(P)} \times \sz{1}}  & \bm{0}_{n_{\bm{h}}^{(P)}N^{(P)} \times \sz{2}} 							& \cdots & \nabla \bm{H}_{\bm{XU}}^{(P)} & \bm{0}_{ n_{\bm{h}}^{(P)} N^{(P)} \times P }
	\end{bmatrix} \,.
\end{equation}



\section[Event Constraints Gradient]{Event Constraints Gradient, $\nabla \bm{\phi}^{(1:P)}$}
\label{App:Jacobian_Event}

With regards to the event constraints, only the initial and final states of each phase represent dependencies, thus, only two blocks of size $n_{\bm{x}} \times n_{\bm{x}}$ appear on the Jacobian of the phase-wise event constraints: one on the left for the dependency on the initial state, and one on the right for the dependency on the final time. In between these two blocks there are only zeros.
\begin{equation}
	\nabla \bm{\phi}_{\bm{XU}}\pp = \left[ \begin{matrix}
		{\textstyle \pdv{\bm{\phi}\pp}{\bm{x}_0\pp} } & \bm{0}_{ n_{\bm{\phi}}\pp \times n_{\bm{x}}}  & \bm{0}_{ n_{\bm{\phi}}\pp \times n_{\bm{u}}}  & {\textstyle \cdots } & {\textstyle \pdv{\bm{\phi}\pp}{\bm{x}_{N\pp}\pp} }& \bm{0}_{ n_{\bm{\phi}}\pp \times n_{\bm{u}}} 
	\end{matrix} \right] \,.
\end{equation}
In order to account for all phases, a diagonal concatenation is in place, as
\begin{equation}
	\nabla \bm{\phi}^{(1:P)} = \begin{bmatrix}
		\nabla \bm{\phi}_{\bm{XU}}^{(1)} & \bm{0}_{n_{\bm{\phi}}^{(1)} \times \sz{2}} 	& \cdots & \bm{0}_{n_{\bm{\phi}}^{(1)} \times \sz{P}} & \bm{0}_{ n_{\bm{\phi}}^{(1)} \times P } \\
		\bm{0}_{n_{\bm{\phi}}^{(2)} \times \sz{1}}  & \nabla \bm{\phi}_{\bm{XU}}^{(2)}	& \cdots & \bm{0}_{n_{\bm{\phi}}^{(2)} \times \sz{P}} & \bm{0}_{ n_{\bm{\phi}}^{(2)} \times P }\\
		\vdots & \vdots	& \ddots & \vdots & \vdots \\
		\bm{0}_{n_{\bm{\phi}}^{(P)} \times \sz{1}}   & \bm{0}_{n_{\bm{\phi}}^{(P)} \times \sz{2}} 							& \cdots & \nabla \bm{\phi}_{\bm{XU}}^{(P)} & \bm{0}_{ n_{\bm{\phi}}^{(P)} \times P }
	\end{bmatrix} \,.
\end{equation}


\section[Gradient of the Linkage Conditions]{Gradient of the Linkage Conditions, $\nabla \fLink^{(1:P-1)}$}
\label{App:Jacobian_Link}
    
Finally, for the linkage conditions, the matrices for the left and right pairs of phases are defined as
\begin{align}
	\nabla \Delta \bm{x}_l\pp &= \begin{bmatrix}
	\bm{0}_{n_{\bm{x}} \times n_{\bm{x}} } &
	\bm{0}_{n_{\bm{x}} \times n_{\bm{x}} }  &
	\bm{0}_{n_{\bm{x}} \times n_{\bm{u}} }  &
	\dots &
	-\bm{I}_{n_{\bm{x}} \times n_{\bm{x}}} &
	\bm{0}_{n_{\bm{x}} \times n_{\bm{u}} }
	\end{bmatrix} \\
	\nabla \Delta \bm{x}_r\pp &= \begin{bmatrix}
	\bm{I}_{n_{\bm{x}} \times n_{\bm{x}}}  &
	\bm{0}_{n_{\bm{x}} \times n_{\bm{x}} }  &
	\bm{0}_{n_{\bm{x}} \times n_{\bm{u}} }  &
	\dots &
	\bm{0}_{n_{\bm{x}} \times n_{\bm{x}} } &
	\bm{0}_{n_{\bm{x}} \times n_{\bm{u}} } 
	\end{bmatrix}
\end{align}
where $\bm{I}_{n_{\bm{x}} \times n_{\bm{x}}}$ is the identity matrix of size $n_{\bm{x}} \times n_{\bm{x}}$. Invariably, the full gradient matrix for the linkage conditions is a diagonal concatenation of of the pairs of matrices expressed above.
\begin{equation}
	\nabla \fLink^{(1:P-1)} = \begin{bmatrix}
		\nabla \Delta \bm{x}_l^{(1)} & \nabla \Delta \bm{x}_r^{(1)} 	& \bm{0}_{n_{\bm{x}} \times \sz{3}} & \cdots & \bm{0}_{n_{\bm{x}} \times \sz{P-1}} & \bm{0}_{n_{\bm{x}} \times \sz{P}} & \bm{0}_{n_{\bm{x}} \times P } \\
		\bm{0}_{n_{\bm{x}} \times \sz{1}} & \nabla \Delta \bm{x}_l^{(2)} & \nabla \Delta \bm{x}_r^{(2)} & \cdots & \bm{0}_{n_{\bm{x}} \times \sz{P-1}} & \bm{0}_{n_{\bm{x}} \times \sz{P}} & \bm{0}_{n_{\bm{x}} \times P } \\
		\vdots & \vdots	& \vdots & \ddots & \vdots & \vdots  & \vdots \\
		\bm{0}_{n_{\bm{x}} \times \sz{1}} & \bm{0}_{n_{\bm{x}} \times \sz{2}} & \bm{0}_{n_{\bm{x}} \times \sz{3}}& \cdots & \nabla \Delta \bm{x}_l^{(P-1)} & \nabla \Delta \bm{x}_r^{(P-1)} & \bm{0}_{n_{\bm{x}} \times P }
	\end{bmatrix}
\end{equation}
It can be noted that the first phase is never the "right" pair of a link and that the last phase is never the "left" pair of any link. This is because the algorithm invariably assumes that the phases are sequential.



