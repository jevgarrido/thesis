
\chapter{Introduction}
\label{chapter:intro}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Objective}
\label{intro:objective}

In this thesis, the flipped Radau Pseudospectral method for optimal control is extended to solve multiphase problems. This extension of the algorithm allows the study of problems that might contain state and/or control discontinuities or local mutations in the equations of motion, thus augmenting in complexity and in quantity the range of problems that are possible to solve. The method is applied to the case of multiple stage launch vehicles in both ascent from surface to orbit and descent from free-fall to vertical landing. \par

%%
%% Maybe: Some motivation stating potential advantages
%%


\section{The Orbital Launch Vehicle}
\label{section:olv}

An orbital launch vehicle (\gls{olv}) is a machine designed to take payloads to a specified orbit in space. These machines, commonly known as \emph{Rockets}, play a very important role in implementing (and sometimes maintaining) modern world utilities such as Global Positioning Systems, telecommunication services, space observatories, among others. Therefore, it is of ultimate interest for these machines to perform in a way that is fuel-effective and which allows a safe payload delivery. \par 

This is not a trivial task, mainly because the propulsion system of a rocket requires high values of propellant mass fraction, which is to say that the propellant alone makes up a significant proportion of the total mass of the vehicle. Essentially, the rocket has to transport all of the fuel required to propel itself along the trajectory, but the more fuel it carries the more fuel is required due to the increased weight. The \emph{Tsiolkovsky Rocket equation} can be used in order to calculate the propellant mass fraction of the vehicle given a required velocity budget. \par 


\subsection{Tsiolkovsky Rocket Equation and Staging}

The \emph{Tsiolkovsky rocket equation} provides a scalar evaluation of the performance of a given vehicle configuration. In the absence of external forces, such as gravity or aerodynamic drag, and assuming perfect fuel consumption, there is a limit to how much velocity a rocket can gain. This limit is related to the mass ratio of the vehicle by \citep{Sutton2016}
\begin{equation} \label{RocketEq}
	\Delta V = v_e \ln(\text{MR}\,) = - v_e \ln(1-\zeta) \,,
\end{equation}
where \(\Delta V\) is, for a given configuration, the available change in velocity the rocket acquires and \(v_e\) is the effective exhaust velocity. The mass ratio, MR, is the quotient between the initial and final mass of the vehicle, and $\zeta$ is the propellant mass fraction as follows \citep{Sutton2016}:
\begin{align}
	\text{MR} &= \dfrac{ \text{Initial Mass} }{ \text{Mass at Burnout} } \,,	\\[1em]
	\zeta &= \dfrac{\text{Propellant Mass}}{\text{Initial Mass}} = 1 - \dfrac{1}{\text{MR}}  \,.
\end{align} 

The higher \(\Delta V\) a vehicle configuration has, the better it is expected to perform. Figure \ref{fig:RocketEq} illustrates the relationship between the propellant mass fraction, \(\zeta\), and the ratio of output \(\Delta V\) to exhaust velocity, \(v_e\), given by \eqref{RocketEq}. The asymptotic gradient of the plot makes it clear that achieving high values of \(\Delta V\) is not trivial, as this implies that the propellant mass fraction will tend to 1 --- a vehicle with no room for payload or structural mass. In practice, the values of propellant mass fraction typically lie in between \numrange{0.75}{0.95} \citep{Isakowitz2004}. \par 
\begin{figure}[ht]
	\centering
	\includegraphics[height=50mm]{Introduction/RocketEquation.eps}
	\caption{The Tsiolkovsky rocket equation relates the propellant mass fraction, $\zeta$, to the ratio \(\Delta V / v_e\).}	
	\label{fig:RocketEq}
\end{figure}

One ingenious way to work around this is the concept of \emph{multi-stage rocket}. As expressed in \eqref{eq:MPRocketEq}, a multi-stage rocket allows the partition of the total \(\Delta V\) into multiple contributions. Letting \(P\) be the number of stages of the vehicle, the rocket equation becomes \citep{Wiesel2010}
\begin{equation} \label{eq:MPRocketEq}
	\Delta V = \sum_{p = 1}^{P} v_e^{(p)} \ln( \text{MR}\,^{(p)} )
	= - \sum_{p = 1}^{P} v_e^{(p)} \ln(1-\zeta^{(p)}) \,.
\end{equation}

A multi-stage rocket is advantageous because it makes it possible to dispose of inert mass along the trajectory as empty burnout stages are detached and jettisoned in stage separation events. At the cost of ejecting fully functioning propulsion systems along with empty tanks and structural mass at stage separation events, a multi-stage configuration also allows the modification of engine specification from stage to stage, therefore the propulsion system of each stage can be tailored to meet the requirements of the environment it will be working in. It is relevant to note that if not for the detachment of the empty stages, the multiphase configuration would be almost redundant due to the additive property of the logarithmic function. \par

The difficulty in achieving high values of \(\Delta V\) due to the character of the rocket equation is only aggravated when the effects of gravity and atmospheric drag are taken into account. Both of these forces act in opposition to the movement of the rocket during the launch sequence, which results in the suppression of the total \(\Delta V\) budget of the vehicle. \par 

In the case of gravity, for instance, assuming a vertical launch and a constant gravitational acceleration, a rough estimate of the velocity budget can be computed based on the duration of flight, \(\Delta t\) as follows \citep{Taylor2017}:
\begin{align}
	\Delta V_{\text{Actual}}	&= \Delta V_{\text{Total}} - \Delta V_{\text{Gravity}} \,, \label{eq:RocketEqGravity} \\
	\Delta V_{\text{Gravity}}	&= g_0 \Delta t \,, \label{eq:GravityTermRocketEqGravity}
\end{align}
where 
\(\Delta V_{\text{Actual}}\) is the expected velocity budget of the vehicle when subject to the force of gravity,
\(\Delta V_{\text{Total}}\) is the velocity budget of the rocket calculated via \eqref{eq:MPRocketEq} (in isolation of external forces),
\(\Delta V_{\text{Gravity}}\) is the equivalent velocity expense of gravity and
\(g_0\) is the standard gravity acceleration of the Earth.

Ultimately, a staged rocket makes it easier to achieve high values of \(\Delta V_{\text{Total}}\), necessary to overcome gravity and atmospheric drag expenses. The motivation for multi stage rockets is thus established. \par 


\subsection{Equations of Motion}

\subsubsection{Generic formulation}

As a dynamic system, an \gls{olv} a can be represented by a state vector, \( \bm{x}(t) \), descriptive of relevant properties associated with the system.  In this case, the state vector will often consist on the group \emph{position-velocity-mass} \citep{Vinh1973,Bryson1975,Betts2010}. \par 

The behaviour of the vehicle is described by the equations of motion (\gls{eom}) which describe the rate of change of the state vector with time. A sequence of states associated with a time interval is called a \emph{trajectory} and it is possible to influence the trajectory by introducing the control vector, \( \bm{u}(t) \), into the equations of motion. In the case of an \gls{olv}, the control vector will often consist on the components of \emph{thrust}. \par 

Given the state and control vectors, respectively \( \bm{x}(t) \in \mathbb{R}^{n_x} \) and \( \bm{u}(t) \in \mathbb{R}^{n_u} \), where \( n_x \) and \( n_u \) are the respective vector dimensions, it is said that the system is subject to equations of motion, generically represented as \citep{Kirk2004}:
\begin{equation}	\label{GeneralEoM}
	\dot{\bm{x}}(t) = \bm{f} \bigl( \bm{x}(t), \bm{u}(t) \bigr) \,,
\end{equation} 
where \( \bm{f}(\cdot) \) is a vector function representative of the right-hand side of the differential equations. The vector function \( \bm{f}(\cdot) \) can be explicitly dependent on the state of the system, \( \bm{x}(t) \) and on the specified control vector, \( \bm{u}(t) \). The generic formulation of \eqref{GeneralEoM} will be used throughout this work as a way to express any given dynamic system. \par 


\subsubsection{Equations of motion for rockets}

The propulsive system of a rocket is based on the reactive force from mass exhaust. The equations of motion for a rocket modelled as a point mass can be expressed as follows \citep{Wiesel2010}:
\begin{align} 
\bm{F}_\text{net} &= \displaystyle \bm{F}_\text{ext} + \bm{T} =  m \dv{\bm{v}}{t} \,, \label{eq:DynamicEq_accel} \\[1em]
\bm{T}		 &= \displaystyle \bm{v}_e \dv{m}{t} \,, \label{eq:DynamicEq_thrust}
\end{align}
where the vector \( \bm{F}_\text{net} \) is the resulting net force acting on the vehicle, the vector \( \bm{F}_\text{ext} \) is the sum of common external forces that do not change the mass of the system, \( \bm{T} \) is the thrust vector, \( m \) is the total mass of the vehicle, \( \bm{v} \) is the velocity vector, and \( \bm{v}_e \) is the effective mass exhaust velocity vector. Equations \eqref{eq:DynamicEq_accel} and \eqref{eq:DynamicEq_thrust} account for propulsion based on both escaping mass, \( \dv{m}{t} < 0\), and incident mass, \( \dv{m}{t} > 0\), with respect to the center of mass of the vehicle.
Because the mass flow rate, \( \dv{m}{t} \), is a scalar quantity, it yields that the thrust vector, \(\bm{T}\), is always collinear with the exhaust velocity vector, \(\bm{v}_e\). \par 

The equations of motion for rockets and other rocket-like propulsive systems can be written in state-space representation without loss of generality by solving \eqref{eq:DynamicEq_accel} and \eqref{eq:DynamicEq_thrust} for the rates of change of velocity and mass, respectively, \( \dv{\bm{v}}{t} \) and \( \dv{m}{t} \), and adjoining the kinematic equations \( \big( \dot{\bm{r}}= \bm{v} \big) \), where \( \bm{r} \) is the position vector:
\begin{align} 
	\dot{\bm{r}}	&= 	\bm{v} \,, \label{GenEoMRocket_r}\\
	\dot{\bm{v}}	&=  \dfrac{\bm{F}_{ext}\big( \bm{r}, \bm{v} \big)}{m}	+  \dfrac{ \bm{T} }{m} \,, \label{GenEoMRocket_v} \\
	\dot{m} 		&= 	- \dfrac{ \bigl\| \bm{T} \bigr\| }{ v_e } \,, \label{GenEoMRocket_m}
\end{align}
where the generic force \(\bm{F}_{ext}(\cdot)\) is representative of external sources of acceleration such as gravitational forces, aerodynamic forces, or fictitious forces introduced by a non-inertial reference frame, and the norm of the effective exhaust velocity vector, \( v_e = \|\bm{v}_e\| \), is a known constant for a given vehicle. \par

% Most often the thrust vector represents the control of the system, \( \bm{T}(t) = \bm{u}(t)\), but in some cases the thrust can be defined as a state instead. This modification is sometimes used for a better modelling of the behaviour of the engine and it implies appending the additional equation \( \dot{\bm{T}}(t) = \bm{u}(t)\) to the equations of motion \eqref{GenEoMRocket_r} through \eqref{GenEoMRocket_m}. This way the thrust is modelled as a state (differential variable) and the \emph{thrust-rate} is modelled as a control (algebraic variable). \par

Equations \eqref{GenEoMRocket_r} to \eqref{GenEoMRocket_m} presume a point-mass approximation of the system, this is to say that these equations overlook some aspects of rocket behaviour. For instance, this model does not encompass the attitude control of the vehicle, and therefore the angle of attack is assumed to be zero at all times. More refined approximations imply the extension of the state vector and the consequent appending of additional equations of motion.


\subsection{Trajectory Optimization}

Before every launch, a trajectory should be generated in order to minimize the fuel consumption of the vehicle. This is where the discipline of optimal control comes in.

Optimizing multi-staged rocket trajectories is not an easy task. The difficulties include:
\begin{stdlist}
	\item The nonlinear nature of the equations of motion.
	\item The modelling of physical phenomena, such as drag which is a function of the relative air velocity.
	\item The mutation of the equations of motion from one stage to the next due to the varying of the exhaust velocity, \(v_e\), or, possibly, other terms.
	\item The state discontinuities that occur at stage separation events from abrupt mass detachments.
	\item The fact that every trajectory optimization problem is a distinct boundary-value problem.
\end{stdlist}

In generic terms, a dynamic system that is subject to non-differentiable points can be subdivided into different \emph{phases} corresponding to periods of time along which the system is fully differentiable. This way, each phase is modelled with the appropriate set of dynamic equations and the non-differentiable points correspond to phase transition points. If there are multiple non-differentiable points then one can subdivide the problem into multiple phases. Always with the assumption that the transitions between phases are instantaneous. Trajectory optimization problems of this sort are called \emph{multiphase optimal control problems}.

In the particular case of multi-staged rockets, it is appropriate to subdivide the problems into phases according to the stage separation events. This way one stage is associated with one phase and stage separation events correspond to phase transition points. In the context of this work, the terms \emph{stage} and \emph{phase} might be used interchangeably.

The challenges described above are the motivation for the development of a multiphase numerical algorithm. There must be a trajectory generation utility capable of dealing with such systems. In addition, this utility would need to be generic instead of problem-specific in order to be useful for both current and future missions.


\subsubsection{Path constraints}

In addition to the difficulties expressed above, the vehicle might be subject to constraints due to design requirements or safety factors.
For instance, it might be necessary to keep the aerodynamic pressure below a certain value along the trajectory, or it might be required to limit the acceleration experienced by the vehicle to an acceptable interval, or, still, the trajectory might be physically conditioned by no-fly zones. These types of constraints are called \emph{path constraints} \citep{Bryson1975,Betts2010} and they can be though of as an algebraic equation (or inequality) that must be verified at each point in time. Path constraints are expressed as
\begin{equation}	\label{GenPathConst}
	\bm{h} \bigl( \bm{x}(t), \bm{u}(t) \bigr) \leq \bm{0} \,.
\end{equation}

For reference, other examples of path constraints commonly used are:
\begin{stdlist}
	\item To assert a constant norm of the thrust vector along the trajectory.
	\item To ensure that the norm of the position vector is greater than or equal to the radius of the Earth.
\end{stdlist}

\subsubsection{Event constraints}

\emph{Event constraints} are a set of equations (or inequalities) that must be satisfied at the boundaries of the problem, that is, at the initial and/or final times of each phase. These are also referred to as \emph{boundary conditions} or \emph{point constraints} \citep{Rao2010,Patterson2014,Betts2010}. \par 

A commonly used set of event constraints in the case of \glspl{olv} is, for example, the assertion of orbital elements at the final time of the trajectory. When finding the optimal trajectory to a desired orbit, it is possible to assert the values of five out of the six orbital elements in order to leave one unknown, usually the true anomaly, as a degree of freedom. This is very convenient as, most often, the true anomaly of the spacecraft at the point of orbit insertion is not a relevant factor. \par

Event constraints are expressed as
\begin{equation}	\label{GenTermConst}
	\bm{\phi} \bigl(\bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr) \leq \bm{0} \,.
\end{equation}

The equations of motion, together with the path constraints and the event constraints dictate feasibility. The state and control sequences that satisfy these constraints form a feasible solution, not necessarily an optimal solution. \par


\subsubsection{The performance measure}

The performance measure is a scalar quantity indicator of optimality \citep{Bryson1975,Naidu2003,Kirk2004,Lewis2012,Betts2010}. In optimal control, this quantity is conventionally expressed as a \emph{cost functional}, \(\mathcal{J}\), this way the optimal trajectory is found by minimizing (or maximizing) the cost. The cost functional is expressed as \citep{Bryson1975,Naidu2003,Kirk2004,Lewis2012,Betts2010}
\begin{equation} \label{GenCostIntro}
\mathcal{J} = \ovbrace{\vphantom{\Bigg(} \unbrace{\vphantom{\Bigg(}\Phi \bigl(\bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr)}{Mayer Term} + \nint{t_0}{t_f} \unbrace{\vphantom{\Bigg(}\Psi \bigl( \bm{x}(t), \bm{u}(t) \bigr)}{Lagrange Term} \dd{t} }{Bolza Problem} \,.
\end{equation}

The Bolza problem is the standard formulation of a generic optimal control problem, standing as the sum of an integral functional of the continuous problem and a function evaluated at the boundaries \citep{Naidu2003,Betts2010}. If the boundary term is omitted, \( \Phi \equiv 0\), the problem is known as the \emph{Lagrange problem}, and if the integrand term is omitted, \( \Psi \equiv 0\), the problem is known as the \emph{Mayer problem} \citep{Naidu2003,Betts2010}. Most often it is possible to express a Lagrange problem as a Mayer problem (and vice-versa).

The performance measure is the parameter that dictates optimality: a trajectory that minimizes time will, most likely, be different to one that minimizes energy, yet both are optimal trajectories. Therefore, the process of finding the optimal trajectory begins by choosing the optimal performance measure. In the case of \glspl{olv}, the performance measure will often consist on maximizing the final mass of the vehicle. \par 


\section{Optimal Control and Trajectory Optimization Methods}
\label{section:state_art}

%% Rocket trajectory optiization is an optimal control problem.
Rocket trajectory optimization is profoundly tied to optimal control. Since launch vehicle technology developed (roughly during the second half of the twentieth century) the optimization of rocket trajectories has been made through Pontryagin's Minimum principle \citep{Lawden1963,Vinh1973}. The terms "trajectory optimization" and "optimal control" are so much tied together that they can often be used interchangeably \citep{Betts1998}. In particular, the term \emph{primer vector} was coined specifically in the context of rocket trajectory optimization \citep{Lawden1963,Vinh1973}. The primer vector is the complementary vector associated with velocity \citep{Lawden1963,Vinh1973,Conway2010}. It has been shown that, for final-mass-maximization problems, one of the optimality conditions is for the thrust vector to be collinear with the primer vector \citep{Lawden1963,Vinh1973,Conway2010}. The complementary vectors (or \emph{covectors}, \emph{costates}, \emph{adjoined vectors}, or, still, \emph{dual variables}) are auxiliary variables that serve a similar purpose to that of Lagrange multipliers in parametric optimization problems. \par 

%% Introduction to Optimal Control
Given a constrained dynamic system, the subject of optimal control is concerned with finding the control sequence that will promote a desired state transformation and which will minimize (or maximize) an associated performance measure \citep{Bryson1975,Naidu2003,Kirk2004,Betts2010,Conway2010,Lewis2012}. Optimal control is a multidisciplinary subject that exists formally for more than three hundred years \citep{Sussmann1997}, although it is not unreasonable to assume that this discipline has been present in the collective human mind long before that in the shape of informal thoughts. \par 

%% Classical Optimal Control and indirect methods
The classical approach to solve optimal control problems is through the calculus of variations, leading to the Hamiltonian boundary-value problem \citep{Bryson1975,Kirk2004}. Most often analytical solutions cannot be derived, and thus numerical methods are employed. Until the beginning of the twenty-first century indirect methods, such as single shooting and multiple shooting methods, were standard for solving trajectory optimization problems, including rocket and spacecraft trajectories \citep{Pontryagin1962,Lawden1963,Bryson1975,Kirk2004,Athans2006}. \par 

But indirect methods are often impractical due to the associated necessity of deriving the problem-wise optimality conditions \citep{Betts2010}. In addition, these methods most often comprise convergence issues \citep{Betts2010,Benson2006}, and thus, lack reliability when a good initial guess is not available.
On top of this, indirect methods are inadequate for problems with inequality path constraints: typically the time intervals relative to the active and the inactive constraints must be known a priori so that the problem can be manually divided into corresponding phases \citep{Betts2010}. Ultimately, these issues make indirect methods hostile to employ in a general purpose software application. \par 

%% Direct methods and pseudospectral methods
In contrast, direct methods, and in particular pseudospectral methods, have gained popularity over the past decades since their introduction by Elnagar \textit{et al.}~\citep{Elnagar1995} and their generalization by Ross, Fahroo \textit{et al.}~\citep{Fahroo2001, Fahroo2002, Ross2003, Gong2007}. Pseudospectral methods consist on the transcription of the dynamics based on the roots of an orthogonal polynomial, typically a particular Jacobi polynomial \citep{Fornberg1996,Rao2010}. The constraints are evaluated at each collocation point resulting in a large list of equality and inequality algebraic constraints, accompanied with the algebraic objective function; this list of constraints, referred to as a nonlinear programming (\gls{nlp}) problem, is passed through an \emph{off-the-shelf} nonlinear optimizer such as SNOPT~\citep{Gill2017} or IPOPT~\citep{Waechter2005,Kawajir2015} and the solution is obtained. In essence, in a pseudospectral method the optimal control problem is transformed into a parametric optimization problem. \par

%% Covector mapping theorem
The solution obtained from a pseudospectral method is computed with disregards to the first order optimality conditions from the Hamiltonian Boundary-Value Problem. Instead, the set of necessary conditions associated with the \gls{nlp} is employed, called the Karush–Kuhn–Tucker conditions \citep{Benson2006}, resulting in some loss of information with regards to the original optimal control problem \citep{Ross2003,Fahroo2008}.
In order to make sure that the solution is optimal, the first order necessary conditions are verified in post processing with the aid of the covector mapping theorem (\gls{cmt}) \citep{Ross2003, Ross2005, Gong2008, Fahroo2008}, thus "double checking" the optimality of the solution. Finally, having the discrete solution, the continuous representation can be obtained by use of Lagrange interpolating polynomials \citep{Sagliano2017a}. Pseudospectral methods can also be referred to as \emph{orthogonal collocation methods} and they are known to converge spectrally~\citep{Trefethen2000,Huntington2008,Ross2003}. \par 

Since the introduction of pseudospectral methods there have been multiple iterations and innovations \citep{Benson2004, Ross2006, Benson2006, Fahroo2008, Garg2011}, including their implementation into several software tools such as GPOPS and GPOPS-II~\citep{Rao2010, Patterson2014}, DIDO~\citep{Fahroo2001,Bollino2006phd}, PSOPT~\citep{Becerra2010}, ICLOCS2~\citep{Nie2018}, \gls{spartan}~\citep{Sagliano2013,Huneker2015,Sagliano_PhD,Sagliano2016,Arslantas2016,Sagliano2017a,Sagliano2017d} among others. One notable instance of the application of pseudospectral methods into the real world was the famous \textit{zero-propellant manoeuvre} of the \gls{iss} where a large angle reorientation of the spacecraft was performed by aid of reaction wheels and gravity gradient only, virtually no propellant was spent \citep{Bedrossian2009}. Direct methods are widely used because they can accommodate a broader range of problem formulations than indirect methods and lack their drawbacks. \par

%% Jacobian Matrix
The \gls{nlp} problem passed to the nonlinear solver via a pseudospectral method requires the aid of an associated Jacobian matrix. Because most optimal control software packages are general purpose, they must either
\begin{enumerate*} [label=\upshape(\roman*\upshape)]
	\item require the user for an analytic Jacobian matrix which can quickly become an exhausting process, especially for larger and more complicated problems, or
	\item the software packages provide the Jacobian matrix via an arbitrary numerical method.
	\end{enumerate*}
For convenience to the user, the second alternative is most often provided. Because of this, these software packages require numerical differentiation methods to compute the partial derivatives of the constraints with respect to the variables and ultimately assemble the Jacobian matrix. In the context of optimal control, methods such as the \emph{complex step} method and the \emph{Dual number} method have been employed to some extent \citep{Alonso2003, DOnofrioThesis2014, DOnofrio2015, Sagliano2017c, Agamawi2020}. \par 

%% Scaling
Because generality is desired, a scaling process is often applied to the \gls{nlp} as a way to normalize the magnitude of the algebraic constraints. One effective scaling method is to factor each constraint by the magnitude of the respective Jacobian matrix row \citep{Rao2009,Sagliano2014,Sagliano2017c}. Another scaling method has been proposed recently which is based on a balancing technique between primal and dual variables \citep{Ross2018}. Despite not introducing coupling between the constraints, the scaling does affect the resulting output dual variables. This means that the \gls{nlp} needs to be rescaled back in post-processing before the output of the final solution and also before the covector mapping. \par

%% LG, LGR and LGR ps methods
Within pseudospectral methods, the three most commonly used techniques are the Legendre-\-Gauss pseudospectral method, the Legendre-\-Gauss-\-Lobatto pseudospectral method and the Legendre-\-Gauss-\-Radau pseudospectral method (direct or flipped), all of which have been properly formalized \citep{Fahroo2008}. The roots of Legendre polynomials sit invariably within the interval \( [ -1, +1 ] \). The position of the roots changes slightly from method to method, but the most relevant difference between the methods is the inclusion (or exclusion) of the interval endpoints (-1) or (+1). The roots of the Legendre-\-Gauss polynomial do not include any of the endpoints; the roots of the Legendre-\-Gauss-\-Lobatto polynomial include both interval endpoints; and the roots of the Legendre-\-Gauss-\-Radau polynomial include either the left endpoint (-1) or the right endpoint (+1) of the interval. \par

%% LG and LGL disadvantages
The roots of the Legendre-Gauss polynomial do not include the endpoints of the interval, because of this, the respective method by itself does not allow for endpoint constraints, in order to include endpoint constraints, one must add an auxiliary quadrature constraint for artificial collocation of the terminal (or initial) endpoint \citep{Rao2010}. This extra constraint constitutes an additional burden to the \gls{nlp}, making the problem slightly, but unnecessarily, more complex.
The Legendre-Gauss-Lobatto method provides a solution to this because, without the necessity of additional constraints, the method allows both endpoints of the domain to be constrained. However, it has been shown that this method has convergence issues with respect to the costates \citep{Fahroo2008,Garg2011} which compromises the calculation of the first order optimality conditions.
Finally, the Legendre-Gauss-Radau method (direct or flipped), due to the asymmetric distribution of collocation points, encompasses the best of both worlds by allowing the domain endpoints to be constrained without sacrificing convergence of the costates \citep{Garg2011}. \par 

%% Integral and differential forms
With regards to pseudospectral methods, there have been efforts to develop the so-called \emph{integral formulation} of the \gls{nlp} as an alternative to the typical differential form
\citep{Benson2004,Garg2010,Garg2011paper,Patterson2014}. It has been shown that the choice of the differential form over the integral form or vice-versa is arbitrary for both the Legendre-Gauss pseudospectral method and the Legendre-Gauss-Radau pseudospectral method (direct or flipped). However, this is not the case for the Legendre-Gauss-Lobatto pseudospectral method due to the structure of the respective differentiation matrix. \par 

%% Multiple phase integration
Regarding multiphase optimal control, successful efforts have been made in order to implement the feature into software \citep{Rao2010, Patterson2014}. Multiphase optimal control allows the study of trajectories that might contain necessary state discontinuities or even mutations of the equations of motion. Figure \ref{fig:MultiStageRocket} illustrates the launch sequence of a vehicle with two stages. It is evident that the stage separation event introduces a discontinuity of mass into the system. In order to generate trajectories of orbital launchers it is relevant to develop a multiphase algorithm. This way state discontinuities can be accounted for. \par

\begin{figure}[ht]
	\centering
	\includegraphics[height=50mm]{Introduction/MultiStageRocket.pdf}
	\caption{Simplified launch sequence illustration of a two staged rocked.}
	\label{fig:MultiStageRocket}
\end{figure}

%% Mesh Refinement Methods
Other developments in pseudospectral optimal control have taken place, most notably \emph{h} and \emph{hp} mesh refinement methods \citep{Darby2009,Sagliano2019,Koeppen2019,Agamawi2020}. In a \emph{h} method the number of polynomials to be concatenated along the domain is increased, while in a \emph{p} method the degree of the polynomial in use is increased. \emph{hp} methods are a combination of the two. The implementation of \emph{p} mesh refinement methods is often superfluous, as it is more satisfying to manually modify the degree of the polynomial at rerun whenever a solution is not considered accurate enough. On the other hand \emph{h} and \emph{hp} methods can be powerful, however, the respective implementation can be laborious and a refinement strategy is required. Other mesh refinement methods have been proposed which are based on discontinuities in the optimal control \citep{Gong2008a,Eide2018pt1,Eide2018pt2}, this is relevant because the control discontinuity points are ideal to concatenate adjacent mesh intervals. \par

%% In this thesis...
In this work, the flipped Radau pseudospectral method is employed to generate optimal trajectories of multi-stage rockets in both ascension to orbit and descent to vertical landing. The Legendre-Gauss-Radau method is well suited because it is a pseudospectral method that allows endpoint collocation, and it does not present dramatic convergence issues on the dual variables \citep{Garg2011}. The foundation for the implementation of the multiphase feature is \gls{spartan} \citep{Sagliano2017a}, a MATLAB tool developed at \gls{dlr} --- Deutsches Zentrum für Luft- und Raumfahrt --- with the objective of solving general purpose optimal control problems. \par


