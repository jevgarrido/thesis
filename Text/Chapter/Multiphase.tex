
\chapter{Implementation of the Flipped Radau Method for Multiphase Problems}
\label{chapter:multiphase}

\section{Multiphase Optimal Control Problem}
\label{sec:mp_ocp}

In this Section, the generic multiphase optimal control problem is presented. The formulation is based on the problem of Section~\ref{sec:ocp}, which can be interpreted as a single phase problem. The purpose of a generic multiphase problem formulation is to account for cases in which state discontinuities are expected.
In the context of rocket launchers composed of multiple stages, state discontinuities are expected at stage separation events.

Letting $p \in [1,\dots,P]$ be a scalar integer indicating a specific phase, where $P$ is the total number of phases in the problem, a generic multiphase optimal control problem can be formulated as minimizing the cost functional
\begin{equation} 	\label{MP_OCPGenCost}
	\mathcal{J} = \Phi \bigl( \bm{x}(t{\scriptstyle _0^{(1)} }), t{\scriptstyle _0^{(1)} }, \bm{x}( t{\scriptstyle _f^{(P)} }), t{\scriptstyle _f^{(P)} } \bigr) +
	\sum_{p=1}^{P} \Biggl\{ \int_{t_0^{(p)}}^{t_f^{(p)}} \Psi \bigl( \bm{x}(t), \bm{u}(t) \bigr) \dd{t} \Biggr\} \,,
\end{equation}
associated with the trajectory of the dynamic system
\begin{equation}	\label{MP_OCPGenEoM}
	\dot{\bm{x}}^{(p)} (t) = \bm{f}^{(p)} \bigl( \bm{x}(t), \bm{u}(t) \bigr) , \quad \quad p = 1,\dots, P \,,
\end{equation} 
subject to the path constraints
\begin{equation}	\label{MP_OCPGenPathConst}
	\bm{h}^{(p)} \bigl( \bm{x}(t), \bm{u}(t) \bigr) \leq \bm{0} , \quad \quad p = 1,\dots, P \,,
\end{equation}
the event constraints
\begin{equation}	\label{MP_OCPGenBoundaries}
	\bm{\phi}^{(p)} \bigl( \bm{x}(t{\scriptstyle _0^{(p)} }), t{\scriptstyle _0^{(p)} }, \bm{x}( t{\scriptstyle _f^{(p)} }), t{\scriptstyle _f^{(p)} } \bigr) \leq \bm{0} , \quad \quad p = 1,\dots, P \,,
\end{equation}
and the phase linkage conditions
\begin{equation}	\label{MP_OCPGenLinkage}
\Delta \bm{x}_\text{min}^{(p)} \: \leq \: \fLink^{(p)} = \bm{x}(t{\scriptstyle _0^{(p+1)} }) - \bm{x}(t{\scriptstyle _f^{(p)} }) \: \leq \:  \Delta \bm{x}_\text{max}^{(p)} , \quad \quad p = 1,\dots, P-1 \,,
\end{equation}
where $ \Delta \bm{x}_\text{min}^{(p)} $ and $ \Delta \bm{x}_\text{max}^{(p)} $ are, respectively, the user-defined lower and upper boundary vectors for the linkage conditions. The functions presented in \eqref{MP_OCPGenCost} through \eqref{MP_OCPGenLinkage} are defined by the following mappings:
\begin{alignat}{4}
&\Phi &&: \mathbb{R}^{n_{\bm{x}}} \times \mathbb{R} &&\times \mathbb{R}^{n_{\bm{x}}} \times \mathbb{R} \quad &&\to \mathbb{R} \,, \\
&\Psi &&: \mathbb{R}^{n_{\bm{x}}} \times \mathbb{R}^{n_{\bm{u}}} && \quad &&\to \mathbb{R} \,, \\
&\bm{f}^{(p)} &&: \mathbb{R}^{n_{\bm{x}}} \times \mathbb{R}^{n_{\bm{u}}} && \quad &&\to \mathbb{R}^{n_{\bm{x}}} \,, \\
&\bm{h}^{(p)} &&: \mathbb{R}^{n_{\bm{x}}} \times \mathbb{R}^{n_{\bm{u}}} && \quad &&\to \mathbb{R}^{n_{\bm{h}}^{(p)}} \,, \\ 
&\bm{\phi}^{(p)} &&: \mathbb{R}^{n_{\bm{x}}} \times \mathbb{R} &&\times \mathbb{R}^{n_{\bm{x}}} \times \mathbb{R} \quad &&\to \mathbb{R}^{n_{\phi}^{(p)}} \,, \\
&\fLink^{(p)} &&: \mathbb{R}^{n_{\bm{x}}} \times \mathbb{R}^{n_{\bm{x}}} && \quad &&\to \mathbb{R}^{n_{\bm{x}}} \,,
\end{alignat}
where $n_{\bm{x}}$ is the dimension of the state vector, $n_{\bm{u}}$ is the dimension of the control vector, $n_{\bm{h}}^{(p)}$ is the number of simultaneous path constraints on phase $p$, and $n_{\bm{\phi}}^{(p)}$ is the total number of event constraints of phase $p$. This formulation indicates that the constraints are not explicit functions of time, therefore, the systems in study are time invariant. Also, the Mayer term and the Lagrange term are not expected to change from phase to phase, which is shown by the absence of the superscript "\((p)\)" which would be attached to these functions in \eqref{MP_OCPGenCost}. Also, the state and control vectors must not change their sizes from phase to phase, again this is shown by the absence of the corresponding superscript "\((p)\)" in $n_{\bm{x}}$ and $n_{\bm{u}}$.

Equation~\eqref{MP_OCPGenLinkage}, which corresponds to the phase linkage conditions, consists of two distinct constraints, the upper bound and the lower bound of the eventual state discontinuities. This allows the specification of several discontinuity scenarios. Table~\ref{tab:linkage_scenarios} presents three possible scenarios of phase linkage conditions in order to describe a given state over a phase transition. The vector $\bm{a}$ represents a generic array that is arbitrary (user-defined), and the phase indicator, $p$, is omitted for syntax simplicity.
\begin{table}[hbt!]
\stdtable
\caption{Three different scenarios of phase linkage conditions in a given phase $p$.}
\label{tab:linkage_scenarios}
\begin{tabular}{@{}lLL@{}}
	\toprule
    & \text{Relationship between } \Delta \bm{x}_\text{min} \text{ and } \Delta \bm{x}_\text{max} & \text{Note} \\
    \midrule
	1. Continuity & \Delta \bm{x}_\text{min} = \Delta \bm{x}_\text{max} = 0 & \\
	2. Known discontinuity   & \Delta \bm{x}_\text{min} = \Delta \bm{x}_\text{max} = \bm{a}  & \bm{a} \in \mathbb{R}^{n_{\bm{x}}} \\
	3. Unknown discontinuity & \Delta \bm{x}_\text{min} < \Delta \bm{x}_\text{max} & \Delta \bm{x}_\text{min}, \Delta \bm{x}_\text{max} \in \mathbb{R}^{n_{\bm{x}}} \\
	\bottomrule
\end{tabular}
\end{table}

It is possible to enforce a continuous state, a known discontinuity or an unknown discontinuity. For instance, in a stage separation event where the ejected mass is a known parameter, $ m_\text{ejected} = \text{known} $, the linkage condition for mass would be 
\begin{equation}
	\Delta m_\text{min} = \Delta m_\text{max} = - m_\text{ejected} \,,
\end{equation}
thus enforcing a known discontinuity of mass at that point (case 2 in Table~\ref{tab:linkage_scenarios}). It is up to the user to decide what the values $ \Delta \bm{x}_\text{min} $ and $ \Delta \bm{x}_\text{max} $ should be in order to best represent a given problem.

\section{Multiphase NLP and Covector Mapping}

Following the reasoning exposed in Section \ref{sub:nlp} for the single phase case, the \gls{nlp} for multiphase problems can be easily worked out. The multiphase \gls{nlp} is quite similar to the single phase counterpart in that the multiphase \gls{nlp} is just an aggregate of several single phase problems. The main difference is that the multiphase \gls{nlp} contains additional \emph{phase linkage conditions} required to relate one phase to the next. In a multiphase problem state discontinuities are expected, and so the phase linkage conditions are important in order to relate the states at the endpoints of each phase. The multiphase \gls{nlp} can be expressed as minimizing the cost 
\begin{equation} \label{eq:mp_nlp_cost}
	\mathcal{J}^N = \Phi + \sum_{p=1}^{P} \dfrac{ t{\scriptstyle _f ^{(p)}} - t{\scriptstyle _0^{(p)}} }{2} \sum_{k}^{ N^{(p)} } w_k^{(p)}  \Psi_k \,, 
\end{equation}
subject to:
\begin{align}
	C_{\lambda,k}^{(p)} \biggl[ \bm{f}_k^{(p)} - \dfrac{2}{ t{\scriptstyle _f ^{(p)}} - t{\scriptstyle _0^{(p)}} } \sum_i \bm{D}_{ki}^{(p)} \bm{x}_i^{(p)} \biggr] &= \bm{0} \,, \label{eq:mp_nlp_defects} \\
	C_{\mu, k}^{(p)} \bm{h}_k^{(p)} &\leq \bm{0} \,, \label{eq:mp_nlp_path} \\
	\bm{\phi}^{(p)} &\leq \bm{0} \,, \label{eq:mp_nlp_event} \\
	\Delta \bm{x}_\text{min}^{(p)}  \leq \fLink^{(p)}  &\leq \Delta \bm{x}_\text{max}^{(p)} \,, \label{eq:nlp_link}
\end{align}
where \( N^{(p)} \) is the number of \emph{collocation points} used to discretize phase $p$.

Similarly, the covector mapping can also be generalized for multiphase based on the single phase formulation presented in Section \ref{sec:covector_mapping}. Multiphase covector mapping can be written as:
\begin{align}
	\bm{\lambda}_k^{(p)} &= \dfrac{ 2 C_{\lambda, k}^{(p)} }{(t{\scriptstyle _f ^{(p)}} - t{\scriptstyle _0^{(p)}})w_k}  
	\tilde{\bm{\lambda}}_k^{(p)} \,, \label{eq:mp_CMT_lambda} \\
	\bm{\mu}_k^{(p)} &= \dfrac{ 2 C_{\mu, k}^{(p)} }{(t{\scriptstyle _f ^{(p)}} - t{\scriptstyle _0^{(p)}})w_k} 
	 \tilde{\bm{\mu}}_k^{(p)} \,, \label{eq:mp_CMT_mu} \\
	\bm{\nu}^{(p)} &= \tilde{\bm{\nu}}^{(p)} \label{eq:mp_CMT_nu} \,.
\end{align}
Again, the superscript $p = 1,2, \dots, P$ serves as an ordinal indicator of a given phase, in a problem with $P$ sequential phases. Figure~\ref{fig:MP_CollocationNodes} illustrates the pattern of collocation nodes for a discretization of the domain composed of multiple phases using the flipped Radau pseudospectral method. The multiphase discretization scheme is a simple concatenation of several "single phase" schemes. It can be noticed that the "density" of nodes increases near the phase transition points.
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.9\textwidth]{ChapterIII/MultiphaseNodes.pdf}
	\caption{Illustration of the pattern of collocation nodes for multiple phase transcription. Example of three phases with five collocation nodes in each phase.}
	\label{fig:MP_CollocationNodes}
\end{figure}

\subsection{Scalar set selection for multiphase covector mapping and simplified NLP}
\label{sub:mp_nlp_selection}

In this work the \emph{minimal mapping set} is used because it does not require dedicated pre-processing tasks. Further, this mapping seems to be the standard in literature \citep{Ross2003,Rao2010}. The multiphase version of the minimal mapping set from Table~\ref{tab:nlp_scalars} is shown in Table~\ref{tab:choice_covector_scalars}.
\begin{table}[hbt!]
\stdtable
\caption{Multiphase scalar factors for the \emph{minimal} costate mapping.}
\label{tab:choice_covector_scalars}
\begin{tabular}{@{}lCC@{}}
	\toprule
		& C_{\lambda, k}^{(p)} & C_{\mu, k}^{(p)} \\
	\midrule
	Choice of scalar factors for costate mapping & \dfrac{t{\scriptstyle _f^{(p)}} - t{\scriptstyle _0^{(p)}} }{2} & 1  \\
	\bottomrule
\end{tabular}
\end{table}

The format of the minimal mapping scalar factors for multiphase is similar to the case of single phase seen in Table~\ref{tab:nlp_scalars}, with the added superscript "\((p)\)" to the variables $t_0$ and $t_f$ in order to specify the phase, as each phase contains its own pair of initial and final times. With the mapping selection shown in Table~\ref{tab:choice_covector_scalars} the constraints from \eqref{eq:mp_nlp_defects} and \eqref{eq:mp_nlp_path} can be simplified to:
\begin{align}
	\Ddyn_k\pp = \dfrac{ t{\scriptstyle _f ^{(p)}} - t{\scriptstyle _0^{(p)}} }{2} \bm{f}_k^{(p)} - \sum_i \bm{D}_{ki}^{(p)} \bm{x}_i^{(p)} &= \bm{0} \,, \label{eq:mp_spec_Ddyn} \\
	\bm{h}_k^{(p)} &\leq \bm{0} \,, \label{eq:mp_spec_path}
\end{align}
where the vector $ \Ddyn_k\pp $ is a shortened representation of the constraints for dynamic defects from \eqref{eq:mp_spec_Ddyn} for phase $ p $. Equations~\eqref{eq:mp_CMT_lambda} and \eqref{eq:mp_CMT_mu} will also be subject to simplifications once the scalars $C_{\lambda, k}^{(p)}$ and $C_{\mu, k}^{(p)}$ are substituted with the respective values from Table~\ref{tab:choice_covector_scalars}, these equations become, respectively:
\begin{align}
	\bm{\lambda}_k^{(p)} &= \dfrac{ \tilde{\bm{\lambda}}_k^{(p)} }{ w_{ k{\scriptstyle ^{(p)}} } } \,, \label{eq:mp_simple_CMT_lambda} \\
	\bm{\mu}_k^{(p)} &= \dfrac{ 2 }{(t{\scriptstyle _f ^{(p)}} - t{\scriptstyle _0^{(p)}})} \dfrac{\tilde{\bm{\mu}}_k^{(p)}}{w_{ k{\scriptstyle ^{(p)}} }} \,, \label{eq:mp_simple_CMT_mu} 
\end{align}

Ultimately, the discrete form of the cost functional expressed in \eqref{eq:mp_nlp_cost} and the discrete form of  the constraints expressed in \eqref{eq:mp_nlp_event}, \eqref{eq:nlp_link}, \eqref{eq:mp_spec_Ddyn} and \eqref{eq:mp_spec_path} are the ones to send to the nonlinear solver.

\section{Vector Formatting and Jacobian Matrix}
\label{sec:formatting}

\subsection{Input format of the nonlinear solver}

In order to solve an optimal control problem via the Radau pseudospectral method, one needs to transcribe the original problem statement into a format which a nonlinear solver can interpret. A nonlinear program is expressed as~\citep{Gill2017,Waechter2005,Kawajir2015} minimizing
\begin{equation}
	J(\bm{X}_{\text{NLP}}) \,,
\end{equation}
subject to
\begin{align}
	\bm{C}_\text{min} \leq \bm{C}(\bm{X}_{\text{NLP}})  &\leq \bm{C}_\text{max} \,, \\
	\bm{X}_\text{min} \leq \mask{\bm{C}(\bm{X}_{\text{NLP}})}{\bm{X}_{\text{NLP}}} &\leq \bm{X}_\text{max} \,.
\end{align}
where $\bm{C}_\text{max}$ and $\bm{C}_\text{min}$ are, respectively, the upper and lower boundary vectors of the constraints, and $\bm{X}_\text{max}$ and $\bm{X}_\text{min}$ are the upper and lower boundary vectors of the decision variables, respectively.
Essentially, there is a cost function, \( J(\bm{X}_{\text{NLP}}) \), to be minimized, a list of algebraic constraints, \(\bm{C}(\bm{X}_{\text{NLP}})\), to be satisfied and a list of parameters, \(\bm{X}_{\text{NLP}}\), that act as decision variables. In order to transform the optimal control problem into a \gls{nlp} it is necessary to concatenate all the decision variables into a main vector and to aggregate all constraints into a main constraint vector, this is depicted by vectors $\bm{X}_{\text{NLP}}$ and $\bm{C}(\bm{X}_{\text{NLP}})$ respectively. The algebraic constraints contained in $\bm{C}(\bm{\bm{X}_{\text{NLP}}})$ can assert either an equality or an inequality by setting $\bm{C}_\text{max} = \bm{C}_\text{min} $ or $\bm{C}_\text{max} > \bm{C}_\text{min}$ respectively.

\subsection{Formatting the vector of decision variables}

In order to construct a nonlinear programming problem one needs both, a list of decision variables and a list of algebraic constraints. Let
\begin{equation}
    \bm{XU}^{(p)} = \begin{bmatrix}
    \transpose{ \bm{x}_0^{(p)} } &
    \transpose{ \bm{x}_1^{(p)} } & \transpose{ \bm{u}_1^{(p)} } & \dots &
    \transpose{ \bm{x}_i^{(p)} } & \transpose{ \bm{u}_i^{(p)} } & \dots & 
    \transpose{ \bm{x}_{N^{(p)}}^{(p)} } & \transpose{ \bm{u}_{N^{(p)}}^{(p)} } \end{bmatrix} ^\intercal \,,
\end{equation}
be the concatenated decision vector of state and control associated with the nodes of a given phase $p$. Notice that there is no control variable associated with the very first node $i = 0$, this is because this node is not collocated \citep{Sagliano2017c}. The resulting size of this column vector is $[n_{\bm{x}} + N^{(p)}(n_{\bm{x}}+n_{\bm{u}})] \times 1$.

The complete vector of decision variables can be expressed as:
\begin{equation} \label{eq:XNLP}
    \bm{X}_{\text{NLP}} = \begin{bmatrix}
    	\transpose{\bm{XU}^{(1)}} & \dots & \transpose{\bm{XU}^{(p)}} & \dots & \transpose{\bm{XU}^{(P)}} & t_f^{(1)} & \dots & t_f^{(p)} & \dots & t_f^{(P)}
    \end{bmatrix} ^\intercal 
\end{equation}
resulting in a column vector of size $\bigl[ \sum_{p = 1}^{P} [ n_{\bm{x}} + N^{(p)}(n_{\bm{x}}+n_{\bm{u}}) ] + P \bigr] \times 1$. The lower and upper boundaries of the vector of decision variables, $\bm{X}_\text{min}$ and $\bm{X}_\text{max}$ can be build according to an analogous procedure, given the parameters specified by the user for each problem.

\subsection{Constraints formatting}

The cost functional fed to the nonlinear solver is identical to the cost described in \eqref{eq:mp_nlp_cost}, thus,
\begin{equation}
	J(\bm{X}_{\text{NLP}}) = \mathcal{J}^N \,.
\end{equation}
In order to construct the concatenated vector, $ \bm{C}(\bm{X}_{\text{NLP}}) $, two additional arrays are introduced, namely $\bm{F}^{(p)}$ and $\bm{H}^{(p)}$, containing all the constraints corresponding to phase $p$ from \eqref{eq:mp_spec_Ddyn} and \eqref{eq:mp_spec_path}:
\begin{align}
	\bm{F}^{(p)} &= \begin{bmatrix}
	\mask{\transpose{ \bm{h}_1^{(p)} }}{\transpose{ \Ddyn_1^{(p)} }} & \mask{\transpose{ \bm{h}_2^{(p)} }}{\transpose{ \Ddyn_2^{(p)} }} & \dots & \mask{\transpose{ \bm{h}_k^{(p)} }}{\transpose{ \Ddyn_k^{(p)} }} & \dots & \mask{\transpose{ \bm{h}_{N^{(p)}}^{(p)} }}{\transpose{ \Ddyn_{N^{(p)}}^{(p)} }}
	\end{bmatrix}^\intercal
	\\
	\bm{H}^{(p)} &= \begin{bmatrix}
	\transpose{ \bm{h}_1^{(p)} } & \transpose{ \bm{h}_2^{(p)} } & \dots & \transpose{ \bm{h}_k^{(p)} } & \dots & \transpose{ \bm{h}_{N^{(p)}}^{(p)} }
	\end{bmatrix}^\intercal
\end{align}
where \(\Ddyn_k^{(p)}\) and \(\bm{h}_k^{(p)}\) are as in \eqref{eq:mp_spec_Ddyn} and \eqref{eq:mp_spec_path}, respectively. This way, the concatenated vector of constraints can be written as 
\begin{equation}
    \bm{C}(\bm{\bm{X}_{\text{NLP}}}) = 
   	\begin{bmatrix}
    \transpose{ \bm{F}^{(1)} } & \dots & \transpose{ \bm{F}^{(P)} }  &
    \transpose{ \bm{H}^{(1)} } & \dots & \transpose{ \bm{H}^{(P)} } &
    \transpose{ \bm{\phi}^{(1)} } & \dots & \transpose{ \bm{\phi}^{(P)} }  &
    \transpose{ \fLink^{(1)} } & \dots & \transpose{ \fLink^{(P-1)} }
    \end{bmatrix}^\intercal \,.
\end{equation}
The lower and upper boundaries for the vector of constraints, $\bm{C}_\text{min}$ and $\bm{C}_\text{max}$, are constructed in an analogous manner, with the peculiarity that the boundaries corresponding to the dynamic defects, must be set invariably equal to zero, \( \bm{F}^{(1:P)} = \bm{0} \), such that the dynamics are enforced and that the trajectory satisfies the equations of motion at all times.

\subsection{Jacobian Matrix}

The Jacobian matrix is a quantitative description of the derivatives of the constraints with respect to the decision variables, providing a first order gradient that is necessary to aid the nonlinear solver. This section serves to overview the components of the Jacobian matrix that results from a discretization based on the flipped Radau method. An in-depth description of this matrix can be consulted in Appendix \ref{append:Jacobian}.

Each row of the Jacobian matrix corresponds to an algebraic constraint, and each column corresponds to a decision variable. Ultimately, each element represents a linear dependency between a constraint (row) with respect to a decision variable (column). Mathematically the Jacobian can be expressed as \citep{Sagliano2017c}
\begin{equation} \label{eq:JacobianGeneric}
    \textbf{Jac} =
    \nabla_{\bm{X}_{\text{NLP}}} \begin{bmatrix} J(\bm{X}_{\text{NLP}}) \\ \bm{C}(\bm{X}_{\text{NLP}}) \end{bmatrix} =
    \begin{bmatrix}
    \nabla \mathcal{J}^N \\
    \nabla \bm{F}^{(1:P)} \\
    \nabla \bm{H}^{(1:P)} \\
    \nabla \bm{\phi}^{(1:P)} \\
    \nabla \fLink^{(1:P-1)}
    \end{bmatrix}  \,.
\end{equation}

The format of the vectors $\bm{X}_\text{NLP}$ and $\bm{C}(\bm{X}_\text{NLP})$ dictate the sparsity pattern of the Jacobian matrix. Figure~\ref{fig:JacobianSparisty} is a visual representation of the pattern of the Jacobian for an example problem with four phases. In this Figure, zero elements are represented by white space, while non-zero elements are represented with coloured dots. The first row of the matrix corresponds to the discrete cost functional where the magenta circles represent a Lagrangian cost and the blue dot at the end of phase four represents a Mayer cost; there are four open terminal times (green columns); a scalar path constraint applied to all phases (concatenated diagonals in cyan); vector event constraints at the terminal times of every phase (magenta blocks); and three linkage conditions connecting the four phases sequentially (red diagonals at the bottom rows). The four phases can be distinguished by the four large red blocks with blue diagonal smaller blocks (corresponding to the dynamic defects of each phase). The prominent red diagonals in the Jacobian correspond to the sparsity pattern of the differentiation matrices and to the identity matrices corresponding to the linkage conditions. The values of the Jacobian matrix corresponding to the red dots are static, i.e., they will remain unchanged in every iteration loop.
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.8\textwidth]{ChapterIII/JacobianStructureEdit.pdf}
	\caption{Sparsity pattern of the Jacobian matrix for an example problem with four phases, five collocation points on each phase, four open terminal times, one scalar path constraint applying to every phase and a terminal constraint vector applying to every phase. White spacing represents zero-valued elements.}
	\label{fig:JacobianSparisty}
\end{figure}

It is relevant to note that the diagonals on the linkage conditions rows are, invariably, positive and negative identity matrices with the dimension of the state vector, $\pm \bm{I}_{n_{\bm{x}} \times n_{\bm{x}}}$, connecting one phase to the next. Also, it is noticeable in Fig. \ref{fig:JacobianSparisty}, that the constraints of different phases are decoupled from one another, in other words, decision variables of one phase do not affect the constraints of any other phase, thus the resulting Jacobian matrix is very sparse.

\section{Overview of the Solving Procedure}

In order to solve a given optimal control problem a procedural approach is taken. This procedure needs to be generic in order to be able to handle as many problems as possible. The main task at hand is to perform a transformation of the user input into variables and constraints that are useful to feed an \gls{nlp} solver. The following description applies to the MATLAB tool \gls{spartan} developed at \gls{dlr} \citep{Sagliano2017a,Sagliano2017c}. Figure \ref{fig:procedure_flowchart} presents a high-level overview of the solving procedure implemented in \gls{spartan} for visual aid.

\begin{figure}[hbt!]
\centering
\includegraphics[width=0.5\textwidth]{ChapterIII/SpartanFlowchart.eps}
\caption{Flowchart of the solving process by the MATLAB tool \gls{spartan}.}
\label{fig:procedure_flowchart}
\end{figure}

\begin{description}
	\item[1. User Input] First the user provides a set of MATLAB functions, with everything deemed necessary to describe the problem, namely: the Mayer term and the Lagrange term, the right-hand side of the equations of motion, the algebraic function of path constraints (and the respective upper and lower boundaries), the algebraic function of the event constraints (and the respective upper and lower boundaries), the upper and lower boundaries of the endpoint variables of every phase, the upper and lower boundaries of state and control along the trajectory of every phase, and the upper and lower boundaries of the phase linkage conditions of every phase transition. Also, with regards to discretization rules, the user needs to provide the number of phases in the problem, the number of collocation nodes to use on each phase and the concrete number of states and controls of the problem. Finally the user may choose the \gls{nlp} solver and the differentiation method along with some minor solver options.
	\item[2. Transcription Process] Once the user provides the input MATLAB functions (along with the respective handle names), then \gls{spartan} takes control and starts the transcription process. This process consists on generating the location of the collocation points (roots of the flipped Radau polynomial) according to the number of nodes specified by the user; generating the concatenated vectors of upper and lower boundaries of the decision variables and of the algebraic constraints based on the problem data provided by the user, perform a dependency analysis on the algebraic function provided by the user in order to construct the structure of the Jacobian matrix with satisfactory sparsity; and generate an initial guess of state and control at the collocation nodes in the form of a concatenated vector of decision variables.
	\item[3. Iterative Loop] The initial guess vector and the boundary vectors then scaled and fed to the \gls{nlp} solver where an iterative loop takes place. For all purposes the solver can be treated as a "black box" which, at every iteration, provides an update on the decision variables and requests an update on both, the constraints and the respective Jacobian matrix. Because the user provides the algebraic functions in "full size", the updated variables need to be scaled up in every iteration loop in order to compute the constraints, and the constraints and the Jacobian need to be scaled down in every iteration loop before being sent to the solver.
	\item[4. Post-processing] Finally, if the problem is feasible, the solver yields a solution to the \gls{nlp}, which consists on the concatenated vector of decision variables. Some post-processing tasks are then required in order to output a solution that makes sense to the user. The discretization rules provided by the user are taken in order to "break apart" the vector of decision variables into states, controls and endpoint times. A similar process takes place in order to extract the dual variables. Then, one needs to extrapolate the endpoint controls and costates corresponding to the initial time, $t_0$, of each phase, as this endpoint is not collocated. After that, the costate mapping is performed and the Hamiltonian is computed. Finally a "continuous" solution is generated based on the Lagrange interpolating polynomials. This interpolation is applied to state, control, costate and Hamiltonian alike.
\end{description}
