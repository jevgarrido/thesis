
\nextoddpage
%\chapter{Transcription of Smooth Problems}
\chapter{Legendre-Gauss-Radau Pseudospectral Method for Smooth Problems}
\label{chapter:SinglePhase}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction to Pseudospectral Methods}

As mentioned before, pseudospectral methods turn the optimal control problem into a nonlinear programming problem. This is then sent to a nonlinear solver and the solution is outputted in discrete form. In essence, the optimal control problem is reformulated as a pure optimization problem: a list of variables, a list of constraints and an objective function. The output of the nonlinear solver is the configuration of variables that satisfy the full list of constraints and which will minimize (or maximize) the objective function. \par 

The set of variables outputted by the solver contains the solution of the optimal control problem in discrete form. Some post processing is required in order to extract this solution and to find a continuous approximation. \par 

What sets pseudospectral methods apart from other methods is that they are extremely versatile
have a straight forward implementation,
general purpose 
typically fast




 (\gls{frmethod})



\section{Mathematical Background}
\label{sec:MathBackground}

\subsection{Gaussian Quadrature and Domain Mapping}
\label{subsec:GaussQuad}

Given a polynomial function, \(f(\tau)\), it is possible to compute the definite integral from \numrange{-1}{1} according to the \emph{Gaussian quadrature}. The function is evaluated at \( N \) discrete points and a weighted sum is performed, as in \citep{Radau1880,Davis1984,Fahroo2008}:
\begin{equation} \label{eq:GaussQuadrature}
    \int_{-1}^{1} f(\tau) \dd{\tau} = \sum_{k=1}^{N} w_k f_k
\end{equation}
where \( f_k = f(\tau_k)\) is the value of the function evaluated at \( \tau = \tau_k \). \par

In order to perform an integration over a generic interval \( t \in \begin{laidvec} t_0 & t_f \end{laidvec} \), the domain of the independent variable, \(t\), is mapped into the normalized domain \(\tau \in \begin{laidvec} -1 & 1 \end{laidvec}\). This mapping is a linear one and can be expressed as follows \citep{Sagliano2017c}:
\begin{align} 
	t(\tau) &= \frac{t_f - t_0}{2} \tau + \frac{t_f + t_0}{2}, \quad
	\tau \in \begin{laidvec} -1 & 1 \end{laidvec} \label{eq:DomainMapping} \\[0.8em]
	\dd{t} &= \frac{t_f - t_0}{2} \dd{\tau} \label{eq:dtDomainMapping}
\end{align}

By performing a change of variable the Gaussian quadrature can be expressed for a generic interval:
\begin{equation} \label{eq:GaussQuadratureGeneric}
	\nint{t_0}{t_f} f(t) \dd{t} = \dfrac{t_f-t_0}{2} \sum_{k=1}^{N}  w_k  f_k  
\end{equation}
where \( f_k = f \bigl( t(\tau_k) \bigr) \) is the value of the function evaluated at \( t(\tau) \) with \( \tau = \tau_k \).

It is important to note that it is not sufficient to evaluate the function at any given set of \( N \) discrete points. For the quadrature to be accurate it is crucial that the distribution of points follows a specific pattern. The pattern must be similar to the distribution of roots of a Legendre-based polynomial \citep{Davis1984}. Gaussian quadrature is exactly accurate for polynomial integrand functions of order up to \citep{Radau1880,Fahroo2008}:
\begin{stdlist}
	\item \(2N-1\), by sampling according to the roots of a Legendre polynomial.
	\item \(2N-2\), by sampling according to the roots of a Legendre-Radau polynomial.
	\item \(2N-3\), by sampling according to the roots of a Legendre-Lobatto polynomial.
\end{stdlist}

If the integrand function is not a polynomial then there will be an error
associated with the integration \citep{Davis1984,Fahroo2008}. This error is reduced by increasing the number of sample points, \(N\), effectively approximating the integrand by a polynomial of higher order. \par 

The change of variable of integration can be interpreted as a shift in the domain from concrete time,\(t\), to a \emph{pseudo-time}, \(\tau\), which is always within \numrange{-1}{1}. This domain shift allows for the problem to be normalized and for the endpoints of the domain, \(t_0 \) and \( t_f\), to be set as decision variables. This is crucial, for instance, when the final time of the trajectory is uncertain. \par


\subsection{Lagrange Polynomial Interpolation and Differentiation Matrix}
\label{subsec:LagrangeDiffMatrix}

With a set of \(N\) discretization points along the domain, and the corresponding samples of a polynomial \(\bm{x}_i\), the smooth function approximation, \(\bm{x}(\tau)\), which passes through the \(N\) points can be found via \emph{Lagrange polynomial interpolation}, as in \citep{Berrut2004}:
\begin{align}
\bm{x}(\tau) &= \sum_{i} \bm{x}_i L_i(\tau) \\
L_i(\tau) &= \prod_{\substack{j \\ j \neq i}} \dfrac{\tau - \tau_j}{\tau_i - \tau_j} \label{eq:LagrangePoly}
\end{align}
where \(\tau_i\) is the independent variable associated with the \(i^\text{th}\) sample point and \(L_i\) is the corresponding Lagrange interpolating polynomial. \par

The first derivative of the polynomial, \( \bm{x}(\tau)\), with respect to the independent variable can also be estimated based on the Lagrange polynomial interpolation, as in \citep{Berrut2004}:
\begin{equation}	\label{LangrangeDerivative}
	\dv{\bm{x}(\tau)}{\tau} =
	\sum_{i} \bm{x}_i \dv{L_i(\tau)}{\tau}
\end{equation}

In discrete terms, this means that a differentiation matrix can be built
in order to compute the first derivative of \(\bm{x}\) with respect to \(\tau\) \citep{Rao2010}:
\begin{equation} \label{DiffMatrixElements}
\bm{D}_{ki} = \dot{L}_i(\tau_k) =
\sum_{\substack{l \\ l \neq i}}
\dfrac{1}{ \tau_i - \tau_l }
\prod_{\substack{j \\ j \neq i,l }} \dfrac{ \tau_k - \tau_j }{ \tau_i - \tau_j }
\end{equation}

The derivative is computed as:
\begin{equation}
	\dv{\bm{x}_k}{\tau} = \sum_{i} \bm{D}_{ki} \bm{x}_i
\end{equation}

It is now possible to apply the chain rule, toguether with Eq. \eqref{eq:dtDomainMapping}, in order to find the derivative about concrete time, \(t\).
\begin{align}
	\dv{\bm{x}}{t}		&= \dv{\tau}{t} \dv{\bm{x}}{\tau}  \\[0.8em]
	\dot{\bm{x}}_k = \dv{\bm{x}_k}{t}	&= \dfrac{2}{t_f-t_0} \sum_{i} \bm{D}_{ki} \bm{x}_i \label{eq:GenDiff}
\end{align}

Equation \eqref{eq:GenDiff} provides a generic way to compute the first derivative of any given function, \(\bm{x}(t)\), with respect to the appropriate independent variable, provided a good sampling pattern is chosen. \par

As was the case for Gaussian quadrature, the accuracy of the Lagrange polynomial interpolation and that of the differentiation matrix is directly related to the choice of sample points along the domain of \(\tau\). 
If no mind is paid to the selection of discretization points, for instance, by choosing a uniform distribution of nodes, the Lagrange approximation is  not very accurate due to the Runge phenomenon. As it happens, the roots of the three Legendre-based polynomials mentioned earlier (Legendre, Legendre-Radau and Legendre-Lobatto) turn out to be a good choice \citep{Fahroo2008} for the discretization pattern. \par 

Although the derivative \( \dv{\bm{x}}{t} \) could be computed directly by building matrix \(\bm{D}\) using the values of \( t\) instead of \(\tau\) in \eqref{eq:LagrangePoly} and \eqref{DiffMatrixElements}, it is more convenient to isolate \(t_0\) and \(t_f\) as most often these will be used as decision variables. And so, matrix \(\bm{D}\) is used for the derivative with respect to \(\tau\) and the derivative with respect to \(t\) yields easily via a simple multiplication. This also has the advantage of matrix \(\bm{D}\) being constant, so it will not change in case the variables \(t_0\) and \(t_f\) change. \par 



%The three above described discrete operations (the Lagrange polynomial interpolation; the derivative operator by differentiation matrix multiplication; and the integration by Gaussian quadrature) can be more or less accurate according to a wiser or poorer choice of sample points \(t_k\) along the domain of the independent variable, respectively. \par
%The distribution of points must be chosen wisely in order to avoid Runge's phenomenon.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Legendre-Radau polynomial and node distribution}
\label{subsec:RadauPoly}

Root distribution figure

Inclusion of terminal endpoint (endpoint collocation)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Direct Transcription of Single Phase Problems}

\subsection{Discrete Nonlinear Programming Problem}

Taking Eqs. \eqref{OCPGenCost} through \eqref{OCPGenBoundaries}, where the single-phase optimal control problem is presented in continuous form, a conversion is performed as to transform the continuous problem into a discrete one prior to finding the optimality conditions. \par

\begin{equation}
	\mathcal{J}^N = \Phi + \dfrac{t_f - t_0}{2} \sum_{k}  w_k  \Psi_k
\end{equation}

\begin{equation}
	f_k - \dfrac{2}{t_f - t_0} \sum_{i} \bm{D}_{ki} \bm{x}_i = 0
\end{equation}

\begin{equation}
	\bm{g}_{min} \leq  \bm{g}_k \leq \bm{g}_{max}
\end{equation}

\begin{equation}
	\bm{\phi}_{min} \leq  \bm{\phi} \leq \bm{\phi}_{max}
\end{equation}


This works because the two terms are always equal and also always linearly independent, as $f$ is applied locally (i.e., once in each discrete node or time instant) and $D$ is applied globally (matrix multiplication), introducing coupling between the nodes.


\subsection{KKT Conditions}

Discrete, adjoined cost functional
\begin{equation}
\mathcal{J}^{N\lambda} = \Phi + \tilde{\bm{\nu}} \bm{\phi} + 
\dfrac{t_f-t_0}{2} \sum_{k}  w_k  \bigg\{ \Psi_k +
C_k^{\lambda}\tilde{\bm{\lambda}}_k \bigg[ f_k - \dfrac{2}{t_f - t_0} \sum_{i} \bm{D}_{ki} \bm{x}_i \bigg] +
C_k^{\mu}\tilde{\bm{\mu}}_k \bm{g}_k \bigg\}
\end{equation}

This approach of discretizing first and then optimizing is the procedure that defines a direct method, which goes in opposition to the classical approach of optimizing first and disretizing after (indirect methods), as discussed in chapter \ref{chapter:ocp}. 





\subsection{Covector Mapping}

\begin{figure}[ht]
\centering
\includegraphics[width=0.5\textwidth]{Introduction/commutative_diagram.png}
\caption{Commutative diagram for discrete transcription and optimality description of a given optimal control problem.}
\source{2006, Benson et al. \citep{Benson2006}}
\label{fig:commutative_diagram}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Adjoined Vectors and Jacobian Matrix}


