
\section{Problem 1: Falcon 9 Rocket Boost-Back Burn and Vertical Landing (3 phases, 2-D trajectory)}
\label{sec:problem_one}

The first problem to consider is an adaptation from \citep{Anglim2017}. This example is concerned with the recovery of the first stage of an orbital launcher via vertical landing, where the landing target is located close to the launching site (return-to-launch-site scenario). The vehicle in question is based on the characteristics of the main booster of SpaceX's Falcon 9 rocket which is illustrated in Figure~\ref{fig:f9_BoosterIllustration}. This problem is appropriate to validate the algorithm developed in this work because it is composed of three distinct phases, resulting from the mutation of the equations of motion from one phase to the next.
The algorithm is validated by analysis of the optimality conditions.
\begin{figure}[bt!]
\centering
\fcolorbox{black}{black}{\includegraphics[height=60mm]{ChapterIV-Example2/F9_booster.png}}
\caption{Illustration of the main stage of SpaceX's Falcon 9 rocket \citep{F9BoosterPic}.}
\label{fig:f9_BoosterIllustration}
\end{figure}

The problem occurs in a vertical, two-dimensional plane (2-D). At the beginning of the trajectory it is assumed that stage separation has already occurred and that the booster has performed the "turn back" attitude manoeuvre such that the boost-back-burn may begin. The trajectory is composed of three phases:
\begin{description}[nosep]
    \item[Phase 1] Boost-back. At $t = t_0$ the rocket is in mid air with ascending linear momentum. Three engines (out of nine) ignite and the thrust direction is constrained to point strictly horizontally. In this phase the rocket inverts its horizontal motion. The duration of the boost-back burn, $t = t_1$, is a known, fixed parameter.
    \item[Phase 2] Coast arc. At $t = t_1$ the engines shut down and remain off during all of phase 2. The vehicle takes a parabolic flight profile during this phase. It is taken for granted that an attitude justification manoeuvre takes place during this phase in order to roughly align the thrust vector with the velocity vector at the beginning of phase 3.
    \item[Phase 3] Landing. At $t = t_2$ one engine (out of nine) ignites. The vehicle is now controllable and the booster makes its way to the target site. Touch-down happens at $t = t_f$. Both $t_2$ and $t_f$ are unknown variables to be determined.
\end{description}

The equations of motion are formulated in the target-centred reference frame \citep{Anglim2017}. Throughout the trajectory, both position and velocity are modelled in Cartesian coordinates, with the reference frame of displacement centred at the landing target location. Figure~\ref{fig:f9_free_body_diagram} presents a free-body diagram of the vehicle for visual reference. Downrange is represented by the horizontal axis, $x$, increasing from left to right, and altitude is represented by the vertical axis, $y$, increasing from bottom to top. The angle $\theta$ indicates the direction of the thrust vector, $\bm{T}$, and the flight-path angle, represented by the angle $\gamma$, indicates the direction of the velocity vector, $\bm{v}$. Both angles are measured from the $x$ axis and positive in the direction of increasing $y$. The Cartesian components of velocity, $v_x$ and $v_y$, are positive in the direction of increasing respective position coordinates. The aerodynamic drag force is represented by vector $\bm{D}$ which is always collinear with the velocity vector, and $m$ is the mass of the vehicle. The Earth is assumed to be flat and non rotating, and the acceleration of gravity is assumed to be invariant with altitude throughout the trajectory, taking the value $g_0$.
The control variable is the thrust tilt angle, $\theta$. A point-mass approximation of the vehicle is employed, thus the attitude of the spacecraft is not modelled and the angle of attack is assumed to be zero at all times. An exponential model is used for the drag force.
\begin{figure}[bt!]
	\centering
	\includegraphics[height=50mm, keepaspectratio]{ChapterIV-Example2/free_body_diagram.pdf}
	\caption{Free-body diagram of the vehicle.}
	\label{fig:f9_free_body_diagram}
\end{figure}
The optimal control problem can be formulated as minimizing the cost
\begin{equation}
    \mathcal{J} = \Phi \bigl( m(t_f) \bigr)  = -m( t_f ) \,,
\end{equation}
associated with the dynamic system
\begin{align}
	\dot{ x }   & = v_x \,,                                                           \\
	\dot{ y }   & = v_y \,,                                                           \\
	\dot{ v_x } & = \dfrac{T}{m} k \cos{\theta} - \dfrac{D}{m} \cos{\gamma} \,,       \\
	\dot{ v_y } & = \dfrac{T}{m} k \sin{\theta} - \dfrac{D}{m} \sin{\gamma} - g_0 \,, \\
	\dot{ m }   & = -\dfrac{T}{I_{sp}g_0} k
\end{align}
and subject to the event constraints \newline
\noindent
\begin{minipage}{0.5\linewidth}
\begin{align}
	x(t_0)   & = \SI{36.022}{\km} \,,                                \\
	y(t_0)   & = \SI{60.708}{\km} \,,                                \\
	v_x(t_0) & = \SI{1.052}{\km\per\second} \label{f9_initialVx} \,, \\
	v_y(t_0) & = \SI{1.060}{\km\per\second} \label{f9_initialVy} \,, \\
	m(t_0)   & = \SI{76501}{\kg} \,,
\end{align}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\begin{align}
	x(t_f)                                 & = \SI{0}{\km} \,,                                   \\
	y(t_f)                                 & = \SI{0}{\km} \,,                                   \\
	v_x(t_f)                               & = \SI{0}{\km\per\second} \,,                        \\
	\SI{-0.5}{\m\per\second}\leq  v_y(t_f) & \leq \SI{0.5}{\m\per\second} \label{f9_finalVy} \,, \\
	m(t_f)                                 & = \text{free} \,,
\end{align}
\end{minipage} \newline
with
\begin{align}
	D            & = \frac{1}{2} \rho_0 \exp{-\frac{y}{h_0}} C_D \pi \frac{d^2}{4} v^2 \,, \\
	v^2          & = v_x^2 + v_y^2 \,,                                                     \\
	\cos{\gamma} & = \dfrac{v_x}{v} \,,                                                    \\
	\sin{\gamma} & = \dfrac{v_y}{v} \,,
\end{align}
and
\begin{equation}
    T = \begin{cases}
    \frac{1}{3} T_{LO} &t \in [ t_0,t_1 ]  \\
    0 &t \in [ t_1,t_2 ] \\
    \frac{1}{9} T_{LO} &t \in [ t_2,t_3 ]
    \end{cases}
	\quad \quad 
	k = \begin{cases}
	1 &t \in [ t_0,t_1 ] \\
	0 &t \in [ t_1,t_2 ] \\
	1 &t \in [ t_2,t_3 ] 
	\end{cases}
	\,,
\end{equation}
where
$k$ is the engine throttle,
$\rho_0$ is the air density at sea level,
$h_0$ is the density scale height,
$C_D$ is the drag coefficient,
$d$ is the diameter of the vehicle and
$v$ is the norm of the velocity vector. 
The linkage conditions are omitted because there are no expected jump discontinuities on the sates of the systems in any phase transition. Table~\ref{tab:Falcon9Constants} shows the constants and parameters used in this problem.
\begin{table}[bt!]
\stdtable
\caption{Relevant constants and parameters for the Falcon 9 first stage recovery problem.}
\label{tab:Falcon9Constants}
\begin{tabular}{@{}lcs@{}}
	\toprule
	Constant                                  &  Value  & Unit                  \\
	\midrule
	Specific impulse, $I_{sp}$                &   282   & \second               \\
	Lift-off Thrust, $T_{LO}$                 &  5886   & \kilo\newton          \\
	Rocket diameter, $d$                      &  3.66   & \m                    \\
	Drag Coefficient, $C_D$                   &  0.75   & 1                     \\
	Density scale height, $h_0$               &  7500   & \meter                \\
	Gravity acceleration at seal level, $g_0$ & 9.80665 & \m\per\second\squared \\
	Air density at sea level, $\rho_0$        &  1.225  & \kg\per\meter\cubed   \\
	Phase 1 initial time, $t_0$               &    0    & \second               \\
	Phase 1 to phase 2 transition time, $t_1$ &  40.8   & \second               \\
	Dry mass, $m_\text{dry}$                  &  25600  & \kg                   \\
	\bottomrule
\end{tabular}
\end{table}

The problem at hand is not formulated exactly the same way as in the original article, therefore, a few remarks are in order to relate the two formulations of this problem. Briefly stated, the problem implemented in this work contains fewer degrees of freedom than that of the original article. Namely:
\begin{stdenum}
	\item The duration of the boost-back burn, $t_1$, is fixed, while in \citep{Anglim2017} this parameter is taken as an additional decision variable. The fixed value of engine shut down time of phase 1, $t_1$, was selected in such a way as to facilitate a vertical flight-path angle towards the end of phase 3.
	\item The throttle level is fixed at the maximum value during phases 1 and 3, while in \citep{Anglim2017} the throttle is allowed vary between $0$ and $1$ in both of these phases.
	\item Downrange, altitude, horizontal velocity at the final time of the trajectory are constrained to zero, while in \citep{Anglim2017} these variables appear solely as weighted minimization parameters in the cost functional.
\end{stdenum}

Also, some parameters have been relaxed in the present formulation with respect to the original article. Namely:
\begin{stdenum}
	\item The control is expressed in polar coordinates throughout the whole trajectory, therefore there is no need to include an additional path constraint in order to assert the modulus of Cartesian components. This contrasts with the formulation in \citep{Anglim2017} where the use of polar and Cartesian coordinates is alternated from phase to phase.
	\item In the present formulation, and opposed to the original article, the landing phase is not constrained to a gravity turn, allowing extra manoeuvrability of the vehicle during this phase end facilitating a vertical flight path angle at the landing point.
	\item The vertical component of the velocity at the landing time, $v_y(t_f)$, is allowed to vary as shown in \eqref{f9_finalVy}, while in \citep{Anglim2017} this variable is a taken as a Mayer cost to be minimized.
	\item The cost functional is composed only of the final mass of the vehicle, while in \citep{Anglim2017} the final positions and velocities are weighted in as well.
\end{stdenum}

During phase 1 the trajectory is completely determined because the control sequence is known and the endpoint times are fixed, and in phase 2 the vehicle is uncontrollable due to the throttle being constrained to zero. Ultimately, the only degrees of freedom available to optimize the trajectory, therefore, are the engine reignition time, $t_2$, the total time of flight, $t_f$ and the direction of the thrust vector during phase 3.

With this simplified formulation one makes sure that the problem is feasible, therefore an optimal solution is guaranteed to exist and it is possible to focus on the ability of the algorithm to find it. A simplified formulation of the problem is adequate in this case because there is no interest in studying the feasibility of the problem, there is only interest in validating the flipped Radau method.

This problem was solved using 10, 8 and 12 collocation nodes in phases 1, 2 and 3, respectively. The \gls{nlp} solver used was IPOPT~\citep{Kawajir2015}, and the method used to compute partial derivatives was the complex step differentiation method~\citep{Alonso2003, DOnofrioThesis2014}. The results are depicted in Figs.~\ref{fig:f9_Position} through \ref{fig:f9_Hamiltonian}. Specifically, Figs.~\ref{fig:f9_Position} through \ref{fig:f9_AnglimTrajectory} deal with the solution of state and control, while Figs.~\ref{fig:f9_MassCostate} through \ref{fig:f9_Hamiltonian} deal with the solution of the dual variables (covectors and Hamiltonian). In addition the resulting parameters of unknown times and final mass are presented in Table~\ref{tab:f9_Results} including the references of the original article.

%% Position and velocity components
Figures~\ref{fig:f9_Position} and \ref{fig:f9_Velocity} illustrate the Cartesian components of position, and the Cartesian components of velocity with time, respectively. The phase transition points are visible through the increase in density of collocation nodes.
With regards to Fig.~\ref{fig:f9_Velocity}, the phase transitions are clearly pronounced due to the removable discontinuities present in each component. The horizontal component of velocity decreases linearly during the first phase and goes from positive to negative, inverting the direction of flight, which is indicative of the boost-back burn phase. This component stays constant during the coasting phase, as expected. It is noticeable that during phases 1 and 2, the vertical component of velocity decreases linearly, without noticeable discontinuities, indicating that this component has been subject to a constant acceleration during these two phases, undoubtedly the acceleration of gravity. It is also visible that the vertical component of velocity goes from positive to negative close to the $\SI{110}{\second}$ mark, inverting the direction flight. Both velocity components go to zero during the landing phase, as expected.
In contrast, regarding Fig.~\ref{fig:f9_Position}, the position does not contain removable discontinuities, this is due to the fact that position is the integration of velocity through time, resulting in a curve that is "one degree" smother. One relevant remark is that both the downrange and the altitude are tangent to the time axis at the endpoint of the trajectory, indicating a smooth landing.
\begin{figure}[bt!]
	\centering
	\begin{minipage}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{ChapterIV-Example2/BestPlots/Position.pdf}
		\caption{Downrange and altitude with time. Both position coordinates are tangent to the time axis at the final time.}
		\label{fig:f9_Position}
	\end{minipage}%
	\hfill
	\begin{minipage}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{ChapterIV-Example2/BestPlots/Velocity.pdf}
		\caption{Cartesian components of velocity with time. Both components approach zero at the final time.}
		\label{fig:f9_Velocity}
	\end{minipage}
\end{figure}

%% Mass profile with time
The mass profile of the vehicle can be seen in Fig.~\ref{fig:f9_Mass}. Due to the constant throttle level, and the constant specific impulses, the mass of the vehicle decays linearly during phases 1 and 3. The mass flow-rate is higher in phase 1 due to the larger thrust associated --- there are three engines on during phase 1 and only one engine ignited during phase 3. Not remarkably, the mass stays constant during phase 2 (coast phase).
\begin{figure}[b!]
\centering
\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/Mass.pdf}
\caption{Profile of the total mass of the vehicle with time. Linear decay in phases 1 and 3 indicates constant mass flow-rate.}
\label{fig:f9_Mass}
\end{figure}

%% Thrust direction (control)
The angle indicating the thrust direction (control) is presented in Fig.~\ref{fig:f9_ThrustDirection}. It can be seen that the direction of thrust is constrained to $\SI{180}{\degree}$ during phase 1, and constrained to $\SI{0}{\degree}$ during phase 2 (during phase 2 the thrust direction is inconsequential due to the throttle being constrained to zero). Finally, in the landing phase the direction of thrust is allowed to vary, and the angle draws a curve that approaches $\SI{90}{\degree}$, indicating that the thrust points vertically at the end of the trajectory.

%% Flight path angle
Figure~\ref{fig:f9_PathAngle} shows the evolution of flight path angle of the vehicle in time. The flight path angle, representing the direction of the velocity vector, starts close to $\SI{45}{\degree}$ at the beginning of the boost back phase, indicating an ascending trajectory compatible with the initial conditions expressed in \eqref{f9_initialVx} and \eqref{f9_initialVy}. The Figure also shows the removable discontinuities occurring at the phase transition times, which goes in agreement with the discussion above with regards to the velocity components in Fig.~\ref{fig:f9_Velocity}, in particular, the slight discontinuity in the flight path angle occurring in the transition between coast phase and landing phase indicates that the powered landing does not follow a gravity turn (in a gravity turn the flight path angle is unaffected by the thrust), however, this angle does approach $\SI{270}{\degree}$ in the last phase, indicating a vertical landing. Ultimately, Fig.~\ref{fig:f9_PathAngle} concurs with Fig.~\ref{fig:f9_ThrustDirection} during the landing phase in the sense that the velocity vector and the thrust vector both tend to be vertical at the end of the trajectory and point in opposite directions.
\begin{figure}[bt!]
\centering
\begin{minipage}[t]{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{ChapterIV-Example2/BestPlots/ThrustDirection.pdf}
  \captionof{figure}{Thrust direction, $\theta$, with time. Thrust direction approaches $\SI{90}{\degree}$ at the final time.}
  \label{fig:f9_ThrustDirection}
\end{minipage}%
\hfill
\begin{minipage}[t]{0.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{ChapterIV-Example2/BestPlots/PathAngle.pdf}
  \captionof{figure}{Flight path angle, $\gamma$, with time; flight path angle approaches $\SI{270}{\degree}$ at the final time.}
  \label{fig:f9_PathAngle}
\end{minipage}
\end{figure}

%% Trajectory
The trajectory of the vehicle is shown in Fig.~\ref{fig:f9_Trajectory}, where the downrange and the altitude are plotted against each other. The boost-back phase is shown in blue, the coasting arc is shown in orange and the landing phase is represented in yellow. Curiously, the vehicle traces a path that vaguely resembles a shepherd's staff. The plot clearly shows that at the beginning of the trajectory the vehicle is travelling from left to right and with ascending velocity. The boost-back burn inverts the horizontal motion of the vehicle and during the coasting arc, the vehicle takes a parabolic flight due to being in complete free-fall. Finally, in the landing phase the vehicle approaches the zenith of the target and the flight path angle gets closer and closer to being vertical. Intuitively speaking, the trajectory follows a predictable path.
\begin{figure}[bt!]
\centering
\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/Trajectory.pdf}
\caption{Altitude vs downrange trajectory. Boost-back phase is represented in blue; the coast phase is represented in orange; and the landing phase is shown in yellow.}
\label{fig:f9_Trajectory}
\end{figure}

% Trajectory comparison
Fig.~\ref{fig:f9_AnglimTrajectory} presents a comparison between the trajectory yielded by \gls{spartan} and the reference solution \citep{Anglim2017}. It can be seen that the trajectories are quite similar, having an identical maximum altitude. The solutions diverge slightly during the descent of the vehicle, and the trajectory from \gls{spartan} approaches the target at a steeper flight path angle. This Figure is presented in order to confirm the plausibility of the scenario, given that the two problem formulations are similar. However, the two trajectories must be judged independently for their optimality, as the corresponding problem formulations are in fact not the same, having a different number of degrees of freedom and a different cost functional.
\begin{figure}[bt!]
\centering
\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/AnglimTrajectory.pdf}
\caption{2-D trajectory yielded by \gls{spartan} (in red) superimposed with the reference solution \citep{Anglim2017} (dashed black).}
\label{fig:f9_AnglimTrajectory}
\end{figure}

%% Mass costate
Moving on to the solution of dual variables, Fig.~\ref{fig:f9_MassCostate} shows the evolution of the mass costate along time. Recalling the first order necessary conditions from Section \ref{sec:HBVP}, it is possible to note that the endpoint condition expressed in \eqref{EulerLagrangeXf} is verified at the final time of the trajectory, namely
\begin{equation}
	\lambda_m(t_f) = \pdv{\Phi}{m(t_f)} = -1 \,.
\end{equation}
This condition is not verified at any other phase endpoint due to the fact that the mass is either fixed at those points or completely determined by the initial conditions, the constant mass flow rates and the fixed endpoint time of phase 1. Ultimately, the validation of the endpoint condition at the final time of the trajectory brings confidence that the solution is optimal.
\begin{figure}[bt!]
\centering
\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/MassCostate.pdf}
\caption{Mass costate with time. The mass costate takes the value -1 at the final time.}
\label{fig:f9_MassCostate}
\end{figure}

%% Primer vector
Another factor to consider in order to validate the optimality of the solution is whether the primer vector (velocity costate) is collinear with the thrust vector \citep{Vinh1973,Conway2010}. Figure~\ref{fig:f9_PrimerVector}, therefore, shows the Cartesian components of this vector (on the left), and the norm of the cross product between this vector and the control (top-right and bottom-right plots). In order for the vectors to be collinear, it is required that the cross product between each other yields zero. It is clear that this collinearity is not achieved during the phase 1, in fact, the norm of the cross product is identical to the vertical component of the primer vector during this phase. This non-collinearity, however, is expected because the control is constrained during this phase of the trajectory, having no degrees of freedom available assert orientation concurrency with the primer vector. During phase 2, the throttle is constrained to be zero at all times, which means that the cross product yields a trivial null vector. However, during phase 3, the thrust is allowed to vary its direction and assert orientation concurrency. By looking at Figure~\ref{fig:f9_PrimerVector} we see that the norm of the cross product is very small with respect to the components of both the primer vector and of the control, being roughly eight orders of magnitude smaller. With this information it is possible to assess that the two vectors are indeed collinear during phase 3. This result further increases confidence in the optimality of the solution.
\begin{figure}[b!]
\centering
\includegraphics[width=\textwidth]{ChapterIV-Example2/BestPlots/PrimerVector.pdf}
\caption{Left: Cartesian components of primer vector with time. Top-right: Norm of the cross product between the control and the primer vector. Bottom-Right: Zoom in on phases 2 and 3 of the top-right plot.}
\label{fig:f9_PrimerVector}
\end{figure}

%% Hamiltonian
Finally, Fig.~\ref{fig:f9_Hamiltonian} shows the Hamiltonian associated with the trajectory. 
By inspection of the plot, one can assert that the Hamiltonian is phase-wise constant, and with regards to phases 2 and 3, one can verify that the Hamiltonian is zero, which implies that the endpoint condition \eqref{JaVarTf} is satisfied in these phases:
\begin{equation}
	\mathcal{H} \bigl(t_f^{(2)}\bigr) = \mathcal{H} \bigl(t_f^{(3)}\bigr) = 0 \,,
\end{equation}
Briefly stated, the Hamiltonian satisfies the optimality conditions for time invariant systems \citep{Bryson1975,Rao2010,Garg2011}, and therefore this result brings further confirmation that the solution is optimal.
\begin{figure}[bt!]
	\centering
	\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/Hamiltonian.pdf}
	\caption{Hamiltonian vs time. Hamiltonian is zero during phases 2 and 3.}
	\label{fig:f9_Hamiltonian}
\end{figure}

Table~\ref{tab:f9_Results} presents the values of pertinent variables obtained with \gls{spartan}. The results of the original article \citep{Anglim2017} are also presented for comparison. Briefly speaking, and disregarding the duration of the boost-back phase, the trajectory yielded by \gls{spartan} consists of a longer duration of coasting arc, and a shorter duration of powered descent when compared to the solution of the original article. This also results in a significantly higher final mass of the vehicle. The results are satisfactory overall.
\begin{table}[hbt!]
\stdtable
\caption{Comparison of relevant parameter results between \gls{spartan} and \citet{Anglim2017}.}
\label{tab:f9_Results}
\begin{tabular}{@{}llls@{}}
	\toprule
	Parameter                       & SPARTAN    & \citet{Anglim2017} & Unit    \\
	\midrule
	Boost-back burn duration, $t_1$ & 40.8       & 40.5684            & \second \\
	Landing burn start time, $t_2$  & 220.4583   & 216.9112           & \second \\
	Total time of flight, $t_f$     & 303.5469   & 307.8189           & \second \\
	Final mass, $m(t_f)$            & 27905.5554 & 26221.1488         & \kg     \\
	\bottomrule
\end{tabular}
\end{table}
