
\section{Problem 2: Delta III Rocket Ascent to Elliptical Orbit (4 phases, 3-D trajectory)}
\label{section:problem_three}

This example deals with the optimization of the ascent trajectory of a solid propellant multi-staged rocket taken from \citep{Rao2010,Patterson2014,Betts2010}. The vehicle associated with this problem is based on the characteristics of Boeing's Delta III rocket which is illustrated in Fig.~\ref{fig:Ex3_DeltaIII}. This problem is appropriate to validate the algorithm developed in this work because it is composed of four distinct phases. The modelling of the problem in four phases results from mutations in the equations of motion from one phase to the next which yield removable discontinuities on the velocity, but also from jump discontinuities in the mass of the vehicle due to stage separation events.
The algorithm is validated by analysis of the optimality conditions and also by comparison of the results with the reference solution.
\begin{figure}[bt!]
\centering
\includegraphics[height=60mm]{ChapterIV-Example3/Delta_III.pdf}
\caption{Delta III rocket illustration \citep{DeltaIIIRocket}.}
\label{fig:Ex3_DeltaIII}
\end{figure}

This problem occurs in three dimensional space (3-D) and it is about finding the trajectory from launch pad to orbit insertion of a vehicle composed of nine solid rocket boosters, one main stage, and one upper stage. In total there are four distinct phases, $P = 4$. The flight sequence is:
\begin{description}[nosep]
    \item[Phase 1] Ignition of the main stage and of six solid boosters (out of nine) at $t = t_0$. Full throttle until the burnout of the six solid boosters. The six empty boosters are separated and dropped out at $t = t_1$.
    \item[Phase 2] Main stage continues in full throttle during all of phase 2. Ignition of the three left over boosters at $t = t_1$. Full throttle until the burnout of the boosters. Three empty boosters are separated and dropped out at $t = t_2$.
    \item[Phase 3] Main stage continues in full throttle. Burn out of the remaining fuel in the main stage. Main stage is separated and dropped out at $t = t_3$.
    \item[Phase 4] Ignition of the upper stage at $t = t_3$. Full throttle until orbit insertion. Payload deployed at $t = t_4$.
\end{description}

The equations of motion are taken directly form \citep{Rao2010}. Figure~\ref{fig:Ex3_free_body_diagram} presents the free-body diagram of the vehicle for visual reference. The position vector is represented by $\bm{r}$, the velocity vector is $\bm{v}$, and the aerodynamic drag force is represented by $\bm{D}$.
\begin{figure}[hbt!]
	\centering
	\includegraphics[height=60mm]{ChapterIV-Example3/free_body_diagram.pdf}
	\caption{Free-body diagram of the vehicle in \gls{eci} coordinates. The Earth is represented by a sphere centred at the origin.}
	\label{fig:Ex3_free_body_diagram}
\end{figure}
The thrust vector, is shown as $\bm{T}$. The air is assumed to be "attached" to the Earth, sharing its rotation. The acceleration of gravity, $\bm{g}$, is collinear with the position vector $\bm{r}$; the aerodynamic drag force, $\bm{D}$, is collinear with the spacecraft's velocity relative to local air, $\bm{v}_\text{rel}$, and the inertial velocity of the spacecraft, $\bm{v}$, can be decomposed into velocity relative to local air, $\bm{v}_\text{rel}$, and inertial air velocity, $\bm{\Omega} \times \bm{r}$, where $\bm{\Omega}$ is the Earth's angular velocity vector relative to the inertial reference frame.
The Earth is assumed to be spherical and rotating about the $z$ axis, and the acceleration of gravity decays with the radial distance to the centre of the planet as \citep{Rao2010}
\begin{equation}
	 \bm{g} = - \dfrac{\mu}{\norm{\bm{r}}^3} \bm{r} \,,
\end{equation}
where $ \mu $ is the standard gravitational parameter of the Earth.
The mass of the vehicle is represented by $m$, and the control variable is the thrust unit vector, $\bm{u}$. A point-mass approximation of the vehicle is employed, thus the attitude of the spacecraft is not modelled and the angle of attack is assumed to be zero at all times. An exponential model is used for the drag force.
The optimal control problem is stated as minimizing the cost functional
\begin{equation}
	\mathcal{J} = \Phi \bigl( m(t_f) \bigr) = -m( t_f ) \,,
\end{equation}
subject to the equations of motion
\begin{align}
     \dot{ \bm{r} } &= \bm{v}  \,, \\
     \dot{ \bm{v} } &= \bm{g} + \dfrac{T}{m}\bm{u} + \dfrac{\bm{D}}{m} \,, \\
     \dot{m}		&= - \dfrac{T}{g_0 I_{sp}} \,,
\end{align}
the initial conditions
\begin{align}
    \bm{r}(t_0^{(1)})   &= \bigl[ \begin{matrix} \num{5605.2e3} & 0 & \num{3043.4e3} \end{matrix} \bigr]^\intercal \si{\meter} \,, \\
    \bm{v}(t_0^{(1)})   &= \bigl[ \begin{matrix} 0 & \num{0.4076e3} & 0 \end{matrix} \bigr]^\intercal  \si{\meter} \,, \\
    m(t_0^{(1)})        &= \SI{301454}{\kg} \,,
\end{align}
the terminal conditions (orbital elements)
\begin{alignat}{2}
	\text{semi-major axis}               && \: \quad a_f^{(4)}      & = \SI{24361.14}{\km} \,,  \\
	\text{eccentricity}                  && e_f^{(4)}      & = \num{0.7308} \,,        \\
	\text{inclination}                 && i_f^{(4)}      & = \SI{28.5}{\degree} \,,  \\
	\text{longitude of ascending node} && \Omega_f^{(4)} & = \SI{269.8}{\degree} \,, \\
	\text{argument of perigee}         && \omega_f^{(4)} & = \SI{130.5}{\degree} \,, \\
	\text{true anomaly}                && \nu_f^{(4)}    & = \text{free} \,,
\end{alignat}
and the phase linkage conditions
\begin{align}
    \bm{r}(t_0^{(p+1)}) - \bm{r}(t_f^{(p)}) &= \bm{0} \,, \\
    \bm{v}(t_0^{(p+1)}) - \bm{v}(t_f^{(p)}) &= \bm{0} \,, \\
    m(t_0^{(p+1)})   - m(t_f^{(p)})         &= - m_\text{dry}^{(p)}  \,.
\end{align}
Where $T$ is the thrust modulus (constant along each phase), $g_0$ is the Earth's gravity acceleration, $I_{sp}$ is the specific impulse (constant along each phase) and $D$ is the aerodynamic drag force.
The drag force assumes an exponential model of the atmospheric air density and it is computed as
\begin{equation}
    \bm{D} = - \dfrac{1}{2} \rho  S C_D \norm{\bm{v}_{\text{rel}}} \bm{v}_{\text{rel}} \,,
\end{equation}
with
\begin{align}
	\rho			&= \rho_0 \exp{ -h/h_0 } \,, \\
    h               &= \norm{\bm{r}} - R_e \,, \\
	\bm{\Omega} 	&= \bigl[ \begin{matrix}  0 & 0 & \Omega_\oplus \end{matrix} \bigr]^\intercal \,, \\
    \bm{v}_{\text{rel}}	&= \bm{v} - \bm{\Omega} \times \bm{r} \,.
\end{align}
Where
$S$ is the reference surface area,
$C_D$ is the drag coefficient,
$\rho$ is the air density as a function of altitude,
$\rho_0$ is the standard air density at sea level,
$h$ is the altitude of the spacecraft,
$h_0$ is the density scale height,
$R_e$ is the radius of the Earth and
$\Omega_\oplus$ is the angular velocity of Earth's rotation. Tables \ref{tab:MultistageSolidConstants} and \ref{tab:MultistageSolidComponentProperties} show the values of all the constants and vehicle properties mentioned above.

It is relevant to note that the intermediate phase transition times $t_1$, $t_2$ and $t_3$ are known (fixed), but the final time of phase 4 is open and to be determined. The problem was solved with the collocation of 8, 8, 8, and 16 flipped Radau nodes on phases 1, 2, 3 and 4, respectively. The \gls{nlp} solver used was IPOPT \citep{Kawajir2015}, and the computation of partial derivatives was done through the complex step differentiation method \citep{Alonso2003,DOnofrioThesis2014}. 
\begin{table}[bt!]
\stdtable
\caption{Relevant constants and parameters for the multistage solid propellant rocket problem \citep{Patterson2014}.}
\label{tab:MultistageSolidConstants}
\begin{tabular}{@{}lcs@{}}
	\toprule
	Constant                                  &        Value        & Unit                        \\ \midrule
	Payload mass, $m_\text{payload}$          &        4164         & \kg                         \\
	Reference surface area, $S$               &       4$\pi$        & \m\squared                  \\
	Drag Coefficient, $C_D$                   &         0.5         & 1                           \\
	Air density at sea level, $\rho_0$        &        1.225        & \kg\per\meter\cubed         \\
	Density scale height, $h_0$               &        7200         & \meter                      \\
	Earth radius, $R_e$                       &       6378145       & \meter                      \\
	Earth rotation rate, $\Omega_\oplus$      & \num{7.29211585e-5} & \radian\per\second          \\
	Standard gravitational parameter, $\mu$   &  \num{3.986012e14}  & \m\cubed\per\second\squared \\
	Gravity acceleration at sea level, $g_0$  &       9.80665       & \m\per\second\squared       \\
	Phase 1 initial time, $t_0$               &          0          & \second                     \\
	Phase 1 to phase 2 transition time, $t_1$ &        75.2         & \second                     \\
	Phase 2 to phase 3 transition time, $t_2$ &        150.4        & \second                     \\
	Phase 3 to phase 4 transition time, $t_3$ &         261         & \second                     \\ \bottomrule
\end{tabular}
\end{table}
\begin{table}[bt!]
\stdtable
\caption{Component properties of the vehicle for the multistage solid propellant rocket problem \citep{Rao2010}.}
\label{tab:MultistageSolidComponentProperties}
\begin{tabular}{@{}lcccs@{}}
\toprule
&  Solid Fuel Boosters    &  Main Stage           &  Upper Stage & Unit       \\
\midrule
Total Mass, $m_\text{component}$ & 19290     & 104380    & 19300     & \si{\kg}      \\
Propellant Mass, $m_\text{prop}$ & 17010     &  95550    & 16820     & \si{\kg}      \\
Dry Mass, $m_\text{dry} = m_\text{component} - m_\text{prop}$ & 2280     & 8830     &   2480  & \si{\kg}      \\
Engine Thrust, $T$          & 628500    & 1083100   & 110094    & \si{\newton}  \\
Burn Time, $t_b$            & 75.2      & 261       & 700       & \si{\second}  \\
Number of Engines           & 9         &  1        & 1         & 1             \\
Specific Impulse, $I_{sp}$  & \multicolumn{3}{c}{$T t_b / (g_0 m_\text{prop})$}   &  \si{\second} \\
\bottomrule
\end{tabular}
\end{table}

The results for this problem are presented in Figs.~\ref{fig:Ex3_Altitude} through \ref{fig:Ex3_Hamiltonian}. More specifically, Figs.~\ref{fig:Ex3_Altitude} through \ref{fig:Ex3_Control} deal with the solution of state and control, while Figs.~\ref{fig:Ex3_PrimerVector} through \ref{fig:Ex3_Hamiltonian} are related to the dual variables and the optimality of the solution.

%% Altitude and Velocity
Figures~\ref{fig:Ex3_Altitude} and \ref{fig:Ex3_NormVel} show the altitude of the spacecraft and the norm of the velocity vector with time, respectively.
It can be seen that the altitude, expressed with respect to the Earth radius, starts at zero with a path that is tangential to the time axis, and goes to about \SI{200}{\km} where orbit insertion occurs. The profile of the altitude seems to "wobble" during the last phase where it decreases after reaching a local maximum, only to increase again for the orbit insertion point. The velocity norm starts a little above zero due to the tangential component introduced by the Earth's rotation at the surface and it follows a non-decreasing profile along the trajectory.
\begin{figure}[t!]
	\centering
	\begin{minipage}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{ChapterIV-Example3/BestPlots/Altitude.pdf}
		\caption{Spacecraft altitude vs time.}
		\label{fig:Ex3_Altitude}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{ChapterIV-Example3/BestPlots/VelocityNorm.pdf}
		\caption{Norm of the velocity vector vs time.}
		\label{fig:Ex3_NormVel}
	\end{minipage}
\end{figure}

%% Altitude and Velocity Comparison with Rao
Figures~\ref{fig:Ex3_RaoAltitude} and \ref{fig:Ex3_RaoNormVel} present a direct comparison between the results obtained with \gls{spartan} and the reference solution \citep{Rao2010}. The figures show the same variables depicted in Fig.~\ref{fig:Ex3_Altitude} and Fig.~\ref{fig:Ex3_NormVel}, respectively: altitude and norm of velocity. The two solutions are superimposed for direct comparison with the state-of-the-art solver GPOPS \citep{Rao2010}. With regards to both plots, it can be seen that the two solutions show excellent agreement with each other, being virtually indistinguishable.
\begin{figure}[bt!]
	\centering
	\begin{minipage}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{ChapterIV-Example3/BestPlots/RaoComparisonAltitude.pdf}
		\caption{Spacecraft altitude vs time. Comparison of results with the reference solution \citep{Rao2010}.}
		\label{fig:Ex3_RaoAltitude}
	\end{minipage}%
	\hfill
	\begin{minipage}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{ChapterIV-Example3/BestPlots/RaoComparisonVelocityNorm.pdf}
		\caption{Velocity norm vs time. Comparison of results with the reference solution \citep{Rao2010}.}
		\label{fig:Ex3_RaoNormVel}
	\end{minipage}
\end{figure}

%% Mass and control
The profile of the total mass of the vehicle is shown in Fig.~\ref{fig:Ex3_Mass}. In agreement with the equations of motion, the mass decays linearly due to constant mass flow rate on each phase (steeper mass gradients correspond to larger mass flow rates). Also, it is possible to see that the plot presents clear jump discontinuities at the phase transitions, this is the direct result of stage separation events where empty rocket boosters are discarded. Jump discontinuities in the mass of the system represent a main motivation for the employment of a multiphase algorithm. This characteristic of the problem makes it adequate to validate the flipped Radau pseudospectral method. 

The Cartesian components of the control, $\bm{u}$, are presented in Fig.~\ref{fig:Ex3_Control}. In this case the phase transition points are evidenced by the increased density of collocated nodes along the time domain. It can be asserted that the components assert a unit vector throughout the trajectory. The representation of thrust in unit vector form is convenient to preserve continuity, as the modulus of thrust changes instantaneously at every phase (the thrust is subject to jump discontinuities). The smoothness of the thrust unit vector along the trajectory indicates that the thrust vector, although subject to jump discontinuities in its norm, preserves smoothness in its direction regardless of the phase transitions.
\begin{figure}[bt!]
\centering
\begin{minipage}[t]{.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{ChapterIV-Example3/BestPlots/Mass.pdf}
\caption{Decay in total vehicle mass along the trajectory.}
\label{fig:Ex3_Mass}
\end{minipage}%
\hfill
\begin{minipage}[t]{.45\textwidth}
  \centering
  \includegraphics[width=\linewidth]{ChapterIV-Example3/BestPlots/Control.pdf}
\caption{Cartesian components of control. The components assert a unit vector.}
\label{fig:Ex3_Control}
\end{minipage}
\end{figure}

%% Mass Costate
With regards to Fig.~\ref{fig:Ex3_MassCostate} there a few relevant points to mention. The left plot presents the mass costate in all 4 phases, while the plot on the right shows a zoom in version of the mass costate that focuses on phases 1, 2 and 3. By inspection of both illustrations in this Figure one can extract the following information:
\begin{align}
\bm{\lambda}_m( t_0^{(2)} ) &=  0 \,, \\
\bm{\lambda}_m( t_0^{(3)} ) &=  0 \,, \\
\bm{\lambda}_m( t_f^{(4)} ) &= -1 \,.
\end{align}
These equalities indicate the validity of the endpoint conditions expressed in \eqref{EulerLagrangeX0} and \eqref{EulerLagrangeXf} from Chapter~\ref{chapter:CovectorMapping}. Namely, equation \eqref{EulerLagrangeX0} applies in phases 2 and 3, and equation \eqref{EulerLagrangeXf} applies in phase 4. Neither of these two conditions apply in phase 1. This is because the initial mass of phase 1 is constrained to a fixed value, $\var{m_0^{(1)}} = 0$ , and thus, the exclusive disjunction property applies. No further conditions apply to the endpoints of phase 1 because the mass has a constant flow-rate: a set initial value and a constant flow-rate imply a set final value in the case of fixed final time. The exclusive disjunction property applies also at the final endpoint of phase 1, $\var{m_f^{(1)}} = 0$.
With regards to phase 4, there is also an explicit constraint on the initial mass to a fixed value, $\var{m_0^{(4)}} = 0$, and the mass flow-rate is also constant, which is a similar scenario to phase 1, however, phase 4 consists of an unconstrained final time, $\var{t_f^{(4)}} \neq 0$, and this means that the final mass is also unconstrained $\var{m_f^{(4)}} \neq 0$ (although completely determined once a final time is obtained). This leaves one degree of freedom available to apply the endpoint condition in \eqref{EulerLagrangeXf}. Curiously, with regards to phases 2 and 3, either \eqref{EulerLagrangeX0} or \eqref{EulerLagrangeXf} (but not both) could be employed, as the mass is unconstrained on both endpoints of these phases. Presumably the \gls{nlp} solver "chooses" to employ the one which will minimize the Hamiltonian given an initial guess of costates, even though this decision will not affect the solution of state and control. Ultimately, the mass costate satisfies the necessary conditions described in Chapter \ref{chapter:CovectorMapping}, which is indicative of an optimal solution.
\begin{figure}[b!]
\centering
\includegraphics[width=\textwidth]{ChapterIV-Example3/MassCostate.pdf}
\caption{Left: Mass costate along the trajectory. Right: Zoom in on phases 1, 2 and 3 of the left plot.}
\label{fig:Ex3_MassCostate}
\end{figure}

%% Primer Vector Collinearity
Further, Fig.~\ref{fig:Ex3_PrimerVector} presents two pertinent plots. The plot on the left illustrates the Cartesian components of the velocity costate (or primer vector), and the plot on the right shows the norm of the cross product between this vector and the thrust unit vector (control), shown in Fig.~\ref{fig:Ex3_Control}. It is known that an optimal trajectory requires the thrust vector to be collinear with the primer vector at all times (provided that the thrust is not constrained) \citep{Vinh1973,Conway2010}. With this in mind, and by looking at the right plot of Fig.~\ref{fig:Ex3_PrimerVector}, one can notice that the norm of the cross product between thrust and primer vector is very close to zero throughout the trajectory, indicating that these two vectors are very close to being collinear. This collinearity brings further evidence to prove the optimality of the solution.
\begin{figure}[bt!]
\centering
\includegraphics[width=\textwidth]{ChapterIV-Example3/PrimerVector.pdf}
\caption{Left: Cartesian components of the velocity costate (primer vector) at each time instant. Right: Norm of the cross product between the control and the primer vector.}
\label{fig:Ex3_PrimerVector}
\end{figure}

%% Hamiltoian
It is well known that every optimal control problem can be formulated in multiple ways without changing what will be the optimal solution of state and control, but the same cannot be said about the Hamiltonian \citep{Bryson1975}. The solution of the Hamiltonian is not unique to a given trajectory of state and control. For instance, in this particular case, the mass flow-rate is constant on every phase $\dot{m} = \text{Constant}$, and therefore the problem could be formulated by having open intermediate times, $t_1$, $t_2$ and $t_3$, and fixed endpoint masses (instead of fixed times and open masses), in which case the endpoint condition in \eqref{JaVarTf} would apply at the terminal point of each phase, forcing the Hamiltonian to be zero everywhere. Nevertheless, the solution of state and control would be identical to the present one, as no additional degrees of freedom would be introduced.

By looking at Fig.~\ref{fig:Ex3_Hamiltonian}, it is clear that the Hamiltonian yielded by \gls{spartan} does not concur with the reference solution \citep{Rao2010} in its entirety. The divergence of the Hamiltonian in phases 1 and 3, is thus, most likely, due to a slight difference in formulations between the two approaches. Finding the exact formulation which yields the Hamiltonian of the original source is a "reverse-engineering" problem that goes beyond the scope of this thesis. As discussed above, although different formulations might affect the Hamiltonian solution, they do not affect the solution of state and control, provided the degrees of freedom remain the same. In other words, the divergence of the Hamiltonian is not a relevant factor to determine optimality in this case. A closer look at Fig.~\ref{fig:Ex3_Hamiltonian} reveals something that is verified in both results, namely, the validation of the endpoint condition from \eqref{JaVarTf} in phase 4,
\begin{equation}
	\mathcal{H}(t_f^{(4)}) = 0 \,.
\end{equation}
This condition is valid because the final time of phase 4 is open, $\var{t_f^{(4)}} \neq 0$. So, because the optimal Hamiltonian is constant \citep{Rao2010,Garg2011}, then it must also be zero everywhere along this phase. Notice that neither \eqref{JaVarT0} or \eqref{JaVarTf} apply to phases 1, 2 or 3, and this is because the endpoint times of these phases are fixed (exclusive disjunction property). Ultimately, Fig.~\ref{fig:Ex3_Hamiltonian} shows that \gls{spartan} yields a phase-wise constant Hamiltonian, implying that the solution is indeed optimal.
\begin{figure}[hbt!]
\centering
\includegraphics[width=.45\textwidth]{ChapterIV-Example3/BestPlots/Hamiltonian.pdf}
\caption{Comparison between the Hamiltonian obtained with \gls{spartan} and the reference solution \citep{Rao2010}.}
\label{fig:Ex3_Hamiltonian}
\end{figure}

Table~\ref{tab:Ex3_Results} presents the results of relevant parameters associated with the problem, namely the final time of phase 4 and the final mass of the vehicle (Mayer cost). The results from \citep{Rao2010} are also presented. In can be noted that the final mass of the vehicle is only a couple hundred grams higher in the solution of \gls{spartan} with regards to the solution of the original article. This increases confidence that the trajectories are virtually identical. Although the final time is unspecified in the original article, the results should be very similar as well due to the identical initial masses at the beginning of phase 4 and the constant mass flow rate during this phase. Ultimately, the results yielded by \gls{spartan} with the flipped Radau method are satisfactory.
\begin{table}[hbt!]
\stdtable
\caption{Comparison of relevant parameter results between \gls{spartan} and \citet{Rao2010}.}
\label{tab:Ex3_Results}
\begin{tabular}{@{}llls@{}}
	\toprule
	Parameter                   & SPARTAN   & \citet{Rao2010} & Unit    \\
	\midrule
	Total time of flight, $t_f$ & 924.13043 & unspecified     & \second \\
	Final mass, $m(t_f)$        & 7529.9284 & 7529.7123       & \kg     \\
	\bottomrule
\end{tabular}
\end{table}

