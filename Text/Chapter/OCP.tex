
\nextoddpage
\chapter{The Optimal Control Problem}
\label{chapter:ocp}

\section{Nature, Optimization and Optimal Control}

Nature is the ultimate optimiser. It will come up with brilliant solutions to the most intricate optimization problems imaginable, and all in real time! This is true for small time scales as well as large ones. A few examples of this are:
\begin{description}[nosep]
\item[Evolution by natural selection.] A slow process happening throughout generations with the objective of \emph{maximizing} genome prosperity. \par 
\item[Newton's law of universal gravitation.] Every particle behaves in such a way as to \emph{minimize} gravitational potential energy, from the falling of a marble to the movements of celestial bodies.
\item[Thermodynamics.] The equilibrium of a system occurs when the absolute value of temperature gradient is \emph{minimized} along the domain.
\item[Fluid mechanics.] The free surface of a fluid is in static equilibrium when the surface tension is \emph{minimized}.
\item[Osmosis.] The process by which trees absorb water and nutrients from the soil is based on the \emph{minimization} of the gradient of solute concentration.
\end{description}

Objective minimization (or maximization) is the driving force of Natural behaviours, and, arguably, the concept is present in the collective conscience of human beings. Objective minimization is so common-sense that one can even act on this concept without realizing it.For instance, one takes the shortest or fastest path from home to work and vice-versa (dynamic optimization); one tends to spend a lot of time sitting or laying-down, minimising energy expenditure (dynamic optimization); and finally, 
one tries to maximize quality-to-cost ratio when purchasing goods or subscribing to services, while subject to financial constraints (parametric optimization). Conclusively, optimization and optimal control are disciplines that have useful applications in every-day life. \par 

One characteristic of optimal control, in contrast to pure parametric optimization, is that the system in study is subject to dynamic constraints as well as algebraic ones. This means that the passing of time, or the passing of a relevant independent variable, is involved in one way or another, and the problems become slightly more complicated, and a lot more interesting. An explicit contrast between parametric optimization and optimal control is presented:
\begin{description}[nosep]
\item[Parametric optimization.] A pure parametric optimization problem is concerned with finding the values of a set of variables which satisfy a list of algebraic constraints and which minimize a relevant objective cost. \par
\item[Optimal control.] The solution to an optimal control problem is the control sequence which promotes a desired change of state of a constrained dynamic system and which minimizes the required objective functional. \par 
\end{description}


In this way, an optimal control problem (\gls{ocp}) is a formal statement that describes a plausible real world scenario where a minimization objective is required, and where a system of ordinary differential equations (\glspl{ode}) is involved.
In this chapter, the formal statement of the generic \gls{ocp} is presented in three equivalent ways, all relevant for different reasons.



\section{Continuous Domain Problem Formulation}
\label{sec:ocp_formulation}

A generic \gls{ocp} is concerned with finding the state-control sequence that promotes a desired change of state. In mathematical terms an \gls{ocp} can be formulated as follows: \par
Minimize the cost functional
\begin{equation} 	\label{OCPGenCost}
	\mathcal{J} = \vphantom{\Bigg(} \Phi \bigl(\bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr) + \nint{t_0}{t_f} \Psi \bigl( \bm{x}(t), \bm{u}(t) \bigr) \dd{t} ,
\end{equation}
associated with the trajectory of the dynamic system
\begin{equation}	\label{OCPGenEoM}
	\dot{\bm{x}}(t) = \bm{f} \bigl( \bm{x}(t), \bm{u}(t) \bigr) ,
\end{equation} 
subject to the path constraints
\begin{equation}	\label{OCPGenPathConst}
	\bm{g}_{min} \leq \bm{g} \bigl( \bm{x}(t), \bm{u}(t) \bigr) \leq \bm{g}_{max} ,
\end{equation}
and to the event constraints
\begin{equation}	\label{OCPGenBoundaries}
	\bm{\phi}_{min} \leq \bm{\phi} \bigl(\bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr) \leq \bm{\phi}_{max} .
\end{equation}

The functions above are defined by the following mappings:
\begin{alignat}{3}
\Phi &: \mathbb{R}^{n_x} &&\times \mathbb{R} \times \mathbb{R}^{n_x} \times \mathbb{R} \: &&\to \mathbb{R} \\
\Psi &: \mathbb{R}^{n_x} &&\times \mathbb{R}^{n_u} &&\to \mathbb{R} \\
\bm{f} &: \mathbb{R}^{n_x} &&\times \mathbb{R}^{n_u} &&\to \mathbb{R}^{n_x} \\
\bm{g} &: \mathbb{R}^{n_x} &&\times \mathbb{R}^{n_u} &&\to \mathbb{R}^{n_g} \\
\bm{\phi} &: \mathbb{R}^{n_x} &&\times \mathbb{R} \times \mathbb{R}^{n_x} \times \mathbb{R} &&\to \mathbb{R}^{n_\phi}
\end{alignat}
where \(n_g\) is the dimension of the path constraint vector, i.e., the number of simultaneous constraints that the system is subject to at a given time and \(n_{\psi}\) is the dimension of the event constraint vector. \par

An optimal control problem exists in the domain of an independent variable about which integration occurs. In the context of geometric problems, for instance, the independent variable can be defined as a spatial coordinate, while in the case of dynamic systems such as \glspl{olv} the independent variable is set to be the continuous time. \par 


\section{Inequality Constraint Equivalence}

The optimal control problem is an attempt to approximate the real world. In this attempt, the inclusion of inequality constraints in a generic formulation is a clear necessity. \par 

Each inequality constraint shown in section \ref{sec:ocp_formulation} by equations \eqref{OCPGenPathConst} and \eqref{OCPGenBoundaries} can be decomposed into two inequalities to be satisfied. Taking equation \eqref{OCPGenPathConst} as example, this can be represented more obviously as follows:
\begin{align*}
\begin{cases}
	\bm{g}_{max} - \bm{g}\bigl(\bm{x}(t),\bm{u}(t) \bigr) 	&\geq 0 \\
	\bm{g}\bigl(\bm{x}(t),\bm{u}(t)\bigr) - \bm{g}_{min} 	&\geq 0 
\end{cases}
\end{align*}

This double inequality constraint formulation is coincident with the formulation of 



Because the pair of constraints cannot be active at the same time (unless the boundaries are identical, making this an equality constraint), they are complementary, and can be thought of as one single constraint.


The functions \(\bm{g}\) and \(\bm{\phi} \) stand as auxiliary inequality constraints defined as:
\begin{alignat*}{4}
	\bm{0} \: \geq \: \bm{g}\bigl( \bm{x}, \bm{u} \bigr) &= \begin{bmatrix}
	 \bm{g} ( \bm{x}, \bm{u} ) - \bm{g}_{max}\\
	 \bm{g}_{min} - \bm{g} ( \bm{x}, \bm{u} )
	\end{bmatrix} , \quad
	&&\bm{g}: \mathbb{R}^{n_x} \times \mathbb{R}^{n_u} \: &&\to \mathbb{R}^{2n_g}
	\\
	\bm{0} \: \geq \: \bm{\phi}\bigl( \bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr) &= \begin{bmatrix}
	 \bm{\phi} \bigl( \bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr) - \bm{\phi}_{max}\\
	 \bm{\phi}_{min} - \bm{\phi} \bigl( \bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr)
	\end{bmatrix}, \quad
	&& \bm{\phi}: \mathbb{R}^{n_x} \times \mathbb{R} \times \mathbb{R}^{n_x} \times \mathbb{R} \: &&\to \mathbb{R}^{2n_\phi}
\end{alignat*}



\section{Hamiltonian Boundary-Value Problem}

Letting \(\mathcal{H}\) be the variational Hamiltonian and \(\mathcal{L}\) be the Lagrangian of the Hamiltonian, as in \citep{Ross2003}:
\begin{align}
	\mathcal{H}\bigl( \bm{x}, \bm{u}, \bm{\lambda} \bigr) &=
	\Psi\bigl( \bm{x}, \bm{u} \bigr) +
	\bm{\lambda}^\intercal \bm{f}\bigl( \bm{x}, \bm{u} \bigr) \\
	\mathcal{L}\bigl( \bm{x}, \bm{u}, \bm{\lambda}, \bm{\mu} \bigr) &=
	\Psi\bigl( \bm{x}, \bm{u} \bigr) +
	\bm{\lambda}^\intercal \bm{f}\bigl( \bm{x}, \bm{u} \bigr) +
	\bm{\mu}^\intercal \bm{g}\bigl( \bm{x}, \bm{u} \bigr)
\end{align}
the augmented cost functional can be written as \citep{Garg2011}
\begin{equation} \label{AugmCost}
\begin{split}
	\mathcal{J}^{\lambda} = 
	\Phi \bigr( \bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigl)
	\: + \: 
	\bm{\nu}^\intercal  \bm{\phi} \bigr( \bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigl)
	\: + \\
	\nint{t_0}{t_f} \Big\{
	\mathcal{L}\bigl( \bm{x}, \bm{u}, \bm{\lambda}, \bm{\mu} \bigr)
	\: - \: 
	\bm{\lambda}^\intercal \dot{\bm{x}}
	\Big\} \dd{t}
\end{split}
\end{equation}
where \(\bm{\lambda}(t) \in \mathbb{R}^{n_x}\) is the state covector (or costate), \(\bm{\mu}(t) \in \mathbb{R}^{2n_g}\) is the constraint covector and \(\bm{\nu}\in \mathbb{R}^{2n_\phi}\) is the endpoint covector. The covectors act as Lagrange multipliers.


% Spacing for the 1st order optimality conditions
\newcommand{\LineSpacing}{0.3em}

%The first order necessary conditions for optimality can be derived from equation \eqref{AugmCost} by setting the variation of the augmented cost functional equal to zero, \(\delta \mathcal{J}_a = 0 \), as in  \citep{Ross2003, Fahroo2008}, \citep[pp.~48,49]{Bryson1975} and \citep[p.~188]{Kirk2004}. These necessary conditions are expressed in terms of the Lagrangian of the Hamiltonian as:

The first order necessary conditions for optimality can be derived from \eqref{AugmCost} by setting the variation of the augmented cost functional equal to zero, \(\delta \mathcal{J}_a = 0 \). These necessary conditions are expressed in terms of the Lagrangian of the Hamiltonian as \citep{Bryson1975,Kirk2004,Ross2003,Fahroo2008}:
\begin{alignat}{2}
&& \bm{f}\bigl( \bm{x}, \bm{u} \bigr) - \dot{\bm{x}} &= \bm{0}
\quad : \fdv*{\mathcal{J}_a}{\bm{\lambda}} \label{JaVarLambda} \\[\LineSpacing]
\quad \bm{\mu} = \bm{0} \quad \veebar && \quad 
\bm{g} \bigl( \bm{x}, \bm{u} \bigr) &= \bm{0} 
\quad : \fdv*{\mathcal{J}_a}{\bm{\mu}} \label{JaVarMu} \\[\LineSpacing]
\bm{\nu} = \bm{0} \quad \veebar && \quad 
\bm{\phi} \bigl( \bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr) &= \bm{0} 
\quad : \fdv*{\mathcal{J}_a}{\bm{\nu}} \label{JaVarNu} \\[\LineSpacing]
 && \dot{\bm{\lambda}}^\intercal + \pdv{\mathcal{L}}{\bm{x}} \bigl( \bm{x}, \bm{u}, \bm{\lambda}, \bm{\mu} \bigr) &= \bm{0}
\quad : \fdv*{\mathcal{J}_a}{\bm{x}} \label{EulerLagrangeX} \\[\LineSpacing]
 && \pdv{\mathcal{L}}{\bm{u}} \bigl( \bm{x}, \bm{u}, \bm{\lambda}, \bm{\mu} \bigr) &= \bm{0}
\quad : \fdv*{\mathcal{J}_a}{\bm{u}} \label{EulerLagrangeU} \\[\LineSpacing]
\var{\bm{x}_0} = \var{\bm{x}(t_0)} + \dot{\bm{x}}(t_0)\var{t_0} = \bm{0} \quad \veebar && \quad
\displaystyle \pdv{\Phi}{\bm{x}(t_0)} + \bm{\nu}^\intercal \pdv{\bm{\phi}}{\bm{x}(t_0)} + \bm{\lambda}^\intercal(t_0) &= \bm{0}
\quad : \fdv*{\mathcal{J}_a}{\bm{x}_0}  \label{EulerLagrangeX0} \\[\LineSpacing]
\var{\bm{x}_f} = \var{\bm{x}(t_f)} + \dot{\bm{x}}(t_f)\var{t_f} = \bm{0} \quad \veebar && \quad
\displaystyle \pdv{\Phi}{\bm{x}(t_f)} + \bm{\nu}^\intercal\pdv{\bm{\phi}}{\bm{x}(t_f)} - \bm{\lambda}^\intercal(t_f)  &= \bm{0}
\quad : \fdv*{\mathcal{J}_a}{\bm{x}_f}  \label{EulerLagrangeXf} \\[\LineSpacing]
\quad \var{t_0} = 0 \quad \veebar && \quad 
\displaystyle \mathcal{H}\bigl( t_0 \bigr) - \pdv{\Phi}{t_0} - \bm{\nu}^\intercal\pdv{\bm{\phi}}{t_0}
&= 0 \quad : \fdv*{\mathcal{J}_a}{t_0}  \label{JaVarT0} \\[\LineSpacing]
\quad \var{t_f} = 0 \quad \veebar && \quad 
\mathcal{H}\bigl( t_f \bigr) + \pdv{\Phi}{t_f} + \bm{\nu}^\intercal\pdv{\bm{\phi}}{t_f}
&= 0 \quad : \fdv*{\mathcal{J}_a}{t_f}  \label{JaVarTf}
\end{alignat}

where

\( \veebar \) is 

\( \mathcal{H}\bigl( t_0 \bigr) \) and \( \mathcal{H}\bigl( t_f \bigr) \) are 

For simplicity, the arguments of the functions are omitted in \eqref{EulerLagrangeX0} through \eqref{JaVarTf}


Equations \eqref{JaVarLambda}, \eqref{JaVarMu} and \eqref{JaVarNu} correspond to the problem statement conditions given in equations \eqref{OCPGenEoM}, \eqref{OCPGenPathConst} and \eqref{OCPGenBoundaries}, respectively. Notice that because the path constraints, \(\bm{g} \leq \bm{0}\), and the boundary conditions, \(\bm{\Phi} \leq \bm{0}\), both consist on inequality constraints, there is a reciprocal relationship with the respective covectors: whenever \(\bm{g}\) or \(\bm{\Phi}\) is smaller than zero, it is said that the constraint is \emph{inactive} and the respective covector \(\bm{\mu}\) or \(\bm{\nu}\) will bind to zero. In contrast, the covector will be different than zero when the respective constraint is \emph{active} (equal to zero). In literature these relationships are called \emph{slackness conditions}, and they apply at every single instant of time along the trajectory for \(\bm{g}\) and at the trajectory endpoints for \(\bm{\Phi}\). \par

Equations \eqref{EulerLagrangeX}, \eqref{EulerLagrangeU}, \eqref{EulerLagrangeX0} and \eqref{EulerLagrangeXf} are known as the Euler-Lagrange equations, and equations \eqref{EulerLagrangeX0}, \eqref{EulerLagrangeXf}, \eqref{JaVarT0} and \eqref{JaVarTf} correspond to the optimality conditions at the boundaries of the problem. \par

The problem of finding a solution that satisfies all of the constraints expressed in \eqref{JaVarLambda} through \eqref{JaVarTf} is known as the Hamiltonian Boundary-Value Problem, often referred to in literature as Problem B\textsuperscript{$\lambda$} to emphasize the introduction of the covectors. \par

Notice that a solution that satisfies all of the conditions is not necessarily the optimal solution, but the optimal solution must satisfy all of the constraints. \par



\section{Discrete Hamiltonian Boundary-Value Problem}


\begin{equation}
\mathcal{J}^{\lambda N} = \Phi + \bm{\nu} \bm{\phi} + 
\dfrac{t_f-t_0}{2} \sum_{k}  w_k  \bigg\{ \Psi_k +
\bm{\lambda}_k \bigg[ f_k - \dfrac{2}{t_f - t_0} \sum_{i} \bm{D}_{ki} \bm{x}_i \bigg] +
\bm{\mu}_k \bm{g}_k \bigg\}
\end{equation}

% Spacing for the 1st order DISCRETE optimality conditions
\renewcommand{\LineSpacing}{0.3em}

\begin{alignat}{2}
&& \bm{f}_k - \sum_{i}\bm{D}_{ki}\bm{x}_i &= \bm{0} \label{JaVarLambdaN} \\[\LineSpacing]
\bm{\mu}_k = \bm{0} \quad \veebar && \quad 
\bm{g}_k &= \bm{0} \label{JaVarMuN} \\[\LineSpacing]
\bm{\nu} = \bm{0} \quad \veebar && \quad 
\bm{\phi} &= \bm{0}  \label{JaVarNuN} \\[\LineSpacing]
&& \sum_{i}\bm{D}_{ki}\bm{\lambda}_i^\intercal + \pdv{\mathcal{L}_k}{\bm{x}_k} &= \bm{0} \label{EulerLagrangeXN} \\[\LineSpacing]
&& \pdv{\mathcal{L}_k}{\bm{u}_k} &= \bm{0} \label{EulerLagrangeUN} \\[\LineSpacing]
\var{\bm{x}_0} = \bm{0} \quad \veebar && \quad
\displaystyle \pdv{\Phi}{\bm{x}_0} + \bm{\nu}^\intercal \pdv{\bm{\phi}}{\bm{x}_0} + \bm{\lambda}_0^\intercal &= \bm{0} \label{EulerLagrangeX0N} \\[\LineSpacing]
\var{\bm{x}_f} = \bm{0} \quad \veebar && \quad
\displaystyle \pdv{\Phi}{\bm{x}_f} + \bm{\nu}^\intercal\pdv{\bm{\phi}}{\bm{x}_f} - \bm{\lambda}_f^\intercal  &= \bm{0} \label{EulerLagrangeXfN} \\[\LineSpacing]
\var{t_0} = 0 \quad \veebar && \quad 
\displaystyle \mathcal{H}_0 - \pdv{\Phi}{t_0} - \bm{\nu}^\intercal\pdv{\bm{\phi}}{t_0}
&= 0 \label{JaVarT0N} \\[\LineSpacing]
\var{t_f} = 0 \quad \veebar && \quad 
\mathcal{H}_f + \pdv{\Phi}{t_f} + \bm{\nu}^\intercal\pdv{\bm{\phi}}{t_f}
&= 0 \label{JaVarTfN}
\end{alignat}


\( \mathcal{L}_k = \mathcal{L}\bigl( \bm{\lambda}_k, \bm{\mu}_k, \bm{x}_k, \bm{u}_k, t_k  \bigr) \)


\section{Chapter Closure Remarks}

An optimal control problem, as was shown, is effectively a boundary value problem.



single shooting and multiple shooting methods.



