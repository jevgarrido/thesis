
\section{Problem 1: Reusable Launch Vehicle (RLV) Descent to Vertical Landing (2 phases, 3-D trajectory)}
\label{sec:problem_one}


The first example is an application of descent-and-landing reusable technologies adapted from \citep{Wang2018}. 
In this example the descent and landing trajectory of a reusable rocket is optimized. The time of engine reignition is set as a decision variable. \par

In this problem, the main stage of a reusable launch vehicle (RLV) is to be landed vertically at a target site. The booster is in free-fall at $t=t_0=0$, and the trajectory is composed of two distinct phases:
\begin{enumerate}[nosep, left= 1.5\parindent .. 2.5\parindent]
    \item[Phase 1] Coast phase. At $t = t_0$ the booster is in free-fall at an altitude of \SI{15}{\km}. The engines are shut off during all of the first phase.
    \item[Phase 2] Landing phase. The engines ignite at $t = t_1$ (unknown time) and the rocket is controllable. The booster lands and the engines are shut down at the unknown final time $t = t_f$.
\end{enumerate}

The optimal control problem is stated as follows. Minimise the cost functional:
\begin{equation}
    \mathcal{J} = -m(t_f^{(2)})
\end{equation}
subject to the equations of motion, expressed in NED:
\begin{align} \label{eq:RLV_eom} \begin{cases}
    \dot{X}         &= R_e \cdot V_N / r \\
    \dot{Y}         &= R_e \cdot V_E / ( r \cos{\phi} )\\
    \dot{H}         &= - V_D \\
    \dot{V}_N       &= \dot{V} \cos{\gamma} \cos{\psi} - \dot{\gamma} V \sin{\gamma}\cos{\psi} - \dot{\psi}V \cos{\gamma} \sin{\psi} \\
    \dot{V}_E       &= \dot{V} \cos{\gamma} \sin{\psi} - \dot{\gamma} V \sin{\gamma}\sin{\psi} + \dot{\psi} V \cos{\gamma} \cos{\psi}\\
    \dot{V}_D       &= - \dot{V} \sin{\gamma} - \dot{\gamma}V \cos{\gamma}\\
    \dot{m}         &= - \norm{\bm{P}} / (I_{sp} g_0) \\
    \dot{\bm{P}}    &= \bm{u}
\end{cases} \end{align}
the event constraints
\begin{align}
    \begin{cases}
    X(t_0^{(1)})        &= \SI{-2400}{\meter} \\
    Y(t_0^{(1)})        &= \SI{-2800}{\meter} \\
    H(t_0^{(1)})        &= \SI{15000}{\meter} \\
    V(t_0^{(1)})        &= \SI{400}{\meter\per\second} \\
    \gamma(t_0^{(1)})   &= \SI{-70}{\deg} \\
    \psi(t_0^{(1)})     &= \SI{50}{\deg} \\
    m(t_0^{(1)})        &= \SI{35000}{\kg} \\
    \end{cases}
    \quad \quad \quad
    \begin{cases}
     \SI{-0.5}{\meter} &\leq X(t_f^{(2)}) \leq \SI{0.5}{\meter} \\
     \SI{-0.5}{\meter} &\leq Y(t_f^{(2)}) \leq \SI{0.5}{\meter} \\
    0 &\leq H(t_f^{(2)})                 \leq \SI{0.2}{\meter} \\
    0 &< V(t_f^{(2)})                    \leq \SI{0.2}{\meter\per\second} \\
    \SI{-90}{\deg} &< \gamma(t_f^{(2)})  \leq \SI{-89.5}{\deg} \\
    &\phantom{=} \psi(t_f^{(2)})     = \text{free} \\
    \SI{25000}{\kg} & \leq m(t_f^{(2)})     \\
    \end{cases}
\end{align}
And the path constraints
\begin{align}
   \bm{P}_{min} \leq  &\norm{\bm{P}} \leq \bm{P}_{max}  \label{eq:ThrustConstraint} \\
   &\abs{\dot{P}_i} \leq \dot{P}_{i,max} \quad \quad i = x,y,z \label{eq:RateConstraint}
\end{align}
with
\begin{align*}
    \bm{P}_{min} =
    \begin{cases}
        0, &t \in [ t_0, t_1 ] \\
        \SI{400}{\kilo\newton}, &t \in [ t_1,t_f ] \\
    \end{cases}
    \quad \quad \quad
    \bm{P}_{max} =
    \begin{cases}
        0, &t \in [ t_0, t_1 ] \\
        \SI{800}{\kilo\newton}, &t \in [ t_1,t_f ] \\
    \end{cases}
\end{align*}

\begin{align} \label{eq:ThrustRates}
    \dot{P}_{i,max} =
    \begin{cases}
        \SI{1000}{\newton\per\second}, & i = x\\
        \SI{2000}{\newton\per\second}, & i = y\\
        \SI{667.6}{\newton\per\second},& i = z\\
    \end{cases}
\end{align}

\begin{align*} \begin{cases}
    \dot{V}         &= -P_x/m - D/m - \mu \sin{\gamma}/r^2 \\
    \dot{\gamma}    &= P_y/(mV) + V\cos{\gamma}/r -\mu\cos{\gamma}/(Vr^2) + 2\Omega_\oplus\sin{\psi}\cos{\phi}     \\
    \dot{\psi}      &= P_z / (mV\cos{\gamma}) + V\tan{\phi}\cos{\gamma}\sin{\psi}/r - 2\Omega_\oplus ( \cos{\psi}\tanh{\gamma} \cos{\phi} - \sin{\phi} )
\end{cases} \end{align*}
and
\begin{align*} \begin{cases}
    V_N             &= V \cos{\gamma} \cos{\psi} \\
    V_E             &= V \cos{\gamma} \sin{\psi} \\
    V_D             &= - V \sin{\gamma}     \\
    \norm{\bm{P}}   &= \sqrt{ P_x^2 + P_y^2 + P_z^2 } \\
    D &= \frac{1}{2}\rho_0 \exp{-kH}V^2 S_{ref} C_D \\
    \end{cases}
    \quad \quad \quad
    \begin{cases}
    V       &= \sqrt{ V_N^2 + V_E^2 + V_D^2 } \\
    \gamma  &= - \arctan{\left( V_D / \sqrt{V_N^2 + V_E^2} \right) } \\
    \psi    &= \arctan{\left( V_E / V_N \right) } \\
    \phi    &= X / R_e \\
    r       &= H + R_e
\end{cases} \end{align*}

Where X is the crossrange (displacement along meridians); Y is the downrange (displacement along parallels); H is the altitude of the spacecraft; $V_N$, $V_E$ and $V_D$ are the North-East-Down components of the velocity vector, respectively; $m$ is the mass of the vehicle; $\bm{P} = [ P_x,\: P_y,\: P_z ]^\intercal$ is the thrust vector expressed relative to the velocity vector: $P_x$ always pointing in the opposite direction of $\bm{V} = [ V_N \: V_E \: V_D ]^\intercal$, $P_y$ positive in the direction of increasing flight path angle, and $P_z$ positive in the direction of increasing heading angle. Finally, the controls are set to be the thrust rates, as shown in the last expression of Eqs. \eqref{eq:RLV_eom}, $\bm{u} = [ \dot{P}_x \: \dot{P}_y \: \dot{P}_z ]^\intercal$. % \par 
Other relevant variables and quantities are: the norm of the velocity vector, $V = \norm{\bm{V}}$; the flight path angle, $\gamma$; the heading angle, $\psi$; latitude and longitude, $\phi$ and $\theta$ respectively. \par

The Cartesian formulations of the path constraints of thrust and thrust-rate expressed in Eqs. \eqref{eq:ThrustConstraint} and \eqref{eq:RateConstraint} respectively, are presented for the sake of simplicity and harmony with the original article, in reality both the thrust and the thrust-rates were modulated in spherical coordinates instead. \par 

Finally, the linkage conditions are omitted because there are no expected discontinuities in the states of the system ( $\Delta \bm{X}^{(1:2)} = \bm{0} $ ). \par

The constants used in this problem are expressed in Table \ref{tab:RLVConstants}. Note that  some of the needed parameters were selected based on typical assumptions for this class of rockets and mission profile, whenever not available in the corresponding reference, (e.g., the thrust rate in Eq. \eqref{eq:ThrustRates} or some environmental parameters, described in Table \ref{tab:RLVConstants}). The problem was discretized with 6 nodes in phase 1, and 20 nodes in phase 2. The results are presented in Figs. \ref{fig:RLV_rocket_states} through \ref{fig:RLV_rocket_controls}. \par

\begin{table}[!ht]
% '\stdtable' command defined in Preamble
\stdtable{1}
\caption{Relevant constants and parameters for the reusable rocket with in-flight re-ignition problem.}
\label{tab:RLVConstants}
\begin{tabular}{lcs}
\toprule
Constant                                    &  Value                & Unit                          \\
\midrule
Specific impulse, $I_{sp}$                  & 250                   & \second                       \\
Reference surface area, $S_{ref}$           & 8                     & \m\squared                    \\
Air density at sea level, $\rho_0$          & 1.225                 & \kg\per\meter\cubed           \\
Drag coefficient, $C_D$                     & 0.25                  & 1                             \\
Inverse, density scale height, $k$          & $\frac{1}{7109}$      & \per\meter                    \\
Earth radius, $R_e$                         & 6378145               & \meter                        \\
Earth rotation rate, $\Omega_\oplus$        & \num{7.29211585e-5}   & \radian\per\second            \\
Standard gravitational parameter, $\mu$     & \num{3.986012e14}     & \m\cubed\per\second\squared   \\
Gravity acceleration at sea level, $g_0$    & 9.80665               & \m\per\second\squared         \\
Phase 1 initial time, $t_0$                 & 0                     & \second                       \\
\bottomrule
\end{tabular}
\end{table}


Figure \ref{fig:RLV_rocket_states} illustrates the 6 kinematic states of the vehicle vs time. Namely, the three-dimensional components of the vectors of position and velocity. As expected, all components tend to zero, this result is satisfactory because it implies a tangential approach to the target landing site, (evident in the left column plots), serving as an indicator of a smooth landing. \par

Similarly to the previous example, Fig. \ref{fig:RLV_rocket_mass} shows a linear mass decay with time in phase 2, which indicates a constant output thrust. \par  

Figure \ref{fig:RLV_rocket_controls} illustrates the components of thrust with time. The most prominent feature of the plots is, of course, the discontinuity as a result of the sudden engine ignition at $t = t_1$. Because this problem has a "bang-bang" solution on the thrust rates, the increase-decrease corner in $P_y$ near the \SI{40}{\second} mark will "sharpen" by increasing the number of nodes used in the second phase. One notable instance of evident disagreement when comparing the solution with the source article is the profile of the thrust component $P_z$. \par 

%Figure \ref{fig:RLV_rocket_vel_vector} shows the progression of the velocity vector with time in polar coordinates, namely, (from top to bottom) the velocity vector norm, $V$, the flight path angle, $\gamma$, and the heading angle, $\psi$. The profiles are consistent with \citep{Wang2018}. \par

Key output values of the solution are the final mass of the vehicle, \SI{25.98e3}{\kg}; the ignition time, $t_1$ = \SI{17.6038}{\second} (transition time from phase 1 to phase 2); and the final time of the trajectory, $t_f$ = \SI{43.8545}{\second}. The results presented in Figs. \ref{fig:RLV_rocket_states} through \ref{fig:RLV_rocket_controls} show satisfactory agreement with \citep{Wang2018}. Slight discrepancies in the results may be due to differences in the choice of some constants, whose values were part of the assumptions made for this work. \par


\begin{figure}[hbt!]
\centering
\includegraphics[width=.6\textwidth]{ChapterIV-Example1/states.pdf}
\caption{Two-phases RLV scenario - Kinematics vs time, North-East-Down components of position (right column) and velocity (left column).}
\label{fig:RLV_rocket_states}
\end{figure}

\begin{figure}[hbt!]
\centering
\includegraphics[width=.6\textwidth]{ChapterIV-Example1/mass.pdf}
\caption{Two-phases RLV scenario - Mass of the vehicle.}
\label{fig:RLV_rocket_mass}
\end{figure}

\begin{figure}[hbt!]
\centering
\includegraphics[width=.6\textwidth]{ChapterIV-Example1/controls.pdf}
\caption{Two-phases RLV scenario - Thrust components.}
\label{fig:RLV_rocket_controls}
\end{figure}


%% Control Rates (not interesting)
%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=.7\textwidth]{ChapterIV-Example1/control_rates.pdf}
%\caption{Two-phases RLV scenario - Control rates}
%\label{fig:RLV_rocket_control_rates}
%\end{figure}

%% Constraints (not used)
%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=.7\textwidth]{ChapterIV-Example1/qdyn_Qheat.pdf}
%\caption{Two-phases RLV scenario - Constraints}
%\label{fig:RLV_rocket_constraints}
%\end{figure}

%\hl{place fig \ref{fig:RLV_rocket_traj} and \ref{fig:RLV_rocket_vel_vector} side by side}

%% Trajectory Plot (too tall, not very interesting)
%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=.7\textwidth]{ChapterIV-Example1/traj.pdf}
%\caption{Two-phases RLV scenario - Trajectory}
%\label{fig:RLV_rocket_traj}
%\end{figure}

%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=.7\textwidth]{ChapterIV-Example1/Vfpa.pdf}
%\caption{Two-phases RLV scenario - velocity vector}
%\label{fig:RLV_rocket_vel_vector}
%\end{figure}
