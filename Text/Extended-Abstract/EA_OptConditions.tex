
\section{Optimality Conditions}
\label{sec:opt_conditions}

The classical approach (indirect methods) of finding a solution to an optimal control problem begins by deriving the first order optimality conditions. In order to do so, the cost functional \eqref{MP_OCPGenCost} is augmented by means of the complementary vectors, $\bm{\nu}$, $\bm{\lambda}$ and $\bm{\mu}$, to include all constraints expressed in \eqref{MP_OCPGenEoM} to \eqref{MP_OCPGenBoundaries}. This process is often referred to as \emph{dualization} \citep{Benson2006}. It is important to discuss these conditions in order to verify if a trajectory obtained with the flipped Radau pseudospectral method is optimal.
Omitting the phase indicator $ p $ for simplicity, one can define the augmented Hamiltonian as \citep{Ross2003}:
\begin{equation}
\mathcal{H}\bigl( \bm{x}, \bm{u}, \bm{\lambda}, \bm{\mu} \bigr) =
\Psi\bigl( \bm{x}, \bm{u} \bigr) +
\bm{\lambda}^\intercal \bm{f}\bigl( \bm{x}, \bm{u} \bigr) +
\bm{\mu}^\intercal \bm{h}\bigl( \bm{x}, \bm{u} \bigr) \,, \label{eq:Hamiltonian}
\end{equation}
and the augmented cost functional is written with respect to the Hamiltonian as \citep{Garg2011}
\begin{equation} \label{AugmCost}
	\mathcal{J}^{\lambda} = 
	\Phi + \bm{\nu}^\intercal  \bm{\phi}
	+ \nint{t_0}{t_f} \bigl( \mathcal{H} - \bm{\lambda}^\intercal \dot{\bm{x}} \bigr) \dd{t} \,,
\end{equation}
where \(\bm{\lambda}(t) \in \mathbb{R}^{n_{\bm{x}}}\) is the state covector (or costate), \(\bm{\mu}(t) \in \mathbb{R}^{n_{\bm{h}}}\) is the constraint covector and \(\bm{\nu} \in \mathbb{R}^{n_{\bm{\phi}}}\) is the endpoint covector.

% Spacing for the 1st order optimality conditions
\newcommand{\LineSpacing}{0.3em}
The first order necessary conditions for optimality can be derived from \eqref{AugmCost} by setting the first variation of the augmented cost functional equal to zero, \(\delta \mathcal{J}^{\lambda} = 0 \) \citep{Bryson1975,Kirk2004}. These necessary conditions are expressed in terms of the Hamiltonian as \citep{Bryson1975,Kirk2004,Ross2003,Fahroo2008}:
\begin{align}
	\bm{f}\bigl( \bm{x}, \bm{u} \bigr) - \dot{\bm{x}}                                                                     & = \bm{0} \,, \label{JaVarLambda}     \\[\LineSpacing]
	\bm{\mu}^\intercal \bm{h} \bigl( \bm{x}, \bm{u} \bigr)                                                                & = 0 \,,  \label{JaVarMu}             \\[\LineSpacing]
	\bm{\nu}^\intercal \bm{\phi} \bigl( \bm{x}(t_0), t_0, \bm{x}(t_f), t_f \bigr)                                         & = 0 \,, \label{JaVarNu}              \\[\LineSpacing]
	\pdv{\mathcal{H}}{\bm{u}} \bigl( \bm{x}, \bm{u}, \bm{\lambda}, \bm{\mu} \bigr)                                        & = \bm{0} \,, \label{EulerLagrangeU}  \\[\LineSpacing]
	\dot{\bm{\lambda}}^\intercal + \pdv{\mathcal{H}}{\bm{x}} \bigl( \bm{x}, \bm{u}, \bm{\lambda}, \bm{\mu} \bigr)         & = \bm{0} \,, \label{EulerLagrangeX}  \\[\LineSpacing]
	\pdv{\Phi}{\bm{x}(t_f)} + \bm{\nu}^\intercal\pdv{\bm{\phi}}{\bm{x}(t_f)} - \bm{\lambda}^\intercal(t_f)  & = \bm{0} \,, \label{EulerLagrangeXf} \\[\LineSpacing]
	\mathcal{H}\bigl( t_f \bigr) + \pdv{\Phi}{t_f} + \bm{\nu}^\intercal\pdv{\bm{\phi}}{t_f}                               & = 0 \,, \label{JaVarTf}
\end{align}
where $\mathcal{H}\bigl( t_f \bigr)$ is the Hamiltonian evaluated at $t_f$, $ \mathcal{H}\bigl( t_f \bigr) = \mathcal{H}\bigl( \bm{x}(t_f), \bm{u}(t_f), \bm{\lambda}(t_f) \bigr) $.

Equations \eqref{JaVarLambda}, \eqref{JaVarMu} and \eqref{JaVarNu} result from the variation of the augmented cost about the three covectors, \(\bm{\lambda}\), \(\bm{\mu}\) and \(\bm{\nu}\), respectively, and they are a rewriting of the constraints of the original problem statement in \eqref{MP_OCPGenEoM} through \eqref{MP_OCPGenBoundaries}. One interesting result that yields from the optimality conditions is the fact that the inequality constraints \eqref{MP_OCPGenPathConst} and \eqref{MP_OCPGenBoundaries} are turned into equality constraints by means of a product with the respective covectors: the conditions are satisfied whenever the inequality constraints are active (\(\bm{h} = \bm{0}\) and/or \(\bm{\phi} = \bm{0}\)), and they can be satisfied when the constraints are inactive (\(\bm{h} < \bm{0}\) and/or \(\bm{\phi} < \bm{0}\)) by making sure that the covectors bind to zero at the corresponding times (\(\bm{\mu} = \bm{0}\) and/or \(\bm{\nu} = \bm{0}\)), thus ensuring a null product in \eqref{JaVarMu} and \eqref{JaVarNu}. Because of this, \eqref{JaVarMu} and \eqref{JaVarNu} are often referred to as \emph{slackness conditions}. \par 

Equations \eqref{EulerLagrangeU} and \eqref{EulerLagrangeX} are called the \emph{Euler-Lagrange} equations and they apply at every point along the domain. In particular \eqref{EulerLagrangeX} is called the \emph{costate equation} because it describes the rate of change of the costate, effectively serving as a new equation of motion corresponding to the costates. Equation \eqref{EulerLagrangeU} is also referred to as the strong form of Pontryagin's minimum principle and it only applies if the optimal control sequence lies exclusively within the allowable control set.

Finally \eqref{EulerLagrangeXf} and \eqref{JaVarTf} are the endpoint conditions that must be satisfied for the cases of unknown final state and unknown final time, respectively \citep{Bryson1975}.

In addition, because it is not an explicit function of time, the Hamiltonian of the optimal solution will be a constant \citep{Bryson1975,Garg2011},:
\begin{equation}
	\mathcal{H}\bigl( \bm{x}(t), \bm{u}(t), \bm{\lambda}(t) \bigr) = \mathcal{H}(t_f) = \mathcal{H}^{\dag} \,,
\end{equation}
where $ \mathcal{H}^{\dag} $ represents a generic constant scalar.

The problem of finding a solution that satisfies all first order optimality conditions is known as the \emph{Hamiltonian boundary value problem} (\gls{hbvp}).

