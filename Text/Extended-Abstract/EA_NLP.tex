
\section{NLP Solver Formatting}
\label{sub:nlp_formatting}


\subsection{Generic input format of the nonlinear solver}

In order to solve an optimal control problem via the Radau pseudospectral method, one needs to transcribe the original problem statement into a format which a nonlinear solver can interpret. A nonlinear program is expressed as~\citep{Gill2017,Waechter2005,Kawajir2015} minimizing
\begin{equation}
	J(\bm{X}_{\text{NLP}}) \,,
\end{equation}
subject to
\begin{align}
	\bm{C}_\text{min} \leq \bm{C}(\bm{X}_{\text{NLP}})  &\leq \bm{C}_\text{max} \,, \\
	\bm{X}_\text{min} \leq \mask{\bm{C}(\bm{X}_{\text{NLP}})}{\bm{X}_{\text{NLP}}} &\leq \bm{X}_\text{max} \,.
\end{align}
where $\bm{C}_\text{max}$ and $\bm{C}_\text{min}$ are, respectively, the upper and lower boundary vectors of the constraints, and $\bm{X}_\text{max}$ and $\bm{X}_\text{min}$ are the upper and lower boundary vectors of the decision variables, respectively.
Essentially, there is a cost function, \( J(\bm{X}_{\text{NLP}}) \), to be minimized, a list of algebraic constraints, \(\bm{C}(\bm{X}_{\text{NLP}})\), to be satisfied and a list of parameters, \(\bm{X}_{\text{NLP}}\), that act as decision variables. In order to transform the optimal control problem into a \gls{nlp} it is necessary to concatenate all the decision variables into a main vector and to aggregate all constraints into a main constraint vector, this is depicted by vectors $\bm{X}_{\text{NLP}}$ and $\bm{C}(\bm{X}_{\text{NLP}})$ respectively. The algebraic constraints contained in $\bm{C}(\bm{\bm{X}_{\text{NLP}}})$ can assert either an equality or an inequality by setting $\bm{C}_\text{max} = \bm{C}_\text{min} $ or $\bm{C}_\text{max} > \bm{C}_\text{min}$ respectively.


\subsection{Formatting the vector of decision variables}

In order to construct a nonlinear programming problem one needs both, a list of decision variables and a list of algebraic constraints. Let
\begin{multline}
	\bm{XU}^{(p)} = \bigl[ \begin{matrix}
		\transpose{ \bm{x}_0^{(p)} } &
		\transpose{ \bm{x}_1^{(p)} } &
		\transpose{ \bm{u}_1^{(p)} } &
		\dots
	\end{matrix} \\ \begin{matrix}
		\transpose{ \bm{x}_k^{(p)} } &
		\transpose{ \bm{u}_k^{(p)} } &
		\dots & 
		\transpose{ \bm{x}_{N^{(p)}}^{(p)} } &
		\transpose{ \bm{u}_{N^{(p)}}^{(p)} }
	\end{matrix} \bigr]^\intercal \,,
\end{multline}
be the concatenated decision vector of state and control associated with the nodes of a given phase $p$. Notice that there is no control variable associated with the very first node $i = 0$, this is because this node is not collocated \citep{Sagliano2017c}. The complete vector of decision variables can be expressed as
\begin{multline} \label{eq:XNLP}
	\bm{X}_{\text{NLP}} = \bigl[ \begin{matrix}
		\transpose{\bm{XU}^{(1)}} &
		\dots &
		\transpose{\bm{XU}^{(p)}} &
		\dots 
	\end{matrix} \\ \begin{matrix}
		\transpose{\bm{XU}^{(P)}} &
		t_f^{(1)} &
		\dots &
		t_f^{(p)} &
		\dots &
		t_f^{(P)}
	\end{matrix} \bigr]^\intercal \,.
\end{multline}
The lower and upper boundaries of the vector of decision variables, $\bm{X}_\text{min}$ and $\bm{X}_\text{max}$ can be build according to an analogous procedure, given the parameters specified by the user for each problem.


\subsection{Constraints formatting}

The cost functional fed to the nonlinear solver is identical to the cost described in \eqref{eq:mp_nlp_cost}, thus,
\begin{equation}
	J(\bm{X}_{\text{NLP}}) = \mathcal{J}^N \,.
\end{equation}
In order to construct the concatenated vector, $ \bm{C}(\bm{X}_{\text{NLP}}) $, two additional arrays are introduced, namely $\bm{F}^{(p)}$ and $\bm{H}^{(p)}$, containing all the constraints corresponding to phase $p$ from \eqref{eq:mp_spec_Ddyn} and \eqref{eq:mp_spec_path}:
\begin{align}
	\bm{F}^{(p)} &= \begin{bmatrix}
		\mask{\transpose{ \bm{h}_1^{(p)} }}{\transpose{ \Ddyn_1^{(p)} }} & \mask{\transpose{ \bm{h}_2^{(p)} }}{\transpose{ \Ddyn_2^{(p)} }} & \dots & \mask{\transpose{ \bm{h}_k^{(p)} }}{\transpose{ \Ddyn_k^{(p)} }} & \dots & \mask{\transpose{ \bm{h}_{N^{(p)}}^{(p)} }}{\transpose{ \Ddyn_{N^{(p)}}^{(p)} }}
	\end{bmatrix}^\intercal
	\\
	\bm{H}^{(p)} &= \begin{bmatrix}
		\transpose{ \bm{h}_1^{(p)} } & \transpose{ \bm{h}_2^{(p)} } & \dots & \transpose{ \bm{h}_k^{(p)} } & \dots & \transpose{ \bm{h}_{N^{(p)}}^{(p)} }
	\end{bmatrix}^\intercal
\end{align}
where \(\Ddyn_k^{(p)}\) and \(\bm{h}_k^{(p)}\) are as in \eqref{eq:mp_spec_Ddyn} and \eqref{eq:mp_spec_path}, respectively. This way, the concatenated vector of constraints can be written as 
\begin{multline}
	\bm{C}(\bm{\bm{X}_{\text{NLP}}}) = \bigl[
	\begin{matrix}
		\transpose{ \bm{F}^{(1)} } &
		\dots &
		\transpose{ \bm{F}^{(P)} } &
		\transpose{ \bm{H}^{(1)} } &
		\dots
	\end{matrix} \\ \begin{matrix}
		\transpose{ \bm{H}^{(P)} } &
		\transpose{ \bm{\phi}^{(1)} } &
		\dots &
		\transpose{ \bm{\phi}^{(P)} } &
		\transpose{ \fLink^{(1)} } &
		\dots &
		\transpose{ \fLink^{(P-1)} }
	\end{matrix} \bigr]^\intercal \,.
\end{multline}
The lower and upper boundaries for the vector of constraints, $\bm{C}_\text{min}$ and $\bm{C}_\text{max}$, are constructed in an analogous manner, with the peculiarity that the boundaries corresponding to the dynamic defects, must be set invariably equal to zero, \( \bm{F}^{(1:P)} = \bm{0} \), such that the dynamics are enforced and that the trajectory satisfies the equations of motion at all times.


\subsection{Jacobian Matrix}

The Jacobian matrix is a quantitative description of the derivatives of the constraints with respect to the decision variables, providing a first order gradient that is necessary to aid the nonlinear solver. This section serves to overview the components of the Jacobian matrix that results from a discretization based on the flipped Radau method. Each row of the Jacobian matrix corresponds to an algebraic constraint, and each column corresponds to a decision variable. Ultimately, each element represents a linear dependency between a constraint (row) with respect to a decision variable (column). The Jacobian can be expressed as \citep{Sagliano2017c}
\begin{equation} \label{eq:JacobianGeneric}
	\textbf{Jac} =
	\nabla_{\bm{X}_{\text{NLP}}} \begin{bmatrix} J(\bm{X}_{\text{NLP}}) \\ \bm{C}(\bm{X}_{\text{NLP}}) \end{bmatrix} =
	\begin{bmatrix}
		\nabla \mathcal{J}^N \\
		\nabla \bm{F}^{(1:P)} \\
		\nabla \bm{H}^{(1:P)} \\
		\nabla \bm{\phi}^{(1:P)} \\
		\nabla \fLink^{(1:P-1)}
	\end{bmatrix}  \,.
\end{equation}
The format of the vectors $\bm{X}_\text{NLP}$ and $\bm{C}(\bm{X}_\text{NLP})$ dictate the sparsity pattern of the Jacobian matrix.
Figure~\ref{fig:JacobianSparisty} is a visual representation of the pattern of the Jacobian for an example problem with four phases. In this Figure, zero elements are represented by white space, while non-zero elements are represented with coloured dots. The first row of the matrix corresponds to the discrete cost functional where the magenta circles represent a Lagrangian cost and the blue dot at the end of phase four represents a Mayer cost; there are four open terminal times (green columns); a scalar path constraint applied to all phases (concatenated diagonals in cyan); vector event constraints at the terminal times of every phase (magenta blocks); and three linkage conditions connecting the four phases sequentially (red diagonals at the bottom rows). The four phases can be distinguished by the four large red blocks with blue diagonal smaller blocks (corresponding to the dynamic defects of each phase). The prominent red diagonals in the Jacobian correspond to the sparsity pattern of the differentiation matrices and to the identity matrices corresponding to the linkage conditions. The values of the Jacobian matrix corresponding to the red dots are static, i.e., they will remain unchanged in every iteration loop.
\begin{figure*}[hbt!]
	\centering
	\includegraphics[width=0.7\textwidth]{ChapterIII/JacobianStructureEdit.pdf}
	\caption{Sparsity pattern of the Jacobian matrix for an example problem with 4 phases, 5 collocation points on each phase. 4 open terminal times. One scalar path constraint applying to every phase and a terminal constraint vector applying to every phase. White spacing represents zero-valued elements.}
	\label{fig:JacobianSparisty}
\end{figure*}

\subsection{Overview of the Solving Procedure}

In order to solve a given optimal control problem a procedural approach is taken. This procedure needs to be generic in order to be able to handle as many problems as possible. The main task at hand is to perform a transformation of the user input into variables and constraints that are useful to feed an \gls{nlp} solver. The following description applies to the MATLAB tool \gls{spartan} developed at \gls{dlr} \citep{Sagliano2017a,Sagliano2017c}. Figure~\ref{fig:procedure_flowchart} presents a high-lever overview of the solving procedure implemented in \gls{spartan} for visual aid. 
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.8\linewidth]{ChapterIII/SpartanFlowchart.eps}
	\caption{Flowchart of the solving process by the MATLAB tool \gls{spartan}.}
	\label{fig:procedure_flowchart}
\end{figure}

