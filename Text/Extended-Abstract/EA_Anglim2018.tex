
\section{Rocket Boost-Back and Vertical Landing Problem}
\label{sec:problem_one}

The problem to consider is an adaptation from \citep{Anglim2017}. This example is concerned with the recovery of the first stage of an orbital launcher via vertical landing, where the landing target is located close to the launching site (return-to-launch-site scenario). The vehicle in question is based on the characteristics of the main booster of SpaceX's Falcon 9 rocket. This problem is appropriate to validate the algorithm developed in this work because it is composed of three distinct phases, resulting from the mutation of the equations of motion from one phase to the next. The algorithm is validated by analysis of the optimality conditions.

The problem occurs in a vertical, two-dimensional plane (2-D). At the beginning of the trajectory it is assumed that stage separation has already occurred and that the booster has performed the "turn back" attitude manoeuvre such that the boost-back-burn may begin. The trajectory is composed of three phases:
\begin{description}[nosep]
	\item[Phase 1] Boost-back. At $t = t_0$ the rocket is in mid air with ascending linear momentum. Three engines (out of nine) ignite and the thrust direction is constrained to point strictly horizontally. In this phase the rocket inverts its horizontal motion. The duration of the boost-back burn, $t = t_1$, is a known, fixed parameter.
	\item[Phase 2] Coast arc. At $t = t_1$ the engines shut down and remain off during all of phase 2. The vehicle takes a parabolic flight profile during this phase. It is taken for granted that an attitude justification manoeuvre takes place during this phase in order to roughly align the thrust vector with the velocity vector at the beginning of phase 3.
	\item[Phase 3] Landing. At $t = t_2$ one engine (out of nine) ignites. The vehicle is now controllable and the booster makes its way to the target site. Touch-down happens at $t = t_f$. Both $t_2$ and $t_f$ are unknown variables to be determined.
\end{description}

The equations of motion are formulated in the target-centred reference frame \citep{Anglim2017}. Throughout the trajectory, both position and velocity are modelled in Cartesian coordinates, with the reference frame of displacement centred at the landing target location. Figure~\ref{fig:f9_free_body_diagram} presents a free-body diagram of the vehicle for visual reference. Downrange is represented by the horizontal axis, $x$, increasing from left to right, and altitude is represented by the vertical axis, $y$, increasing from bottom to top. The angle $\theta$ indicates the direction of the thrust vector, $\bm{T}$, and the flight-path angle, represented by the angle $\gamma$, indicates the direction of the velocity vector, $\bm{v}$. Both angles are measured from the $x$ axis and positive in the direction of increasing $y$. The Cartesian components of velocity, $v_x$ and $v_y$, are positive in the direction of increasing respective position coordinates. The aerodynamic drag force is represented by vector $\bm{D}$ which is always collinear with the velocity vector, and $m$ is the mass of the vehicle. The Earth is assumed to be flat and non rotating, and the acceleration of gravity is assumed to be invariant with altitude throughout the trajectory, taking the value $g_0$.
The control variable is the thrust tilt angle, $\theta$. A point-mass approximation of the vehicle is employed, thus the attitude of the spacecraft is not modelled and the angle of attack is assumed to be zero at all times. An exponential model is used for the drag force.
\begin{figure}[hbt!]
	\centering
	\includegraphics[height=40mm, keepaspectratio]{ChapterIV-Example2/free_body_diagram.pdf}
	\caption{Free-body diagram of the vehicle.}
	\label{fig:f9_free_body_diagram}
\end{figure}
The optimal control problem can be formulated as minimizing the cost
\begin{equation}
	\mathcal{J} = \Phi \bigl( m(t_f) \bigr)  = -m( t_f ) \,,
\end{equation}
associated with the dynamic system
\begin{align}
	\dot{ x }   & = v_x \,,                                                           \\
	\dot{ y }   & = v_y \,,                                                           \\
	\dot{ v_x } & = \dfrac{T}{m} k \cos{\theta} - \dfrac{D}{m} \cos{\gamma} \,,       \\
	\dot{ v_y } & = \dfrac{T}{m} k \sin{\theta} - \dfrac{D}{m} \sin{\gamma} - g_0 \,, \\
	\dot{ m }   & = -\dfrac{T}{I_{sp}g_0} k
\end{align}
and subject to the event constraints
\begin{align}
	x(t_0)                                 & = \SI{36.022}{\km} \,,                                \\
	y(t_0)                                 & = \SI{60.708}{\km} \,,                                \\
	v_x(t_0)                               & = \SI{1.052}{\km\per\second} \label{f9_initialVx} \,, \\
	v_y(t_0)                               & = \SI{1.060}{\km\per\second} \label{f9_initialVy} \,, \\
	x(t_f)                                 & = \SI{0}{\km} \,,                                     \\
	y(t_f)                                 & = \SI{0}{\km} \,,                                     \\
	v_x(t_f)                               & = \SI{0}{\km\per\second} \,,                          \\
	\SI{-0.5}{\m\per\second}\leq  v_y(t_f) & \leq \SI{0.5}{\m\per\second} \label{f9_finalVy} \,,   
\end{align}
with
\begin{align}
	D            & = \frac{1}{2} \rho_0 \exp{-\frac{y}{h_0}} C_D \pi \frac{d^2}{4} v^2 \,, \\
	v^2          & = v_x^2 + v_y^2 \,,                                                     \\
	\cos{\gamma} & = \dfrac{v_x}{v} \,,                                                    \\
	\sin{\gamma} & = \dfrac{v_y}{v} \,,
\end{align}
and
\begin{equation}
	T = \begin{cases}
		\frac{1}{3} T_{LO} &t \in [ t_0,t_1 ]  \\
		0 &t \in [ t_1,t_2 ] \\
		\frac{1}{9} T_{LO} &t \in [ t_2,t_3 ]
	\end{cases}
	\quad 
	k = \begin{cases}
		1 &t \in [ t_0,t_1 ] \\
		0 &t \in [ t_1,t_2 ] \\
		1 &t \in [ t_2,t_3 ] 
	\end{cases}
	\,,
\end{equation}
where
$k$ is the engine throttle,
$\rho_0$ is the air density at sea level,
$h_0$ is the density scale height,
$C_D$ is the drag coefficient,
$d$ is the diameter of the vehicle and
$v$ is the norm of the velocity vector. 
The linkage conditions are omitted because there are no expected jump discontinuities on the sates of the systems in any phase transition. Table~\ref{tab:Falcon9Constants} shows the constants and parameters used in this problem.
\begin{table}[hbt!]
	\stdtable
	\caption{Relevant constants and parameters for the Falcon 9 first stage recovery problem.}
	\label{tab:Falcon9Constants}
	\begin{tabular}{@{}lcs@{}}
		\toprule
		Constant                        &  Value  & Unit                  \\ \midrule
		Specific impulse, $I_{sp}$      &   282   & \second               \\
		Lift-off Thrust, $T_{LO}$       &  5886   & \kilo\newton          \\
		Rocket diameter, $d$            &  3.66   & \m                    \\
		Drag Coefficient, $C_D$         &  0.75   & 1                     \\
		Density scale height, $h_0$     &  7500   & \meter                \\
		Gravity acceleration, $g_0$     & 9.80665 & \m\per\second\squared \\
		Sea level air density, $\rho_0$ &  1.225  & \kg\per\meter\cubed   \\
		Phase 1 initial time, $t_0$     &    0    & \second               \\
		Boost-back duration, $t_1$      &  40.8   & \second               \\
		Dry mass, $m_\text{dry}$        &  25600  & \kg                   \\ \bottomrule
	\end{tabular}
\end{table}
This problem was solved using 10, 8 and 12 collocation nodes in phases 1, 2 and 3, respectively. The \gls{nlp} solver used was IPOPT~\citep{Kawajir2015}, and the method used to compute partial derivative was the complex step differentiation method \citep{DOnofrioThesis2014}. The results are depicted in Figs.~\ref{fig:f9_Trajectory} through \ref{fig:f9_Hamiltonian}. In addition, the resulting parameters of unknown times and final mass are presented in Table~\ref{tab:f9_Results}.

%% Trajectory
The trajectory of the vehicle is shown in Fig.~\ref{fig:f9_Trajectory}, where the downrange and the altitude are plotted against each other. The boost-back phase is shown in blue, the coasting arc is shown in orange and the landing phase is represented in yellow. Curiously, the vehicle traces a path that vaguely resembles a shepherd's staff. The plot clearly shows that at the beginning of the trajectory the vehicle is travelling from left to right and with ascending velocity. The boost-back burn inverts the horizontal motion of the vehicle and during the coasting arc, the vehicle takes a parabolic flight due to being in complete free-fall. Finally, in the landing phase the vehicle approaches the zenith of the target and the flight path angle gets closer and closer to being vertical. Intuitively speaking, the trajectory follows a predictable path.
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/Trajectory.pdf}
	\caption{Altitude vs downrange trajectory.}
	\label{fig:f9_Trajectory}
\end{figure}

%% Position and velocity components
Figure~\ref{fig:f9_Velocity} illustrate the Cartesian components of velocity with time. The phase transition points are visible through the increase in density of collocation nodes and also through the removable discontinuities present in each component. The horizontal component of velocity decreases linearly during the first phase and goes from positive to negative, inverting the direction of flight, which is indicative of the boost-back burn phase. This component stays constant during the coasting phase, as expected. It is noticeable that during phases 1 and 2, the vertical component of velocity decreases linearly, without noticeable discontinuities, indicating that this component has been subject to a constant acceleration during these two phases, undoubtedly the acceleration of gravity. It is also visible that the vertical component of velocity goes from positive to negative close to the $\SI{110}{\second}$ mark, inverting the direction flight. Both velocity components go to zero during the landing phase, as expected.
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/Velocity.pdf}
	\caption{Cartesian components of velocity with time.}
	\label{fig:f9_Velocity}
\end{figure}

%% Mass profile with time
The mass profile of the vehicle can be seen in Fig.~\ref{fig:f9_Mass}. Due to the constant throttle level, and the constant specific impulses, the mass of the vehicle decays linearly during phases 1 and 3. The mass flow-rate is higher in phase 1 due to the larger thrust associated --- there are three engines on during phase 1 and only one engine ignited during phase 3. Not remarkably, the mass stays constant during phase 2 (coast phase).
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/Mass.pdf}
	\caption{Mass of the vehicle with time.}
	\label{fig:f9_Mass}
\end{figure}

%% Thrust direction (control)
The angle indicating the thrust direction (control) is presented in Fig.~\ref{fig:f9_ThrustDirection}. It can be seen that the direction of thrust is constrained to $\SI{180}{\degree}$ during phase 1, and constrained to $\SI{0}{\degree}$ during phase 2 (during phase 2 the thrust direction is inconsequential due to the throttle being constrained to zero). Finally, in the landing phase the direction of thrust is allowed to vary, and the angle draws a curve that approaches $\SI{90}{\degree}$, indicating that the thrust points vertically at the end of the trajectory.
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/ThrustDirection.pdf}
	\captionof{figure}{Thrust direction, $\theta$, with time.}
	\label{fig:f9_ThrustDirection}
\end{figure}

%% Mass costate
Moving on to the solution of dual variables, Fig.~\ref{fig:f9_MassCostate} shows the evolution of the mass costate along time. Recalling the first order necessary conditions from Section \ref{sec:opt_conditions}, it is possible to note that the endpoint condition expressed in \eqref{EulerLagrangeXf} is verified at the final time of the trajectory, namely
\begin{equation}
	\lambda_m(t_f) = \pdv{\Phi}{m(t_f)} = -1 \,.
\end{equation}
This condition is not verified at any other phase endpoint due to the fact that the mass is either fixed at those points or completely determined by the initial conditions, the constant mass flow rates and the fixed endpoint time of phase 1. Ultimately, the validation of the endpoint condition at the final time of the trajectory brings confidence that the solution is optimal.
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/MassCostate.pdf}
	\caption{Mass costate with time. The mass costate takes the value -1 at the final time.}
	\label{fig:f9_MassCostate}
\end{figure}

%% Hamiltonian
Finally, Fig.~\ref{fig:f9_Hamiltonian} shows the Hamiltonian associated with the trajectory. 
By inspection of the plot, one can assert that the Hamiltonian is phase-wise constant, and with regards to phases 2 and 3, one can verify that the Hamiltonian is zero, which implies that the endpoint condition \eqref{JaVarTf} is satisfied in these phases:
\begin{equation}
	\mathcal{H} \bigl(t_f^{(2)}\bigr) = \mathcal{H} \bigl(t_f^{(3)}\bigr) = 0 \,,
\end{equation}
Because the final time of phase 1 is fixed, the endpoint condition \eqref{JaVarTf} does not apply to that phase.
Briefly stated, the Hamiltonian satisfies the optimality conditions for time invariant systems \citep{Bryson1975,Rao2010,Garg2011}, and therefore this result brings confirmation that the solution is optimal.
\begin{figure}[hbt!]
	\centering
	\includegraphics[width=0.45\textwidth]{ChapterIV-Example2/BestPlots/Hamiltonian.pdf}
	\caption{Hamiltonian vs time. Hamiltonian is zero during phases 2 and 3.}
	\label{fig:f9_Hamiltonian}
\end{figure}

Table~\ref{tab:f9_Results} presents the values of pertinent variables obtained with \gls{spartan}. The results are overall satisfactory.
\begin{table}[t!]
	\stdtable
	\caption{Relevant optimization parameters obtained with \gls{spartan}.}
	\label{tab:f9_Results}
	\begin{tabular}{@{}lls@{}}
		\toprule
		Parameter							& SPARTAN		& Unit		\\
		\midrule
		Boost-back burn duration, $t_1$		& 40.8			& \second	\\
		Landing burn start time, $t_2$		& 220.4583		& \second	\\
		Total time of flight, $t_f$ 		& 303.5469		& \second	\\
		Final mass, $m(t_f)$				& 27905.5554	& \kg		\\
		\bottomrule
	\end{tabular}
\end{table}
