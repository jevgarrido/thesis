
\section{Multiphase Optimal Con\-trol Prob\-lem}
\label{sec:mp_ocp}

The purpose of a generic multiphase problem formulation is to account for cases in which state discontinuities are expected.
In the context of rocket launchers composed of multiple stages, state discontinuities are expected at stage separation events.

Letting $p = 1,\dots, P$ be a scalar integer indicating a specific phase, where $P$ is the total number of phases in the problem, a generic multiphase optimal control problem can be formulated as minimizing the cost functional
\begin{multline} 	\label{MP_OCPGenCost}
	\mathcal{J} = \Phi \bigl( \bm{x}(t{\scriptstyle _0^{(1)} }), t{\scriptstyle _0^{(1)} }, \bm{x}( t{\scriptstyle _f^{(P)} }), t{\scriptstyle _f^{(P)} } \bigr) \\ 
	+ \sum_{p=1}^{P} \Biggl\{ \int_{t_0^{(p)}}^{t_f^{(p)}} \Psi \bigl( \bm{x}(t), \bm{u}(t) \bigr) \dd{t} \Biggr\} \,,
\end{multline}
associated with the trajectory of the dynamic system
\begin{equation}	\label{MP_OCPGenEoM}
	\dot{\bm{x}}^{(p)} (t) = \bm{f}^{(p)} \bigl( \bm{x}(t), \bm{u}(t) \bigr) \,,
\end{equation} 
subject to the path constraints
\begin{equation}	\label{MP_OCPGenPathConst}
	\bm{h}^{(p)} \bigl( \bm{x}(t), \bm{u}(t) \bigr) \leq \bm{0} \,,
\end{equation}
the event constraints
\begin{equation}	\label{MP_OCPGenBoundaries}
	\bm{\phi}^{(p)} \bigl( \bm{x}(t{\scriptstyle _0^{(p)} }), t{\scriptstyle _0^{(p)} }, \bm{x}( t{\scriptstyle _f^{(p)} }), t{\scriptstyle _f^{(p)} } \bigr) \leq \bm{0} \,,
\end{equation}
and the phase linkage conditions
\begin{equation}	\label{MP_OCPGenLinkage}
	\Delta \bm{x}_\text{min}^{(p)} \: \leq \: \fLink^{(p)} = \bm{x}(t{\scriptstyle _0^{(p+1)} }) - \bm{x}(t{\scriptstyle _f^{(p)} }) \: \leq \:  \Delta \bm{x}_\text{max}^{(p)} \,,
\end{equation}
where $\bm{x}(t) \in \mathbb{R}^{n_{\bm{x}}}$ is the state vector, $\bm{u}(t) \in \mathbb{R}^{n_{\bm{u}}}$ is the control vector, and $ \Delta \bm{x}_\text{min}^{(p)} $ and $ \Delta \bm{x}_\text{max}^{(p)} $ are, respectively, the user-defined lower and upper boundary vectors for the linkage conditions.
This formulation indicates that the constraints are not explicit functions of time, therefore, the systems in study are time invariant. Equation \ref{MP_OCPGenLinkage}, which corresponds to the phase linkage conditions, consists of two distinct constraints, the upper bound and the lower bound of the eventual state discontinuities. This allows the specification of several discontinuity scenarios.


\subsection{Gaussian Quadrature and Domain Mapping}
\label{subsec:GaussQuad}

It is possible to compute the definite integral from \numrange{-1}{1} of a given polynomial function, \(f(\tau)\), according to the \emph{Gaussian quadrature} where the function is evaluated at \( N \) discrete points and a weighted sum of the samples is performed \citep{Davis1984,Fahroo2008}. In order to integrate over a generic interval \( t \in [ t_0, t_f ] \), the domain of the independent variable, \(t\), is mapped into the normalized domain \( \tau \in [ -1, 1 ] \). This mapping is linear and it can be expressed as \citep{Sagliano2017c}
\begin{equation} 
	t(\tau) = \frac{t_f - t_0}{2} \tau + \frac{t_f + t_0}{2}, \quad
	\tau \in [ -1, 1 ] \,. \label{eq:DomainMapping}
\end{equation}
By performing a change of variables, the Gaussian quadrature can be expressed for a generic interval as
\begin{equation} \label{eq:GaussQuadratureGeneric}
	\nint{t_0}{t_f} f(t) \dd{t} = \dfrac{t_f-t_0}{2} \sum_{k=1}^{N}  w_k  f_k \,,
\end{equation}
where \( f_k = f \bigl( t(\tau_k) \bigr) \) is the value of the function evaluated at \( t(\tau_k) \), and $ w_k $ is a scalar quadrature weight associated with the $ k^{\text{th}} $ function sample. 
It is important to note that it is not sufficient to evaluate the function at any given set of \( N \) discrete points. For the quadrature to be accurate it is crucial that the distribution of points follows a pattern similar to the distribution of roots of a particular Jacobi polynomial (also called Gauss points) \citep{Davis1984,Fahroo2008}.


\subsection{Lagrange Polynomial Interpolation and the Differentiation Matrix}
\label{subsec:LagrangeDiffMatrix}

In order to compute the first derivative of a polynomial function $\bm{x}(\tau)$ with respect to $ \tau \in [-1, 1] $, a differentiation matrix, $\bm{A}$, can be built based on a Lagrange polynomial interpolation \citep{Berrut2004,Rao2010,Ross2003}
\begin{equation} \label{DiffMatrixElements}
	\bm{A}_{ki} =
	\sum_{\substack{l \\ l \neq i}} \dfrac{1}{ \tau_i - \tau_l }
	\prod_{\substack{j \\ j \neq i,l }} \dfrac{ \tau_k - \tau_j }{ \tau_i - \tau_j } \,,
\end{equation}
where $\bm{A}_{ki}$ is a scalar element on the $k^\text{th}$ row and $i^\text{th}$ column of matrix $\bm{A}$,
and the derivative operation with respect to a generic domain, $t \in [t_0, t_f]$, is
\begin{equation}
	\dot{\bm{x}}_k = \dfrac{2}{t_f-t_0} \sum_{i} \bm{A}_{ki} \bm{x}_i \,, \label{eq:GenDiff}
\end{equation}
where $\bm{x}_k$ is the polynomial $\bm{x}(\tau)$ evaluated at \(k^\text{th}\) sample $ \tau_k $.
As was the case for Gaussian quadrature, the accuracy of the Lagrange polynomial interpolation, and that of the differentiation matrix, is directly related to the choice of sample points along the domain of \(\tau\). 


\subsection{Root distribution of the flipped Radau polynomial}
\label{subsec:RadauPoly}

A good choice of sample points is a proportional mapping of the roots of Legendre-based polynomials, such as the \emph{flipped Radau polynomial}. The flipped Radau polynomial is the result of the difference between two Legendre polynomials of consecutive order, as
\begin{equation}
	R_N(\tau) = P_N(\tau) - P_{N-1}(\tau), \quad  \tau \in [ -1, 1 ] \,,
\end{equation}
where $P_N(\tau)$ is the Legendre polynomial of order $N$. And the Gaussian quadrature weights associated with the roots of the flipped Radau polynomial can be computed by doing a \emph{flip} operation on the weights of the direct Radau, reversing their order \citep{Fahroo2008, Sagliano2017c}
\begin{equation}
	w_k = \texttt{flip} \biggl\{ \dfrac{1-\tau_k^\text{DR}}{N^2 P_{N-1}^2(\tau_k^\text{DR})} \biggr\}, \quad k = 1,2,..,N \,,
\end{equation}
where $\tau_k^\text{DR}$ is the $k^{th}$ abscissa of the direct Radau roots, $N$ is the degree of the Radau polynomial, and \(P_{N-1}(\tau_k^\text{DR})\) is the Legendre polynomial of degree $N-1$ evaluated at $\tau_k^\text{DR}$. 

Figure~\ref{fig:MP_CollocationNodes} illustrates the pattern of collocation nodes for a discretization of the domain composed of multiple phases using the flipped Radau pseudospectral method. The multiphase discretization scheme is a simple concatenation of several "single phase" schemes. It can be noticed that the "density" of nodes increases near the phase transition points. One peculiarity of the roots of the Radau polynomial is that they are asymmetric with respect to the origin.
\begin{figure}[hbt!]
	\centering
	%\includegraphics[width=0.8\textwidth]{ChapterIII/MultiphaseNodes.pdf}
	\includegraphics[width=\linewidth]{ChapterIII/MultiphaseNodes_small.pdf}
	\caption{Illustration of the pattern of collocation nodes for multiple phase transcription. Example of three phases, each with five collocation nodes.}
	\label{fig:MP_CollocationNodes}
\end{figure}


\subsubsection{Multiphase OCP expressed as a NLP problem}

The multiphase nonlinear programming problem can be expressed as minimizing the cost 
\begin{equation}
	\mathcal{J}^N = \Phi + \sum_{p=1}^{P} \dfrac{ t{\scriptstyle _f ^{(p)}} - t{\scriptstyle _0^{(p)}} }{2} \sum_{k}^{ N^{(p)} } w_k^{(p)}  \Psi_k \,, \label{eq:mp_nlp_cost}
\end{equation}
subject to
\begin{align}
	\Ddyn_k\pp = \dfrac{ t{\scriptstyle _f ^{(p)}} - t{\scriptstyle _0^{(p)}} }{2} \bm{f}_k^{(p)} - \sum_i \bm{A}_{ki}^{(p)} \bm{x}_i^{(p)} &= \bm{0} \,, \label{eq:mp_spec_Ddyn} \\
	\bm{h}_k^{(p)} &\leq \bm{0} \,, \label{eq:mp_spec_path}  \\
	\bm{\phi}^{(p)} &\leq \bm{0} \,, \label{eq:mp_spec_event} \\
	\Delta \bm{x}_\text{min}^{(p)}  \leq \fLink^{(p)}  &\leq \Delta \bm{x}_\text{max}^{(p)} \,, \label{eq:nlp_link}
\end{align}
where the vector $ \Ddyn_k\pp $ is a shortened representation of the constraints for dynamic defects from \eqref{eq:mp_spec_Ddyn}.
