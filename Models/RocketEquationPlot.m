function RocketEquationPlot

close all

MassRatio   = 1:1e-2:25;
SelectMR   	= [2, 3, 4, 24];

Nplots      = length(SelectMR);

DeltaV      = log(MassRatio) ;
DeltaVaux   = log(SelectMR);

eps         = 1e-3;
Zeta        = 0:eps:(1-eps);
DeltaVVe    = -log(1-Zeta);

%%

w = 700;
k = 2/4;
h = floor( k*w );

figure(1);
set(gcf, 'Position', [200, 200, w, h]);
plot(Zeta, DeltaVVe, 'Linewidth', 2)
grid on
box on
title('Tsiolkovsky Rocket Equation')
xlabel('Propellant Mass Fraction, \zeta')
ylabel('\DeltaV / v_e')


%%

w = 700;
k = 2/4;
h = floor( k*w );

figure(2);
set(gcf, 'Position', [200, 200, w, h]);
hold on
plot( MassRatio, DeltaV, 'Linewidth', 1.5);
for ii = 1:Nplots
    stem( SelectMR(ii), DeltaVaux(ii), '.k', 'MarkerSize', 2,'Linewidth', 2)
    
end
text(SelectMR(1), DeltaVaux(1)+0.1, '\DeltaV_1', 'rotation', 90, 'fontweight','bold');
text(SelectMR(2), DeltaVaux(2)+0.1, '\DeltaV_2', 'rotation', 90, 'fontweight','bold');
text(SelectMR(3), DeltaVaux(3)+0.1, '\DeltaV_3', 'rotation', 90, 'fontweight','bold');
text(SelectMR(4)-1, 1.2, '\DeltaV_{Total}', 'rotation', 90, 'fontweight','bold');

text(10, log(10), 'v_e^{(1)} = v_e^{(2)} = v_e^{(3)} = 1 v.u.', 'rotation', 15, 'BackgroundColor','w');

dim = [.4 .1 .3 .3];
% str = {'\DeltaV_{Total} = \DeltaV_1 + \DeltaV_2'};
str = {'\DeltaV_1 + \DeltaV_2 + \DeltaV_3 = \DeltaV_{Total}'};
annotation('textbox',dim,'String',str,'FitBoxToText','on', 'BackgroundColor', 'w', 'FontSize', 11, 'fontweight','bold');
		

hold off
% set(gca,'XTickLabel', [],'YTickLabel', [])
grid on
box on
axis tight
title({'{\bf\fontsize{12} Tsiolkovsky Rocket Equation}'; 'Three-Staged Vehicle Example'},'FontWeight','Normal')
xlabel('Mass Ratio, {\bf MR}')
ylabel('\DeltaV')

end
