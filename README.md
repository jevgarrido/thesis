# Development of Multiphase Radau Psudospectral Methods for Optimal Control Problems

#### Thesis to obtain the Master of Science Degree in Aerospace Engineering at IST
### by José Garrido



## Abstract

In this work, the flipped Radau pseudospectral method is employed in order to solve multiphase optimal control problems. The application of interest is the generation of multi-stage rocket trajectories, both in ascent to orbit and descent to vertical landing. The method is implemented in the MATLAB tool SPARTAN, developed at DLR, with the use of the NLP solver IPOPT.
Two relevant numerical examples are implemented for validation of the algorithm. The examples are appropriate because they contain either mutations in the equations of motion from one phase to the next or jump discontinuities in the states (or both).
The first problem is composed of three distinct phases and is concerned with the recovery of the main booster of the orbital launcher Falcon 9 via a boost-back manoeuvre and a vertical landing near the launch site.
The second problem is composed of four phases and is concerned with the launch, ascent and orbit insertion of the multiple-staged solid fuel orbital rocket Delta III.
The algorithm is validated both, by the comparison of the results to the corresponding reference solutions, and by the analysis of the dual variables and of the Hamiltonian associated with the trajectories.
The results show that the method is able to generate optimal trajectories with accuracy comparable to state of the art solvers.



##### Documents - Final Version

[Dissertation](Text/Archive/2021-01-28/Thesis.pdf)
[Extended Abstract](Text/Archive/2021-01-28/ExtendedAbstract.pdf)



##### Official IST Dissertation Webpage

[IST webpage](https://fenix.tecnico.ulisboa.pt/cursos/meaer/dissertacao/1691203502344219)
